enum Flavor {
  PILOT,
  STAGING,
  PROD,
}

class F {
  static Flavor? appFlavor;

  static String get title {
    switch (appFlavor) {
      case Flavor.PILOT:
        return 'Tuzz Test';
      case Flavor.STAGING:
        return 'Tuzz Staging';
      case Flavor.PROD:
        return 'Tuzz';
      default:
        return 'title';
    }
  }

  static String get baseUrl {
    switch (appFlavor) {
      case Flavor.PILOT:
        return 'https://api-poc.tuzz.com/api/';
      case Flavor.STAGING:
        return 'https://api-poc.tuzz.com/api/';
      case Flavor.PROD:
        return 'https://api-prod.tuzz.com/api/';
      default:
        return 'https://api-prod.tuzz.com/api/';
    }
  }

  static String get baseUrlParameter {
    switch (appFlavor) {
      case Flavor.PILOT:
        return 'https://api-poc.tuzz.com/api/';
      case Flavor.STAGING:
        return 'https://api-poc.tuzz.com/api/';
      case Flavor.PROD:
        return 'https://api-prod.tuzz.com/api/';
      default:
        return 'https://api-prod.tuzz.com/api/';
    }
  }

}
