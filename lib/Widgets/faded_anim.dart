
import 'package:flutter/material.dart';

class FadeAnimation extends StatefulWidget {
  final dynamic delay;
  final Widget child;

  FadeAnimation(this.delay, this.child);

  @override
  _FadeAnimationState createState() => _FadeAnimationState();
}

class _FadeAnimationState extends State<FadeAnimation> with TickerProviderStateMixin {

  // AnimationController animation = AnimationController(vsync: );
  // Animation<double>? _fadeInFadeOut;

  @override
  void initState() {
    super.initState();
    // animation = AnimationController(vsync: this, duration: Duration(seconds: double.parse(widget.delay.toString()).toInt()),);
    // _fadeInFadeOut = Tween<double>(begin: 0.0, end: 0.5).animate(animation);
    //
    // animation.addStatusListener((status){
    //   if(status == AnimationStatus.completed){
    //     animation.reverse();
    //   }
    //   else if(status == AnimationStatus.dismissed){
    //     animation.forward();
    //   }
    // });
    // animation.forward();
  }

  @override
  void dispose() {
    // animation.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return widget.child;
    // return FadeTransition(
    //   opacity: _fadeInFadeOut!,
    //   child: widget.child,
    // );
  }
}
