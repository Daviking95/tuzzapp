import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Snippets/top_categories_snippet.dart';
import 'package:tuzz/Widgets/text.dart';

import 'image_widget.dart';

class TopCategoryCustomTab extends StatefulWidget {
  final List<dynamic> tabTitleStringList;
     final List<Widget> tabWidgetsList;
  final TabController? tabController;

  TopCategoryCustomTab(
      this.tabTitleStringList, this.tabWidgetsList, this.tabController);

  @override
  _TopCategoryCustomTabState createState() => _TopCategoryCustomTabState();
}

class _TopCategoryCustomTabState extends State<TopCategoryCustomTab> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: widget.tabTitleStringList.length,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Theme.of(context).backgroundColor,
          toolbarHeight: 110,
          elevation: 0,
          bottom: TabBar(
              controller: widget.tabController,
              unselectedLabelColor: Color(0xff5F6C6A),
              labelColor: Color(0xff1B1C20),
              indicatorWeight: 70,
              indicatorSize: TabBarIndicatorSize.tab,
              isScrollable: true,
              onTap: (index) {
                setState(() {
                  widget.tabController!.index = index;

                });
              },
              automaticIndicatorColorAdjustment: false,
              indicator: BoxDecoration(
                // color: Color(0xff1B1C20),
                  borderRadius: BorderRadius.circular(100)),
              labelStyle: textStyleBlackColorNormal.copyWith(fontSize: 10),
              tabs: [
                for (var i = 0; i < widget.tabTitleStringList.length; i++) ...[
                  SizedBox(
                    height: 90,
                    child: Tab(
                      icon: Column(
                        children: [
                          Container(
                            height: 60,
                            width: 60,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(100),
                                color: i == widget.tabController!.index
                                    ? Color(0xff1B1C20)
                                    : Color(0xffDFFFF4)),
                            child: ImageWidget.withSizeAndColor(
                                widget.tabTitleStringList[i]["imageString"], 24, 20, i == widget.tabController!.index
                                ? Color(0xffDFFFF4)
                                : Color(0xff11A683), true)
                                .paddingAll(15),
                          ),
                        ],
                      ),
                      text: widget.tabTitleStringList[i]["title"],
                    ),
                  ),
                ]
              ]),
        ),
        // backgroundColor: Theme.of(context).backgroundColor,
        body: TabBarView(controller: widget.tabController, children: [
          for (var tabIndexWidget in widget.tabWidgetsList) ...[tabIndexWidget]
        ]),
      ),
    );
  }
}

Widget customTab(BuildContext context, List<dynamic> tabTitleStringList,
    List<Widget> tabWidgetsList,
    [TabController? tabController]) {
  return DefaultTabController(
    length: tabTitleStringList.length,
    child: Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Theme.of(context).backgroundColor,
        toolbarHeight: 100,
        elevation: 0,
        bottom: TabBar(
            controller: tabController,
            unselectedLabelColor: Color(0xff5F6C6A),
            labelColor: Color(0xff1B1C20),
            indicatorSize: TabBarIndicatorSize.tab,
            isScrollable: true,
            onTap: (index) {
              tabController!.index = index;
            },
            automaticIndicatorColorAdjustment: false,
            indicator: BoxDecoration(
                // color: Color(0xff1B1C20),
                borderRadius: BorderRadius.circular(100)),
            tabs: [
              for (var i = 0; i < tabTitleStringList.length; i++) ...[
                Tab(
                  icon: Container(
                    height: 45,
                    width: 45,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        color: i == tabController!.index
                            ? Color(0xff1B1C20)
                            : Color(0xffDFFFF4)),
                    child: ImageWidget.withSizeOnly(
                            tabTitleStringList[i]["imageString"], 24, 24, true)
                        .paddingAll(15),
                  ),
                  text: tabTitleStringList[i]["title"],
                  // child: TopCategoryWidget(
                  //     imageString: tabIndexString["imageString"],
                  //     title: tabIndexString["title"],
                  //     array: tabTitleStringList,
                  //     passedIndexData: tabIndexString),
                ),
              ]
            ]),
      ),
      // backgroundColor: Theme.of(context).backgroundColor,
      body: TabBarView(controller: tabController, children: [
        for (var tabIndexWidget in tabWidgetsList) ...[tabIndexWidget]
      ]),
    ),
  );
}
