import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppConstants/ImageStringConstants/image_string_constants.dart';
import 'package:tuzz/AppConstants/RoutesConstants/route_constants.dart';
import 'package:tuzz/AppConstants/StringConstants/app_constants.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';

import 'image_widget.dart';

class CustomBottomNavigation extends StatefulWidget {
  final int? selectedIndex;

  CustomBottomNavigation({this.selectedIndex});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return CustomBottomNavigationState();
  }
}

class CustomBottomNavigationState extends State<CustomBottomNavigation> {
  double iconSize = 18.0;

  void _onItemTapped(int index) {
    setState(() {
      if (index == 0) {
        Get.offAndToNamed(RoutesConstants.DASHBOARD_URL);
      } else if (index == 1) {
        Get.offAndToNamed(RoutesConstants.NOTIFICATION_URL);
      } else if (index == 2) {
        Get.offAndToNamed(RoutesConstants.DASHBOARD_URL);
      } else if (index == 3) {
        Get.offAndToNamed(RoutesConstants.DASHBOARD_URL);
      } else if (index == 4) {
        Get.offAndToNamed(RoutesConstants.DASHBOARD_URL);
      } else {
        Get.offAndToNamed(RoutesConstants.DASHBOARD_URL);
        // Navigator.pushReplacementNamed(context, RoutesConstants.dashboardUrl);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return BottomNavigationBar(
      elevation: 10.0,
      type: BottomNavigationBarType.fixed,
      unselectedItemColor: Color(0XFF11A683),
      backgroundColor: Colors.white,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          activeIcon: _bNavSpacing( ImageWidget.withSizeOnly("assets/images/svgs/btm_nav_icons/home_active.svg", 24, 24), true),
          icon: _bNavSpacing(ImageWidget.withSizeOnly("assets/images/svgs/btm_nav_icons/home.svg", 24, 24)),
          title: Container(),
        ),
        BottomNavigationBarItem(
          activeIcon: _bNavSpacing( ImageWidget.withSizeOnly("assets/images/svgs/btm_nav_icons/notification_active.svg", 24, 24), true),
          icon: _bNavSpacing(ImageWidget.withSizeOnly("assets/images/svgs/btm_nav_icons/notification.svg", 24, 24)),
          title: Container(),
        ),
        BottomNavigationBarItem(
          activeIcon: _bNavSpacing( ImageWidget.withSizeOnly("assets/images/svgs/btm_nav_icons/transaction_active.svg", 48, 48), true),
          icon: _bNavSpacing(ImageWidget.withSizeOnly("assets/images/svgs/btm_nav_icons/transaction.svg", 48, 48)),
          title: Container(),
        ),
        BottomNavigationBarItem(
          activeIcon: _bNavSpacing( ImageWidget.withSizeOnly("assets/images/svgs/btm_nav_icons/profile_active.svg", 24, 24), true),
          icon: _bNavSpacing(ImageWidget.withSizeOnly("assets/images/svgs/btm_nav_icons/profile.svg", 24, 24)),
          title: Container(),
        ),
        BottomNavigationBarItem(
          activeIcon: _bNavSpacing( ImageWidget.withSizeOnly("assets/images/svgs/btm_nav_icons/more.svg", 24, 24), true),
          icon: _bNavSpacing(ImageWidget.withSizeOnly("assets/images/svgs/btm_nav_icons/more.svg", 24, 24)),
          title: Container(),
        ),
      ],
      currentIndex: widget.selectedIndex!,
      selectedItemColor: appPrimaryColor,
      onTap: _onItemTapped,
    );
  }

  Widget _bNavSpacing(Widget widget, [bool isActive = false, double spacingSize = 0]) {
    return Container(
        margin:
            EdgeInsets.symmetric(vertical: spacingSize == 0 ? 5 : spacingSize),
        child: Column(
          children: [
            widget,
            isActive ? Container(
              width: 24,
                child: Divider(thickness: 3, color: Colors.red,)) : Container()
          ],
        ));
  }

  TextStyle _bNavTextStyle() {
    TextStyle textStyle = TextStyle(fontSize: 15, fontWeight: FontWeight.w400);
    return textStyle;
  }
}
