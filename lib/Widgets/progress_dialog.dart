
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';

class ProgressWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Center(
        child: Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10)), color: Colors.white),
          width: Get.width / 1.5,
          height: 80,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Platform.isIOS
                  ? CupertinoActivityIndicator()
                  : CircularProgressIndicator(),
              SizedBox(width: 30,),
              Text("Please wait...", style: TextStyle(color: appPrimaryColor, fontSize: 15),)
            ],
          ).paddingSymmetric(horizontal: 15, vertical: 5),
        ),
      ),
    );
  }
}
