import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'package:tuzz/AppConstants/ImageStringConstants/image_string_constants.dart';
import 'package:tuzz/AppConstants/RoutesConstants/route_constants.dart';
import 'package:tuzz/AppConstants/StringConstants/app_constants.dart';
import 'package:tuzz/Helpers/Others/get_theme_state.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/text.dart';

import 'app_dialog_widget.dart';
import 'bottom_sheet_widget.dart';
import 'buttons.dart';


dynamic exitApp(BuildContext context) {
  return showDialog(
        builder: (context) => new AlertDialog(
          title: Column(
            children: [
              Lottie.asset(ImageStringConstants.SAD_FACE, width: 80),
              SizedBox(
                height: 10,
              ),
              Text(
                StringConstants.EXIT_APP_TITLE,
                textAlign: TextAlign.center,
                style: textStylePrimaryColorNormal.copyWith(
                  color: appSecondaryColor,
                ),
              ),
            ],
          ),
          content: Text(
            StringConstants.EXIT_APP_CONTENT,
            textAlign: TextAlign.center,
            style: textStylePrimaryColorNormal,
          ),
          actions: <Widget>[
            new TextButton(
              onPressed: () => Get.offAllNamed(RoutesConstants.LOGIN_URL),
              child: new Text(StringConstants.NO),
            ),
            new TextButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: new Text(StringConstants.YES),
            ),
          ],
        ),
        context: context,
      );
}

dynamic warningDialog(
    BuildContext context, String title, String subtitle, Function _function) {
  return showDialog(
        builder: (context) => new AlertDialog(
          title: Column(
            children: [
              Lottie.asset(ImageStringConstants.CONFIRM_IMAGE, width: 80),
              SizedBox(
                height: 20,
              ),
              Text(
                title.toUpperCase(),
                textAlign: TextAlign.center,
                style: textStylePrimaryColorNormal.copyWith(
                  color: appSecondaryColor,
                ),
              ),
            ],
          ),
          content: Text(
            subtitle,
            textAlign: TextAlign.center,
            style: textStylePrimaryColorNormal,
          ),
          actions: <Widget>[
            new TextButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: new Text(
                StringConstants.CANCEL,
                style: textStylePrimaryColorNormal.copyWith(color: Colors.red),
              ),
            ),
            new TextButton(
              onPressed: () {
                Navigator.of(context).pop(false);
                _function();
              },
              child: new Text(StringConstants.PROCEED, style: textStylePrimaryColorNormal),
            ),
          ],
        ),
        context: context,
      );
}

dynamic pinCodeDialog(
    BuildContext context, String title, String subtitle, Function _function, TextEditingController pinPutController) {
  final FocusNode _pinPutFocusNode = FocusNode();

  BoxDecoration _pinPutDecoration = BoxDecoration(
    border: Border.all(color: Colors.deepPurpleAccent),
    borderRadius: BorderRadius.circular(15.0),
  );

  return showDialog(
        builder: (context) => new AlertDialog(
          title: Column(
            children: [
              Lottie.asset(ImageStringConstants.OTP_PIN_IMAGE, width: 80),
              SizedBox(
                height: 10,
              ),
              Text(
                title.toUpperCase(),
                textAlign: TextAlign.center,
                style: textStylePrimaryColorNormal.copyWith(
                  color: appSecondaryColor,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                subtitle,
                textAlign: TextAlign.center,
                style: textStylePrimaryColorNormal,
              ),
              SizedBox(
                height: 5,
              ),
              PinPut(
                fieldsCount: 6,
                // onSubmit: (String pin) => _snackBar,
                focusNode: _pinPutFocusNode,
                controller: pinPutController,
                submittedFieldDecoration: _pinPutDecoration.copyWith(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                keyboardType: TextInputType.number,
                selectedFieldDecoration: _pinPutDecoration,
                followingFieldDecoration: _pinPutDecoration.copyWith(
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all(
                    color: Colors.deepPurpleAccent.withOpacity(.5),
                  ),
                ),
              )
            ],
          ),
          actions: <Widget>[
            new TextButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: new Text(
                StringConstants.CANCEL,
                style: textStylePrimaryColorNormal.copyWith(color: Colors.red),
              ),
            ),
            new TextButton(
              onPressed: () {
                Navigator.of(context).pop(false);
                _function();
              },
              child: new Text(StringConstants.PROCEED, style: textStylePrimaryColorNormal),
            ),
          ],
        ),
        context: context,
      );
}

dynamic editSuccess(
    BuildContext context, String msg, bool isSuccess, String routeToGo) {
  return showDialog(
        builder: (context) => new AlertDialog(
          content: Container(
            height: 250,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Lottie.asset(ImageStringConstants.CONFIRM_IMAGE, width: 80),
                SizedBox(
                  height: 20,
                ),
                Text(
                  msg,
                  textAlign: TextAlign.center,
                  style: textStyleBlackColorNormal,
                ),
                SizedBox(
                  height: 30,
                ),
                AppPrimaryDarkButtonRound(
                  width: Get.width,
                  // isLoader: state is SignUpLoading ? true : false,
                  borderRadius: 10.0,
                  textTitle: StringConstants.CONTINUE,
                  textStyle: textStyleWhiteColorNormal,
                  functionToRun: () => Get.toNamed(routeToGo),
                )
              ],
            ),
          ),
        ),
        context: context,
      );
}

displayImagePickerDialog(
    BuildContext context, Function? openCamera, Function? openGallery) async {
  return modalBottomSheet(
      context,
      Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            child: InkWell(
              onTap: openCamera!(),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.camera,
                      size: 30,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CustomText(
                      title: StringConstants.OPEN_CAMERA,
                      textSize: 15.0,
                      fontFamily: StringConstants.INTER_FONT_FAMILY,
                      textColor: appPrimaryColor,
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: InkWell(
              onTap: openGallery!(),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.photo,
                      size: 30,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CustomText(
                      title: StringConstants.OPEN_GALLERY,
                      textSize: 15.0,
                      fontFamily: StringConstants.INTER_FONT_FAMILY,
                      textColor: appPrimaryColor,
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
      false,
      150);
}

openImageInDialog(context, imageItem) {
  return showDialog(
    builder: (context) => new AlertDialog(
      content: Container(
        width: Get.width / 2,
        height: Get.height / 2,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: ExactAssetImage(imageItem == null || imageItem == "null"
                    ? ImageStringConstants.APP_LOGO_PNG
                    : imageItem),
                fit: BoxFit.contain)),
      ),
    ),
    context: context,
  );
}

dynamic twoButtonsDialog(
    BuildContext context, String title, String subtitle, Function _function) {
  return showDialog(
    builder: (context) => new AlertDialog(
      title: Column(
        children: [
          // Lottie.asset("assets/images/confirm_img.json", width: 80),
          SizedBox(
            height: 20,
          ),
          Text(
            title.toUpperCase(),
            textAlign: TextAlign.center,
            style: textStylePrimaryColorNormal.copyWith(
              color: appSecondaryColor,
            ),
          ),
        ],
      ),
      content: Text(
        subtitle,
        textAlign: TextAlign.center,
        style: textStylePrimaryColorNormal,
      ),
      actions: <Widget>[
        new TextButton(
          onPressed: () => Navigator.of(context).pop(false),
          child: new Text(
            StringConstants.CANCEL,
            style: textStylePrimaryColorNormal.copyWith(color: Colors.red),
          ),
        ),
        new TextButton(
          onPressed: () {
            Navigator.of(context).pop(false);
            _function();
          },
          child: new Text(StringConstants.PROCEED, style: textStylePrimaryColorNormal),
        ),
      ],
    ),
    context: context,
  );
}


showErrorWidgetDialog(
    {BuildContext? context,
      String? title,
      Widget? widget,
      bool hasNextButton = false,
      Function? nextFunction}) async {
  return showDialog<String>(
    context: context!,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(
          title == null ? StringConstants.UNKNOWN_ERROR : title,
          style: textStyleBlackColorNormal.copyWith(
              fontSize: 20.0, color: Colors.red),
        ),
        content: Container(
          padding: EdgeInsets.all(0.8),
          margin: EdgeInsets.only(left: 14, right: 14),
          width: isPortrait(Get.context)
              ? Get.width
              : Get.width / 2,
          child: Card(
            color: Colors.white,
            clipBehavior: Clip.hardEdge,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: Container(
              child: Column(
                children: [
                  Container(
                    width: isPortrait(Get.context)
                        ? Get.width
                        : Get.width / 2,
                    decoration: BoxDecoration(
                      color: appErrorColor,
                    ),
                    padding: EdgeInsets.all(3),
                    child: Flex(
                      direction: Axis.horizontal,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: 20,
                          height: 20,
                        ),
                        CustomText(
                          title: title ?? "",
                          textColor: Colors.white,
                          isBold: true,
                          fontWeight: FontWeight.w600,
                        ),
                        IconButton(
                          iconSize: 20.0,
                          padding: EdgeInsets.zero,
                          icon: Icon(
                            Icons.clear,
                            color: Colors.white,
                          ),
                          onPressed:() {
                            Navigator.of(context).pop();

                            nextFunction!();
                          },
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: isPortrait(Get.context)
                        ? Get.width
                        : Get.width / 2,
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.all(10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Expanded(child: overlayObject.icon),
                        Expanded(
                          flex: 6,
                          child: CustomText(
                            isCenter: false,
                            title: title ?? "",
                            textColor: Color(0xff1B1C20),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: Text(
              StringConstants.CANCEL,
              style: textStyleBlackColorNormal.copyWith(
                  fontSize: 14.0, color: Colors.red),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          hasNextButton
              ? TextButton(
            child: Text(
              StringConstants.PROCEED,
              style: textStylePrimaryColorNormal,
            ),
            onPressed: () {
              Navigator.of(context).pop();

              nextFunction!();
            },
          )
              : Container()
        ],
      );
    },
  );
}

showWidgetDialog(
    {BuildContext? context,
      String? title,
      Widget? widget,
      bool hasNextButton = false,
      Function? nextFunction}) async {
  return showDialog<String>(
    context: context!,
    barrierDismissible: false,
    // dialog is dismissible with a tap on the barrier
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(
          title!,
          style: textStyleBlackColorNormal.copyWith(
              fontSize: 20.0, color: appPrimaryColor),
        ),
        content: widget,
        actions: <Widget>[
          FlatButton(
            child: Text(StringConstants.CANCEL),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          hasNextButton
              ? FlatButton(
            child: Text(
              StringConstants.PROCEED,
              style: textStylePrimaryColorNormal,
            ),
            onPressed: () {
              Navigator.of(context).pop();

              nextFunction!();
            },
          )
              : Container()
        ],
      );
    },
  );
}