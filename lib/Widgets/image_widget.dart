import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';

class ImageWidget extends StatelessWidget {
  final String imageString;
  final double? height;
  final double? width;
  final bool isPng;
  final Color? imageColor;

  ImageWidget(this.imageString, this.height, this.width,
      [this.isPng = false])
      : this.imageColor = appPrimaryColor;

  ImageWidget.withSizeOnly(this.imageString, this.height, this.width,
      [this.isPng = false])
      : this.imageColor = null;

  ImageWidget.withNoSizeAndNoColor(this.imageString, [this.isPng = false])
      : this.imageColor = null,
        this.height = 0,
        this.width = 0;

  ImageWidget.withColorOnly(this.imageString, this.imageColor,
      [this.isPng = false])
      : this.height = 0,
        this.width = 0;

  ImageWidget.withSizeAndColor(this.imageString, this.height, this.width, this.imageColor,
      [this.isPng = false]);

  @override
  Widget build(BuildContext context) {
    return height == 0 || width == 0
        ? isPng
            ? Image.asset(
                imageString,
                color: imageColor,
              )
            : SvgPicture.asset(
                imageString,
                color: imageColor,
              )
        : isPng
            ? Image.asset(
                imageString,
                height: height,
                width: width,
                color: imageColor,
              )
            : SvgPicture.asset(
                imageString,
                height: height,
                width: width,
                color: imageColor,
              );
  }
}
