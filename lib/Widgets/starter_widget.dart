import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/progress_dialog.dart';

import 'custom_bottom_nav_wid.dart';
import 'dialogs_widget.dart';

class StarterWidget extends StatefulWidget {
  final Widget? topPageContent;
  final Widget? bottomPageContent;
  final bool isBgAllowed;
  final bool isFabAllowed;
  final bool isCustomFab;
  final Widget? customFab;
  final bool hasTopWidget;
  final bool hasNavDrawer;
  final IconData? fabIcon;
  final Function? fabFunc;
  final double extraSpace;
  final int navItemPage;
  final String bgImage;
  final BoxDecoration? bgWidget;
  final bool isExitApp;
  final bool hasRefresh;
  // final Function refreshFunction;
  final bool hasPagePadding;
  final bool hasBottomNav;
  final String fabText;
  final bool isPageScrollable;
  final int bottomNavIndex;
  final bool isBackWidget;
  final bool startLoader;
  final Widget? backUrlWidgetScreen;

  StarterWidget(
      {this.topPageContent,
        this.bottomPageContent,
        this.isBgAllowed = false,
        this.isFabAllowed = false,
        this.hasTopWidget = false,
        this.hasNavDrawer = false,
        this.isCustomFab = false,
        this.customFab,
        this.fabIcon,
        this.fabFunc,
        this.extraSpace = 0.0,
        this.navItemPage = 1,
        this.bgImage = "",
        this.bgWidget,
        this.fabText = "",
        this.startLoader = false,
        this.isExitApp = false,
        this.hasRefresh = false,
        this.hasPagePadding = true,
        this.hasBottomNav = false,
        this.bottomNavIndex = 0,
        // required this.refreshFunction,
        this.isPageScrollable = true,
        this.isBackWidget = true,
        this.backUrlWidgetScreen});

  @override
  _StarterWidgetState createState() => _StarterWidgetState();
}

class _StarterWidgetState extends State<StarterWidget> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => widget.isBackWidget
          ? widget.backUrlWidgetScreen == null
          ? Future.value(true)
          : Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (context) => widget.backUrlWidgetScreen!),
              (context) => false)
          : exitApp(context),
      child: SafeArea(
        child: LoadingOverlay(
          isLoading: widget.startLoader,
          progressIndicator: ProgressWidget(),
          color: Color(0xff1B1C20),
          opacity: 0.8,
          child: Scaffold(
            key: _scaffoldKey,
            body: pageContent(),
            resizeToAvoidBottomInset: true,
            bottomNavigationBar: widget.hasBottomNav
                ? CustomBottomNavigation(selectedIndex: widget.bottomNavIndex)
                : SizedBox(),
          ),
        ),
      ),
    );
  }

  Widget pageContent() {
    return Container(
      padding: widget.hasPagePadding
          ? EdgeInsets.symmetric(vertical: 0.0, horizontal: 20.0)
          : EdgeInsets.all(0.0),
      decoration: widget.isBgAllowed
          ? widget.bgWidget
          : BoxDecoration(color: Theme.of(context).backgroundColor),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                  child: widget.hasTopWidget
                      ? widget.topPageContent!
                      : Container()),
            ],
          ),
          Expanded(
            child: widget.isPageScrollable
                ? SingleChildScrollView(child: widget.bottomPageContent)
                : widget.bottomPageContent!,
          ),
        ],
      ),
    );
  }
}
