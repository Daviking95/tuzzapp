import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:snack/snack.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'buttons.dart';

showToast(
    {String? toastMsg,
    Toast? toastLength,
    ToastGravity? toastGravity,
    Color? bgColor,
    Color? txtColor}) {
  Fluttertoast.showToast(
      msg: toastMsg!,
      toastLength: toastLength,
      gravity: toastGravity,
      timeInSecForIosWeb: 1,
      backgroundColor: bgColor,
      textColor: txtColor,
      fontSize: 16.0);
}

showSnackBar(BuildContext context, Widget widget) {
  SnackBar(
    content: widget,
    duration: Duration(seconds: 2),
  ).show(context);
}