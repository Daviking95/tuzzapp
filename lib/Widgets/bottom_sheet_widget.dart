import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Helpers/Others/get_theme_state.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';

modalBottomSheet(BuildContext context, Widget widget,
    [bool isScrollControlled = true,
    double? height,
    Color bgColor = Colors.white, bool hasPadding = true]) {
  showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (context) {
        return StatefulBuilder(
            builder: (BuildContext context, StateSetter setModalState) {
          return Container(
            height: height,
            // padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
            color: Color(0xFF737373),
            child: new Container(
              padding: EdgeInsets.all(hasPadding ? 15.0 : 0),
              decoration: new BoxDecoration(
                  color: bgColor,
                  borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(30.0),
                      topRight: const Radius.circular(30.0))),
              child: widget,
            ),
          );
        });
      });
}
