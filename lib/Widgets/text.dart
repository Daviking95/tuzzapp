import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tuzz/AppBloc/ThemeBloc/theme_bloc.dart';
import 'package:tuzz/AppConstants/StringConstants/app_constants.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';

class CustomText extends StatelessWidget {
  final String? title;
  final bool? isCenter;
  final bool? isBold;
  final bool? isPrimary;
  final double? textSize;
  final Color? textColor;
  final String? fontFamily;
  final FontWeight? fontWeight;
  final TextDecoration? textDecoration;
  final double height;

  const CustomText(
      {Key? key,
      this.title,
      this.isCenter = false,
      this.textSize = 15,
      this.isBold = false,
      this.isPrimary = false,
      this.textColor,
      this.height = 1.7,
      this.fontFamily = StringConstants.INTER_FONT_FAMILY,
      this.fontWeight = FontWeight.normal,
      this.textDecoration = TextDecoration.none})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    FontWeight? _fontWeight;
    TextStyle? _textStyle;
    Color? _txtColor;

    return BlocBuilder<ThemeChangeBloc, ThemeChangeState>(
      builder: (BuildContext context, ThemeChangeState themeState) {
        _fontWeight = isBold! ? FontWeight.bold : fontWeight!;
        _textStyle = isPrimary!
            ? textStylePrimaryColorNormal
            : textStyleBlackColorNormal;
        _txtColor = textColor == null
            ? isPrimary!
                ? Theme.of(context).primaryColor
                : Theme.of(context).textTheme.bodyText1!.color
            : textColor;

        return Text(title!,
            textAlign: isCenter! ? TextAlign.center : TextAlign.left,
            style: _textStyle!.copyWith(
                fontSize: textSize,
                fontWeight: _fontWeight,
                color: _txtColor,
                height: height,
                decoration: textDecoration,
                fontFamily: fontFamily),
            softWrap: true);
      },
    );
  }
}
