import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:tuzz/AppConstants/StringConstants/app_constants.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/text.dart';

import 'buttons.dart';

Future showSuccessDialog(context, {String message = "", bool canClick = true}) {
  return showDialog(
      context: context,
      builder: (BuildContext context) {
        return GestureDetector(
          onTap: () {
            if (canClick) {
              Navigator.of(context).pop();
            } else {
              return;
            }
          },
          child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.transparent,
            body: Center(
              child: Container(
                margin: EdgeInsets.all(13),
                child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width,
                  height: 200,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CustomText(
                        title:
                        message,
                      ),
                      AppPrimaryDarkButtonRound(
                          functionToRun: () {
                            Navigator.pop(context);
                          },
                          textTitle: StringConstants.OK)
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      });
}

bool isPortrait(context) {
  bool value = MediaQuery.of(context).size.height > MediaQuery.of(context).size.width;
  return value;
}