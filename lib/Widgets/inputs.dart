import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:line_icons/line_icons.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:tuzz/AppBloc/ThemeBloc/theme_bloc.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/AppConstants/StringConstants/app_constants.dart';
import 'package:tuzz/Helpers/Others/date_picker.dart';
import 'package:tuzz/Helpers/Others/print_log_formats.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/text.dart';

class AppTextInputs extends StatelessWidget {
  final String textInputTitle;
  final TextEditingController controller;
  final int? maxLength;
  final TextInputType? textInputType;
  final Color? color;
  final bool autoFocus;
  final void Function()? onTapFunction;
  final void Function(String value)? onChange;
  final bool isReadOnly;
  final int? maxLine;
  final Widget? suffixIcon;
  final double borderRadius;
  final TextInputAction textInputAction;
  final bool hasSuffixIcon;
  final String suffixText;
  final TextInputFormatter? formatter;
  final dynamic validation;
  final bool isStartTextCenter;
  final bool hasDecoration;
  final double fontSize;

  AppTextInputs(
      {this.textInputTitle = "",
      required this.controller,
      this.maxLength = 0,
      this.textInputType,
      required this.color,
      this.autoFocus = false,
      this.onTapFunction,
      this.isReadOnly = false,
      this.onChange,
      this.maxLine = 1,
      this.formatter,
      this.suffixIcon,
      this.suffixText = "",
      this.textInputAction = TextInputAction.next,
      this.borderRadius = 10.0,
      this.hasSuffixIcon = false,
      this.validation,
      this.isStartTextCenter = false,
      this.hasDecoration = true,
      this.fontSize = 14});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 5.0,
        ),
        maxLength == 0
            ? _buildTextInput()
            // materialWrapper(_buildTextInput(), suffixText: suffixText)
            //         .marginSymmetric(vertical: 5)
            : _buildTextInputWithMaxLength(),
        // materialWrapper(_buildTextInputWithMaxLength(), height: 60),
        SizedBox(
          height: 5.0,
        ),
      ],
    );
  }

  Widget _buildTextInput() {
    return BlocBuilder<ThemeChangeBloc, ThemeChangeState>(
      builder: (BuildContext context, ThemeChangeState themeState) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title: textInputTitle,
              textColor: Color(0xff5F6C6A),
              textSize: 14,
              fontWeight: FontWeight.w500,
            ),
            TextFormField(
              decoration: hasDecoration
                  ? textInputDecorator(textInputTitle, color!, suffixIcon,
                      borderRadius, context, themeState, suffixText)
                  : InputDecoration(
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                    ),
              style: textStyleBlackColorNormal.copyWith(
                  color: color, fontSize: fontSize),
              cursorColor: color,
              // initialValue: textInputTitle,
              // textAlignVertical: TextAlignVertical.center,
              textAlign: isStartTextCenter ? TextAlign.center : TextAlign.start,
              keyboardType: textInputType,
              controller: controller,
              autofocus: autoFocus,
              validator: validation,
              onTap: onTapFunction,
              readOnly: isReadOnly,
              inputFormatters: <TextInputFormatter>[
                formatter ?? FilteringTextInputFormatter.singleLineFormatter
              ],
              autovalidateMode: AutovalidateMode.onUserInteraction,
              textInputAction: textInputAction,
              autocorrect: true,
              onChanged: onChange,
              maxLines: maxLine,
            ).marginOnly(top: 10, bottom: 10),
          ],
        );
      },
    );
  }

  Widget _buildTextInputWithMaxLength() {
    return BlocBuilder<ThemeChangeBloc, ThemeChangeState>(
      builder: (BuildContext context, ThemeChangeState themeState) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title: textInputTitle,
              textColor: Color(0xff5F6C6A),
              textSize: 14,
              fontWeight: FontWeight.w500,
            ),
            TextFormField(
              maxLengthEnforcement: MaxLengthEnforcement.enforced,
              decoration: textInputDecorator(textInputTitle, color!, suffixIcon,
                  borderRadius, context, themeState, suffixText),
              style: textStyleBlackColorNormal.copyWith(
                  color: Theme.of(context).textTheme.bodyText1!.color),
              cursorColor: Theme.of(context).textTheme.bodyText1!.color,
//      initialValue: textInputTitle,
              keyboardType: textInputType,
              maxLength: maxLength,
              controller: controller,
              inputFormatters: <TextInputFormatter>[
                formatter ?? FilteringTextInputFormatter.singleLineFormatter
              ],
              validator: validation,
              onTap: onTapFunction,
              readOnly: isReadOnly,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              onChanged: onChange,
              maxLines: maxLine,
            ).marginOnly(top: 10, bottom: 10),
          ],
        );
      },
    );
  }
}

class AppPasswordTextInput extends StatefulWidget {
  final String textInputTitle;
  final TextEditingController? controller;
  final Color? color;
  final TextInputAction? textInputAction;
  final bool? isThereNoBottomPadding;

  AppPasswordTextInput(
      {this.textInputTitle = "",
      this.controller,
      this.textInputAction = TextInputAction.next,
      required this.color,
      this.isThereNoBottomPadding = false});

  @override
  _AppPasswordTextInputState createState() => _AppPasswordTextInputState();
}

class _AppPasswordTextInputState extends State<AppPasswordTextInput> {
  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemeChangeBloc, ThemeChangeState>(
      builder: (BuildContext context, ThemeChangeState themeState) {
        return
            // materialWrapper(
            Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title: widget.textInputTitle,
              textColor: Color(0xff5F6C6A),
              textSize: 14,
              fontWeight: FontWeight.w500,
            ),
            TextFormField(
              obscureText: _obscureText,
              keyboardType: TextInputType.visiblePassword,
              style: textStyleBlackColorNormal.copyWith(color: widget.color),
              validator: DIConstants.validators.passwordValidator,
              controller: widget.controller,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              cursorColor: widget.color,
//            initialValue: widget.textInputTitle,
              textInputAction: widget.textInputAction,
              decoration: textInputDecorator(widget.textInputTitle,
                      widget.color!, SizedBox(), 10.0, context, themeState)
                  .copyWith(
                suffixIcon: GestureDetector(
                    onTap: _toggle,
                    child: new Icon(
                      _obscureText ? LineIcons.eye : LineIcons.eyeSlash,
                      color: Theme.of(context).textTheme.bodyText1!.color,
                    )),
              ),
              // ),
            ).marginOnly(top: 10, bottom: 10),
          ],
        );
      },
    );
  }

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }
}

class AppDatePickerTextInputs extends StatelessWidget {
  final String textInputTitle;
  final TextEditingController controller;
  final int? maxLength;
  final TextInputType? textInputType;
  final Color? color;
  final bool autoFocus;
  final dynamic validateInput;
  final void Function()? onTapFunction;
  final void Function(String value)? onChange;
  final bool isReadOnly;
  final int? maxLine;
  final Widget? suffixIcon;
  final double borderRadius;
  final TextInputAction textInputAction;

  AppDatePickerTextInputs(
      {this.textInputTitle = "",
        required this.controller,
        this.maxLength = 0,
        this.textInputType,
        required this.color,
        this.autoFocus = false,
        this.validateInput,
        this.onTapFunction,
        this.isReadOnly = false,
        this.onChange,
        this.maxLine = 1,
        required this.suffixIcon,
        this.textInputAction = TextInputAction.next,
        this.borderRadius = 5.0});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemeChangeBloc, ThemeChangeState>(
      builder: (BuildContext context, ThemeChangeState themeState) {
        return TextFormField(
          decoration: textInputDecorator(
              textInputTitle, color!, suffixIcon, 10.0, context, themeState),
          cursorColor: transparentColor,
          style: textStyleBlackColorNormal.copyWith(
              color: Theme.of(context).textTheme.bodyText1!.color),
          keyboardType: textInputType,
          controller: controller,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          onTap: onTapFunction,
          readOnly: isReadOnly,
          onChanged: onChange,
          textInputAction: textInputAction,
        );
      },
    );
  }
}

class DashboardDatePicker extends StatefulWidget {
  final String text;
  final TextEditingController datePickerController;
  final bool isEndDate;
  final bool isLastDate;
  final int noOfYears;
  final TextEditingController? startDate;
  final bool hasNoTapFunction;

  DashboardDatePicker(
      {this.text = "",
        required this.datePickerController,
        this.isEndDate = false,
        this.startDate,
        this.isLastDate = false,
        this.hasNoTapFunction = false,
        this.noOfYears = 18});

  @override
  _DashboardDatePickerState createState() => _DashboardDatePickerState();
}

class _DashboardDatePickerState extends State<DashboardDatePicker> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomText(
          title: "Date of Birth",
          textColor: Color(0xff5F6C6A),
          textSize: 14,
          fontWeight: FontWeight.w500,
        ),
        AppDatePickerTextInputs(
            controller: widget.datePickerController,
            textInputType: TextInputType.number,
            onTapFunction: widget.hasNoTapFunction ? () {} : openDatePicker,
            // onChange: ,
            textInputTitle: widget.datePickerController.text.isEmpty
                ? widget.text
                : widget.datePickerController.text,
            isReadOnly: true,
            color: Color(0xff1B1C20).withOpacity(.2),
            validateInput: DIConstants.validators.validateString,
            suffixIcon: Icon(
              LineIcons.calendar,
              color: appPrimaryColor,
            )).marginSymmetric(vertical: 10),
      ],
    );
  }

  Future<String> openDatePicker() async {
    widget.datePickerController.text = await customDatePicker(
        Get.context, widget.datePickerController,
        isEndDate: widget.isEndDate,
        startDate: widget.startDate,
        isLastDate: widget.isLastDate,
        noOfYears: widget.noOfYears,
        dateFormat: DateFormat(StringConstants.DATE_FORMATTER));

    return widget.datePickerController.text;
  }
}

class PhoneNumberTextInput extends StatelessWidget {
  final TextEditingController controller;
  final Color? color;

  PhoneNumberTextInput({required this.controller, this.color});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemeChangeBloc, ThemeChangeState>(
      builder: (BuildContext context, ThemeChangeState themeState) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title: "Phone Number",
              textColor: Color(0xff5F6C6A),
              textSize: 14,
              fontWeight: FontWeight.w500,
            ),
            IntlPhoneField(
              decoration: textInputDecorator("Phone Number", color!, SizedBox(),
                  10.0, context, themeState, ""),
              controller: controller,
              style: textStyleBlackColorNormal.copyWith(color: color),
              initialCountryCode: 'NG',
              onChanged: (phone) {
                // controller.text = phone.completeNumber;
                getPrintLogInDifferentWays(phone.completeNumber);
              },
            ).marginOnly(top: 10, bottom: 10),
          ],
        );
      },
    );
  }
}

class PinTextInput extends StatefulWidget {
  final TextEditingController controller;
  final dynamic validation;
  final String? obscureText;
  final int fieldCount;
  final bool isTransactionPin;
  final void Function(String value)? onChange;

  PinTextInput(
      {required this.controller,
        required this.validation,
        this.fieldCount = 4,
        this.isTransactionPin = false,
        this.onChange,
        this.obscureText = "#"});

  @override
  _PinTextInputState createState() => _PinTextInputState();
}

class _PinTextInputState extends State<PinTextInput> {
  final FocusNode pinPutFocusNode = FocusNode();

  @override
  Widget build(BuildContext context) {

    return PinCodeTextField(
      appContext: context,
      length: widget.fieldCount,
      focusNode: pinPutFocusNode,
      controller: widget.controller,
      onCompleted: (value) {
        print("Completed $value");
        widget.controller.text = value;
        widget.onChange!(value);

      },
      // readOnly: true,
      beforeTextPaste: (text) {
        print("Allowing to paste $text");
        //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
        //but you can show anything you want here, like your pop up saying wrong paste format or etc
        return true;
      },
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: widget.validation,
      enabled: true,
      onChanged: (value) {
        print(value);
        widget.controller.text = value;
      },
      obscureText: widget.fieldCount == 4 ? true : false,
      animationType: AnimationType.fade,
      enableActiveFill: false,
      enablePinAutofill: false,
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.box,
        borderRadius: BorderRadius.circular(10),
        fieldHeight: 70,
        fieldWidth: 60,
        inactiveColor: appPrimaryColor,
        activeColor: Colors.white,
        activeFillColor: Colors.white,
      ),
      animationDuration: Duration(milliseconds: 300),
      textStyle: textStyleBlackColorNormal,
      keyboardType: TextInputType.number,
    );
  }

  BoxDecoration _pinPutDecoration = BoxDecoration(
    border: Border.all(color: appPrimaryDarkColor),
    borderRadius: BorderRadius.circular(15.0),
  );
}

InputDecoration textInputDecorator(String text, Color color, Widget? suffixIcon,
    double borderRadius, BuildContext context, ThemeChangeState themeState,
    [String suffixText = ""]) {
  return InputDecoration(
    suffixIcon: suffixIcon ??
        SizedBox(
          width: 0.0,
        ),
    // counterStyle: textStyleBlackColorNormal.copyWith(color: color),
    // labelText: text,
    border: InputBorder.none,
    prefix: suffixIcon ??
        SizedBox(
          width: 0.0,
        ),
    // hintText: suffixText,
    hintStyle: textStyleWhiteColorNormal,
    fillColor: Theme.of(context).textTheme.bodyText1!.color,
    labelStyle: textStyleBlackColorNormal.copyWith(
        color: Theme.of(context).textTheme.overline!.color),
    // filled: true,
    // counterText: suffixText,
    suffixText: suffixText,
    focusedErrorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: appErrorColor, width: 1.0),
        borderRadius: BorderRadius.all(Radius.circular(borderRadius))),
    errorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: appErrorColor, width: 1.0),
        borderRadius: BorderRadius.all(Radius.circular(borderRadius))),
    focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: color, width: 1.0),
        borderRadius: BorderRadius.all(Radius.circular(borderRadius))),
    // contentPadding: EdgeInsets.fromLTRB(80.0, 20.0, 20.0, 0.0),
    isDense: true,
    enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: color, width: 1.0), // Color(0xffE0DCDC)
        borderRadius: BorderRadius.all(Radius.circular(borderRadius))),
  );
}

Widget materialWrapper(Widget? widget,
    {double radius = 0, double height = 60.0, String suffixText = ""}) {
  return Container(
    height: suffixText != "" ? 65 : height,
    child: widget,
  );
}