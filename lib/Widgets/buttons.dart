import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppBloc/ThemeBloc/theme_bloc.dart';
import 'package:tuzz/Helpers/Others/get_theme_state.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';

class AppPrimaryDarkButtonRound extends StatelessWidget {
  final String? textTitle;
  final String? routeToGo;
  final Color? bgColor;
  final TextStyle? textStyle;
  final double? width;
  final double? borderRadius;
  final Object? arguments;
  final Function? functionToRun;
  final bool? isIcon;
  final Widget? icon;
  final bool? isFakeButton;
  final bool? hasPadding;
  final bool? isLoader;
  final bool? hasSuffixIcon;
  final Widget? suffixIcon;
  final double? fontSize;

  const AppPrimaryDarkButtonRound(
      {Key? key,
      this.textTitle = "",
      this.routeToGo,
      this.bgColor,
      this.textStyle,
      this.width = 0,
      this.arguments = true,
      this.functionToRun,
      this.isFakeButton = false,
      this.borderRadius = 20.0,
      this.isIcon = false,
      this.icon,
      this.hasPadding = true,
      this.isLoader = false,
      this.hasSuffixIcon = false,
      this.suffixIcon,
      this.fontSize = 15})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return BlocBuilder<ThemeChangeBloc, ThemeChangeState>(
      builder: (BuildContext context, ThemeChangeState themeState) {
        return AnimatedContainer(
          duration: Duration(seconds: 5),
          curve: Curves.bounceIn,
          child: Container(
            height: 50,
            decoration: _shadowDecoration(),
            child: Material(
              // elevation: 1.0,
              borderRadius: BorderRadius.circular(borderRadius!),
              color: bgColor ??
                  Theme.of(context)
                      .primaryColor, // isFakeButton ? appBrownGrayOneColor : bgColor,,
              child: MaterialButton(
                minWidth: width == 0 ? 0 : width,
                // padding: hasPadding
                //     ? EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0)
                //     : EdgeInsets.all(5.0),
                onPressed: isLoader!
                    ? () {}
                    : () {
                        routeToGo != null
                            ? Get.toNamed(routeToGo!)
                            : functionToRun!();
                      },
                child: isLoader!
                    ? Container(
                        height: 20,
                        width: 20,
                        child: Center(
                            child: CircularProgressIndicator(
                          backgroundColor: Colors.white,
                          strokeWidth: 2,
                        )))
                    : isIcon!
                        ? icon
                        : hasSuffixIcon!
                            ? Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    textTitle!,
                                    textAlign: TextAlign.center,
                                    style: textStyle != null ? textStyle
                                        : textStyleWhiteColorNormal.copyWith(
                                        fontWeight: FontWeight.normal,
                                        fontSize: fontSize),
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(left: 10),
                                      child: suffixIcon)
                                ],
                              )
                            : Text(
                                textTitle!,
                                textAlign: TextAlign.center,
                                style: textStyle != null ? textStyle
                                    : textStyleWhiteColorNormal.copyWith(
                                    fontWeight: FontWeight.normal,
                                    fontSize: fontSize),
                              ),
              ),
            ),
          ).paddingSymmetric(vertical: 10),
        );
      },
    );
  }

}

class AppOutlineButton extends StatelessWidget {
  final double? borderRadius;
  final double? width;
  final Color? borderColor;
  final TextStyle? textStyle;
  final String? textTitle;
  final String? routeToGo;
  final Object? arguments;
  final Function? functionToRun;
  final double? fontSize;

  AppOutlineButton(
      {this.borderRadius = 20.0,
      this.width = 0,
      this.borderColor,
      this.textTitle,
      this.routeToGo,
      this.arguments = true,
      this.functionToRun,
      this.textStyle,
      this.fontSize = 15});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: width == 0 ? 0 : width,
      height: 50,
      // decoration: BoxDecoration(
      //   border: Border.symmetric(horizontal: BorderSide(
      //     width: 2
      //   ))
      // ),
      child: OutlineButton(
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(borderRadius!),
        ),
        borderSide: BorderSide(
          color: borderColor!,
          style: BorderStyle.solid,
          width: 1.0,
        ),
        padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
        onPressed: () =>
            routeToGo != null ? Get.toNamed(routeToGo!) : functionToRun!(),
        child: Text(
          textTitle!,
          textAlign: TextAlign.center,
          style: textStyle!.copyWith(fontSize: fontSize, color: borderColor),
        ),
      ),
    ).paddingSymmetric(vertical: 10);
  }
}

BoxDecoration _shadowDecoration() {
  return BoxDecoration(
      boxShadow: [
        BoxShadow(
          color: Color(0xff000000).withOpacity(.5),
          blurRadius: 3.0, // soften the shadow
          spreadRadius: -7,
          offset: Offset(
            0, // Move to right 10  horizontally
            3.0, // Move to bottom 10 Vertically
          ),
        )
      ]
  );
}
