import 'package:flutter/material.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';

class NumericalKeyboard extends StatelessWidget {
  final KeyboardCallback? onKeyPressed;

  const NumericalKeyboard({Key? key, this.onKeyPressed}) : super(key: key);

  static const backspaceKey = 42;
  static const clearKey = 69;

  @override
  Widget build(BuildContext context) {
    return Table(
      defaultColumnWidth: IntrinsicColumnWidth(flex: 1.0),
      // border: TableBorder.all(),
      children: [
        TableRow(
          children: [
            _buildNumberKey(1),
            _buildNumberKey(2),
            _buildNumberKey(3),
          ],
        ),
        TableRow(
          children: [
            _buildNumberKey(4),
            _buildNumberKey(5),
            _buildNumberKey(6),
          ],
        ),
        TableRow(
          children: [
            _buildNumberKey(7),
            _buildNumberKey(8),
            _buildNumberKey(9),
          ],
        ),
        TableRow(
          children: [
            Container(),
            _buildNumberKey(0),
            _buildKey(Icon(Icons.arrow_back_ios, size: 15), backspaceKey),
          ],
        )
      ],
    );
  }

  Widget _buildNumberKey(int n) {
    return _buildKey(
        Text(
          '$n',
          style: textStyleBlackColorBold.copyWith(fontSize: 25),
        ),
        n);
  }

  Widget _buildKey(Widget icon, int key) {
    return IconButton(
      icon: icon,
      padding: EdgeInsets.all(25.0),
      onPressed: () => onKeyPressed!(key),
    );
  }
}

typedef KeyboardCallback(int key);
