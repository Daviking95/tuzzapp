
import 'package:flutter/material.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';

class BiometricIconWidget extends StatelessWidget {
  const BiometricIconWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      decoration: BoxDecoration(
        color: Color(0xffE7DDFF),
        borderRadius:
        BorderRadius.circular(10.0),
        boxShadow: [
          BoxShadow(
            color:
            appPrimaryColor.withAlpha(80),
            blurRadius: 10.0,
            // soften the shadow
            spreadRadius: -7,
            //extend the shadow
            offset: Offset(
              0,
              // Move to right 10  horizontally
              3.0, // Move to bottom 10 Vertically
            ),
          )
        ],
      ),
      child: Icon(
        Icons.fingerprint,
        color: appPrimaryColor,
      ),
    );
  }
}
