import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/Widgets/text.dart';

import 'more_info_snippets.dart';

Widget CompetitionParticipantsSnippet() {
  return SingleChildScrollView(
    child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
              title: "All Prizes",
              textSize: 20,
              fontWeight: FontWeight.w700,
              textColor: Color(0xff1B1C20),
            ),
            GestureDetector(onTap: () => Get.back(), child: Icon(Icons.close))
          ],
        ),
        SizedBox(
          height: 16,
        ),
        moreInfoItemSnippet("winner takes",
            DIConstants.moneyFormatter.returnFormattedMoney(10000, true)),
        moreInfoItemSnippet("RUNNER UP",
            DIConstants.moneyFormatter.returnFormattedMoney(10000, true)),
        moreInfoItemSnippet("3RD",
            DIConstants.moneyFormatter.returnFormattedMoney(10000, true)),
        moreInfoItemSnippet("4TH",
            DIConstants.moneyFormatter.returnFormattedMoney(10000, true)),
        moreInfoItemSnippet("5TH",
            DIConstants.moneyFormatter.returnFormattedMoney(10000, true)),
        moreInfoItemSnippet("6TH",
            DIConstants.moneyFormatter.returnFormattedMoney(10000, true)),
        moreInfoItemSnippet("7TH",
            DIConstants.moneyFormatter.returnFormattedMoney(10000, true)),
        SizedBox(
          height: 50,
        ),
      ],
    ),
  );
}
