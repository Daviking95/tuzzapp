import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/text.dart';

Widget withdrawTransactionSnippet(
    String iconString, String title, String subTitle, Function functionToRun,
    [bool hasCaret = false]) {
  return GestureDetector(
    onTap: () => functionToRun(),
    child: Container(
      decoration: BoxDecoration(
          color: Color(0xffEEF2F0), borderRadius: BorderRadius.circular(16)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ImageWidget.withSizeOnly(iconString, 40, 40),
          SizedBox(
            width: 25,
          ),
          Expanded(
            flex: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  title: title,
                  textSize: 16,
                  fontWeight: FontWeight.w600,
                  textColor: Color(0xff1B1C20),
                ),
                SizedBox(
                  height: 5,
                ),
                CustomText(
                  title: subTitle,
                  textSize: 12,
                  fontWeight: FontWeight.w400,
                  textColor: Color(0xff1B1C20),
                ),
              ],
            ),
          ),
          hasCaret
              ? Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.grey,
                )
              : Container()
        ],
      ).paddingSymmetric(horizontal: 20, vertical: 15),
    ).marginSymmetric(horizontal: 10),
  ).marginSymmetric(vertical: 5);
}

Widget selectAccountWidget(
    String iconString, String title, String subTitle, String bankName,
    [bool isSelected = false, hasDecoration = false, removeDivider = false]) {
  return GestureDetector(
    onTap: () => Get.back(),
    child: Container(
      width: Get.width,
      decoration: hasDecoration
          ? BoxDecoration(
              color: Color(0xffEEF2F0), borderRadius: BorderRadius.circular(15))
          : BoxDecoration(),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              ImageWidget.withSizeOnly(iconString, 20, 20),
              SizedBox(
                width: 20,
              ),
              Expanded(
                flex: 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(
                      title: title,
                      textSize: 14,
                      fontWeight: FontWeight.w600,
                      textColor: Color(0xff1B1C20),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    CustomText(
                      title: subTitle,
                      textSize: 12,
                      fontWeight: FontWeight.w400,
                      textColor: Color(0xff1B1C20),
                    ),
                  ],
                ),
              ),
              CustomText(
                title: bankName,
                textSize: 12,
                fontWeight: FontWeight.w500,
                textColor: Color(0xff1B1C20),
              ),
              Spacer(),
              isSelected
                  ? Icon(
                Icons.check,
                color: Colors.green,
              )
                  : SizedBox()
            ],
          ),
          removeDivider
              ? Container()
              : Divider(
                  height: 1,
                  color: Colors.black.withOpacity(.4),
                ).paddingSymmetric(vertical: 10)
        ],
      ).paddingSymmetric(horizontal: 10, vertical: hasDecoration ? 15 : 5),
    ),
  ).marginSymmetric(vertical: 5);
}
