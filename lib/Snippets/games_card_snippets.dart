import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppConstants/ApiResponseDataConstants/user_response_data_constants.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/Modules/CompetitionModule/Screens/join_fpl_screen.dart';
import 'package:tuzz/Modules/OnboardingModule/Screens/login_screen.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/bottom_sheet_widget.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/text.dart';

Widget FilterCardSnippet() {
  return Container(
    height: 37,
    width: 90,
    child: Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      color: Color(0xffEEF2F0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: CustomText(
              title: "Filter",
              textSize: 14,
              fontWeight: FontWeight.w400,
            ),
          ),
          CustomText(
            title: "80",
            textSize: 12,
            fontWeight: FontWeight.w900,
          ),
        ],
      ).paddingSymmetric(horizontal: 10),
    ),
  );
}

Widget GamesCardSnippet() {
  return GestureDetector(
    onTap: () => UserResponseDataConstants.IS_USER_LOGGEDIN
        ? Get.to(() => JoinFPLDetailsScreen())
        : modalBottomSheet(Get.context!, LoginScreen(), true, Get.height / 1.5),
    child: Container(
      height: 200,
      width: 150,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        border: Border.all(color: Color(0xff1B1C20).withOpacity(.1)),
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomText(
            title: DIConstants.moneyFormatter.returnFormattedMoney(26000),
            textSize: 20,
            fontWeight: FontWeight.w700,
          ),
          Spacer(),
          iconTitle("assets/images/svgs/games_card_users_icon.svg", "45"),
          iconTitle("assets/images/svgs/games_card_title_icon.svg",
              "Do am if e easy Do am if e easy"),
          iconTitle("assets/images/svgs/games_card_amount_icon.svg",
              DIConstants.moneyFormatter.returnFormattedMoney(10000)),
          SizedBox(
            height: 10,
          ),
        ],
      ).paddingAll(16),
    ).marginSymmetric(horizontal: 8),
  );
}

Widget XBoxGamesCardSnippet() {
  return Container(
    width: 150,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(16),
      border: Border.all(color: Color(0xff1B1C20).withOpacity(.1)),
      color: Colors.white,
    ),
    child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: CustomText(
                title: DIConstants.moneyFormatter.returnFormattedMoney(26000),
                textSize: 18,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            ImageWidget.withSizeOnly("assets/images/svgs/ps_icon.svg", 16, 16)
          ],
        ),
        Spacer(),
        iconTitle("assets/images/svgs/games_card_users_icon.svg", "45"),
        iconTitle(
            "assets/images/svgs/games_card_title_icon.svg", "Do am if e easy"),
        iconTitle("assets/images/svgs/games_card_location_icon.svg", "Online"),
        iconTitle("assets/images/svgs/games_card_amount_icon.svg",
            DIConstants.moneyFormatter.returnFormattedMoney(10000)),
      ],
    ).paddingAll(15),
  ).marginSymmetric(horizontal: 8);
}

Widget BoardGamesCardSnippet() {
  return Container(
    width: 150,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(16),
      border: Border.all(color: Color(0xff1B1C20).withOpacity(.1)),
      color: Colors.white,
    ),
    child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: CustomText(
                title: DIConstants.moneyFormatter.returnFormattedMoney(26000),
                textSize: 18,
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
        Spacer(),
        iconTitle("assets/images/svgs/games_card_users_icon.svg", "45"),
        iconTitle(
            "assets/images/svgs/games_card_title_icon.svg", "Do am if e easy"),
        iconTitle("assets/images/svgs/games_card_location_icon.svg", "Online"),
        iconTitle("assets/images/svgs/games_card_amount_icon.svg",
            DIConstants.moneyFormatter.returnFormattedMoney(10000)),
      ],
    ).paddingAll(15),
  ).marginSymmetric(horizontal: 8);
}

iconTitle(String iconString, String? title) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
      ImageWidget.withSizeOnly(iconString, 16, 16),
      SizedBox(
        width: 15,
      ),
      Expanded(
        child: Text(
          title!,
          softWrap: true,
          overflow: TextOverflow.ellipsis,
          style: textStyleBlackColorNormal.copyWith(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: Color(0xff1B1C20)),
        ),
      )
    ],
  ).marginSymmetric(vertical: 3);
}
