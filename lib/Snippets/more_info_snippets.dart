
import 'package:flutter/material.dart';
import 'package:tuzz/Widgets/text.dart';
import 'package:get/get.dart';

Widget moreInfoItemSnippet(String title, String? value) {
  return Column(
    children: [
      ListTile(
        leading: CustomText(
          title: title.toUpperCase(),
          textSize: 12,
          fontWeight: FontWeight.w500,
          textColor: Color(0xff5F6C6A),
        ),
        trailing: CustomText(
          title: value,
          textSize: 14,
          fontWeight: FontWeight.w400,
          textColor: Color(0xff1B1C20),
        ),
      ),
      Divider(
        height: 1,
        color: Colors.black.withOpacity(.4),
      ).paddingSymmetric(vertical: 5)
    ],
  );
}
