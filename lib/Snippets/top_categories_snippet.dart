import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Helpers/Others/print_log_formats.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/text.dart';

class TopCategorySnippet extends StatefulWidget {

  final String imageString;
  final String title;
  final List<dynamic> array;
  final dynamic passedIndexData;

  TopCategorySnippet(
      {
      required this.imageString,
      required this.title,
      required this.array, required this.passedIndexData});

  @override
  _TopCategorySnippetState createState() => _TopCategorySnippetState();
}

class _TopCategorySnippetState extends State<TopCategorySnippet> {

  Color color = Color(0xffDFFFF4);
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          // currentIndex = widget.index;
          color = Color(0xffDFFFF4);
        });

        getPrintLogInDifferentWays("logString ${widget.array.indexOf(widget.passedIndexData)}");
        getPrintLogInDifferentWays("currentIndex ${currentIndex}");
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 60,
            width: 60,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(100), color: widget.array.indexOf(widget.passedIndexData) == currentIndex
                ? Color(0xff1B1C20)
                : Color(0xffDFFFF4)),
            child: ImageWidget.withSizeOnly(widget.imageString, 24, 24, true)
                .paddingAll(15),
          ),
          SizedBox(
            height: 10,
          ),
          CustomText(
            title: widget.title,
            textSize: 10,
            fontWeight: FontWeight.w600,
            textColor: Color(0xff1B1C20),
          ),
        ],
      ).paddingSymmetric(horizontal: 5),
    );
  }
}
