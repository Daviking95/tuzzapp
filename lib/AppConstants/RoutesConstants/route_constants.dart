

class RoutesConstants {

  static const String STARTER_URL = '/';

  static const String LOGIN_URL = '/login';

  static const String FORGOT_PASSWORD_URL = '/forgot_password';

  static const String DASHBOARD_URL = "/dashboard";

  static const String JOIN_FPL_DETAILS_URL = "/join_fpl_details";

  static const String NOTIFICATION_URL = "/notifications";

}