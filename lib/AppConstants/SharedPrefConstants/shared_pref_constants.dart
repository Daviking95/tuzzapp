

class SharedPreferenceConstants{

  static const String EMAIL_VERIFY = '_EMAIL_VERIFY_';

  static const String SHARED_PREF_TOKEN = 'token';

  static const String SKIP_STARTER_SCREEN = 'skipStarterScreen';

  static const String REMEMBER_ME = 'rememberMe';

  static const String LOGIN_DETAILS = "loginDetails";

  static const String BIOMETRIC_TRANSACTION = "biometricTransactions";
}