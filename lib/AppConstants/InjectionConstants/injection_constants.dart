
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:tuzz/Helpers/Others/money_formatter.dart';
import 'package:tuzz/Helpers/Others/validators.dart';

class DIConstants {

  static MoneyFormatter moneyFormatter = new MoneyFormatter();

  static Validators validators = new Validators();
  static CurrencyTextInputFormatter currencyTextInputFormatter = CurrencyTextInputFormatter(symbol: "NGN", decimalDigits: 2);
}