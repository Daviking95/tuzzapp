

class ImageStringConstants {

  static const String APP_LOGO_PNG = 'assets/images/pngs/app_logos/logo_only.png';

  static const String SAD_FACE = 'assets/images/lottie_images/sad_face.json';
  static const String CONFIRM_IMAGE = "assets/images/lottie_images/confirm_img.json";
  static const String OTP_PIN_IMAGE = 'assets/images/lottie_images/otp_pin.json';

  static const String BTM_NAV_DASHBOARD_SVG = 'assets/images/svgs/btm_nav_icons/home.svg';
  static const String BTM_NAV_VAULT_SVG = 'assets/images/svgs/btm_nav_icons/notification.svg';
  static const String BTM_NAV_TRANSACTIONS_SVG = 'assets/images/svgs/btm_nav_icons/transaction.svg';
  static const String BTM_NAV_SUPPORT_SVG = 'assets/images/svgs/btm_nav_icons/profile.svg';
  static const String BTM_NAV_PROFILE_SVG = 'assets/images/svgs/btm_nav_icons/more.svg';

  // static const String APP_LOGO_PNG = 'assets/images/pngs/app_logos/logo_with_black_name.png';
}