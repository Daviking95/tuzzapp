class StringConstants {
  static const String APP_TITLE = "Tuzz";

  static const String INTER_FONT_FAMILY = "Inter";

  static const String CURRENCY = "₦";

  static const String DATE_FORMATTER = "yyyy-MM-dd";

  static const String ALREADY_HAVE_ACCOUNT = "I ALREADY HAVE AN ACCOUNT";
  static const String GET_STARTED = "GET STARTED";

  static const List<Map<String, String>> PAGE_VIEW_TEXT_LIST = [
    {
      "pageOneTitle": "Welcome to Tuzz",
      "pageOneSubTitle": "Let Tuzz take you on a cruise to \nfinancial freedom"
    },
    {
      "pageTwoTitle": "Welcome to Tuzz",
      "pageTwoSubTitle": "Let Tuzz take you on a cruise to \nfinancial freedom"
    },
    {
      "pageThreeTitle": "Welcome to Tuzz",
      "pageThreeSubTitle": "Let Tuzz take you on a cruise to \nfinancial freedom"
    },
  ];

  static const String EXIT_APP_TITLE = "Leaving Already ? ";
  static const String EXIT_APP_CONTENT =
      'We hate to see you leave... \nYou can try more of our products';

  static const String NO = "No, I am leaving";
  static const String YES = "Okay, I'll stay";

  static const String CANCEL = "Cancel";
  static const String PROCEED = "Proceed";
  static const String CONTINUE = "Continue";

  static const String OPEN_CAMERA = "Open Camera";
  static const String OPEN_GALLERY = "Open Gallery";

  static const String BTM_NAV_HOME = "Home";
  static const String BTM_NAV_VAULT = "Vault";
  static const String BTM_NAV_TRANSACTIONS = "Transactions";
  static const String BTM_NAV_SUPPORT = "Support";
  static const String BTM_NAV_PROFILE = "Profile";

  static const String BIOMETRICS_CANCEL_BUTTON = 'cancel';
  static const String BIOMETRICS_SETTINGS_BUTTON = 'settings';
  static const String BIOMETRICS_SETTINGS_DESC_BUTTON = 'Please set up your Touch ID.';
  static const String BIOMETRICS_LOCKOUT_DESC_BUTTON = 'Please re-enable your Touch ID';
  static const String BIOMETRICS_AUTH_REASON_DESC = "Place your finger on your fingerprint hardware to proceed";
  static const String BIOMETRICS_FIRST_TIME_LOGIN = "First Time Login Needed";
  static const String BIOMETRICS_FIRST_TIME_LOGIN_DESC = "You need to login with username and password first before setting up fingerprint authentication for this user";
  static const String BIOMETRICS_FINGERPRINT_NOT_SUPPORTED = "Fingerprint not supported";
  static const String BIOMETRICS_FINGERPRINT_NOT_SUPPORTED_DESC = "Your device does not support fingerprint authentication";

  static const String UNKNOWN_ERROR = 'Unknown Error';
  static const String ERROR_STRING = "Error!";
  static const String SUCCESS = "Success";

  static const String EMAIL = "Email";
  static const String PASSWORD = "Password";
  static const String KEEP_LOGGED_IN = "Keep me logged in ";
  static const String LOG_IN = "LOG INTO Tuzz";

  static const String OK = "Okay";

  static const String FORGOT_PASSWORD = "FORGOT PASSWORD?";


}
