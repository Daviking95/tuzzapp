import 'dart:async';

import 'package:catcher/catcher.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppConstants/StringConstants/app_constants.dart';
import 'package:tuzz/Shared/Controllers/main_controllers.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';

import 'AppBloc/ThemeBloc/theme_bloc.dart';
import 'AppConstants/RoutesConstants/route_constants.dart';
import 'Modules/DashboardModule/Screens/dashboard_screen.dart';
import 'Shared/Routers/router_generator.dart';
import 'rb_starter_functions.dart';

void main() async {
  await StarterFunctions().setupFunctions();
  List catchOptions = await StarterFunctions().errorCatcherSetup();
  final MainController _mainController = Get.put(MainController());

  Catcher(
      rootWidget: TuzzMainApp(navigatorKey : _mainController.navigatorKey),
      navigatorKey: _mainController.navigatorKey,
      debugConfig: catchOptions[0],
      releaseConfig: catchOptions[1]);

  // runApp(
  //   MultiRepositoryProvider(
  //     providers: RepositoryProviders.repositoryProviderList,
  //     child: MultiBlocProvider(
  //       providers: SharedBlocProviders.providersList,
  //       child: TuzzMainApp(),
  //     ),
  //   ),
  // );
}

class TuzzMainApp extends StatefulWidget {
  final Uri? uri;
  final navigatorKey;

  TuzzMainApp({Key? key, this.uri, this.navigatorKey}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TuzzMainAppState();
  }
}

class _TuzzMainAppState extends State<TuzzMainApp> {
  final MainController _mainController = Get.put(MainController());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: _mainController.handleUserInteraction,
      onPanDown: _mainController.handleUserInteraction,
      child: BlocProvider<ThemeChangeBloc>(
        create: (context) => ThemeChangeBloc(),
        child: BlocBuilder<ThemeChangeBloc, ThemeChangeState>(
          builder: (BuildContext context, ThemeChangeState themeState) {
            return GetMaterialApp(
              title: StringConstants.APP_TITLE,
              darkTheme: darkTheme,
              theme: lightTheme,
              themeMode: ThemeMode.system,
              debugShowCheckedModeBanner: false,
              key: widget.navigatorKey,
              getPages: RouteGenerator.routeList,
              home: DashboardScreen(),
            );
          },
        ),
      ),
    );
  }
}
