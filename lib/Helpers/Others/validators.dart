import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/AppConstants/StringConstants/app_constants.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/toasts_and_snacks.dart';


class Validators {
  bool isFieldEmpty(String fieldValue) => fieldValue.isEmpty;

  String? validateName(dynamic value) {
    if (value.trim().isEmpty) return 'Field is required.';
    final RegExp nameExp = new RegExp(r'^[A-za-z. ]+$');
    if (!nameExp.hasMatch(value.trim()))
      return 'Please enter only alphabetical characters.';
    return null;
  }

  String? validateEmail(dynamic value) {
    if (value!.trim().isEmpty) return 'Field is required.';
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern.toString());
    if (!regex.hasMatch(value.trim()))
      return 'Enter Valid Email';
    else
      return null;
  }

  bool passwordValidationToast(TextEditingController controller) {
    Pattern pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[#?!@$%^&*-]).{8,}$';
    RegExp regex = new RegExp(pattern.toString());
    if (controller.text.isEmpty) {
      showToast(
          toastMsg: 'Please enter password.',
          toastLength: Toast.LENGTH_LONG,
          toastGravity: ToastGravity.CENTER,
          bgColor: appPrimaryColor,
          txtColor: appWhiteColor);
      return false;
    } else if (!regex.hasMatch(controller.text.trim())) {
      showToast(
          toastMsg:
              'Password should contain at least one upper case, at least one lower case, at least one digit and at least one Special character and minimum of 8 characters',
          toastLength: Toast.LENGTH_LONG,
          toastGravity: ToastGravity.BOTTOM,
          bgColor: appPrimaryColor,
          txtColor: appWhiteColor);
      return false;
    } else {
      return true;
    }
  }

  List passwordValidationList = [
    8,
    r'(?=.*?[A-Z])',
    r'(?=.*?[0-9])',
    r'(?=.*?[#?!@$%^&*-])'
  ];

  List<String> dynamicPasswordValidationString = [
    'Password must be at least 8 digits long',
    'Password must have at least one capital letter',
    'Password must have at least one number',
    'Password must have at least one special character',
  ];

  final passwordValidator = MultiValidator([
    RequiredValidator(errorText: 'Password is required'),
    MinLengthValidator(8, errorText: 'Password must be at least 8 digits long'),
    PatternValidator(r'(?=.*?[A-Z])',
        errorText: 'Password must have at least one capital letter'),
    PatternValidator(r'(?=.*?[0-9])',
        errorText: 'Password must have at least one number'),
    PatternValidator(r'(?=.*?[#?!@$%^&*-])',
        errorText: 'Password must have at least one special character')
  ]);

  samePasswordValidation(val, password) =>
      MatchValidator(errorText: 'Passwords do not match')
          .validateMatch(val, password);

  String? validatePassword(String value) {
    if (value.trim().isEmpty || value.trim().length < 4)
      return 'Please enter password.';
    Pattern pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[#?!@$%^&*-]).{8,}$';
    RegExp regex = new RegExp(pattern.toString());

    // if (!regex.hasMatch(value.trim()))
    //   return 'Password should contain at least one upper case';
    // else
    // if (!RegExp(r'^(?=.*?[a-z])$').hasMatch(value.trim()))
    //   return 'Password should contain at least one lower case';
    // else
    // if (!RegExp(r'^(?=.*?[#?!@$%^&*-])$').hasMatch(value.trim()))
    //   return 'Password should contain at least one Special character';
    // else
    // if (!RegExp(r'^.{8,}$').hasMatch(value.trim()))
    //   return 'Password must have minimum of 8 characters';
    // else
    // if (!regex.hasMatch(value.trim()))
    //   return 'Password should contain at least one upper case, at least one lower case, at least one digit and at least one Special character and minimum of 8 characters';
    // else
    return null;
  }

  String? validateNumber(dynamic value) {
    if (value.trim().isEmpty) return 'Field is required.';
    final RegExp nameExp = new RegExp(r'^[-0-9 ]+$');
    if (!nameExp.hasMatch(value.trim()))
      return 'Please enter only numeric characters.';
//    if (value.trim().length != 11)
//      return 'Field must be of 11 digits';
    else
      return null;
  }

  String? validatePhoneNumber(dynamic value) {
    if (value.trim().isEmpty) return 'Field is required.';
    final RegExp nameExp = new RegExp(r'^(?:[+0])?[0-9]{10}$');
    if (!nameExp.hasMatch(value.trim()))
      return 'Please enter only correct phone number.';
    if (value.trim().length != 11)
      return 'Field must be of 11 digits';
    else
      return null;
  }

  String? validateString(dynamic value) {
    if (value == null || value.toString().trim().isEmpty)
      return 'Field is required.';
    else
      return null;
  }

  bool spinnerVaidation(TextEditingController controller, String text) {
    if (controller.text.isEmpty) {
      showToast(
          toastMsg: text,
          toastLength: Toast.LENGTH_LONG,
          toastGravity: ToastGravity.BOTTOM,
          bgColor: appPrimaryColor,
          txtColor: appWhiteColor);
      return false;
    } else {
      return true;
    }
  }

  String? validateRange(
      TextEditingController controller, String dataString, bool isMoney) {
    if (controller.text.toString().trim().isEmpty) return 'Field is required.';

    double amount = isMoney
        ? double.parse(controller.text.replaceAll(",", ""))
        : double.parse(controller.text);
    double minValue = double.parse(dataString.split("-")[0]);
    double maxValue = double.parse(dataString.split("-")[1]);

    print("Validating $amount $minValue $maxValue");

    if (amount < minValue) {
      print(
          "You cannot input less than ${DIConstants.moneyFormatter.returnFormattedMoney(minValue)}");
      return "You cannot input less than ${isMoney ? DIConstants.moneyFormatter.returnFormattedMoney(minValue) : minValue}";
    } else if (amount > maxValue) {
      print(maxValue);
      return "You cannot input more than ${isMoney ? DIConstants.moneyFormatter.returnFormattedMoney(maxValue) : maxValue}";
    }
    return null;
  }
}
