import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:tuzz/AppConstants/StringConstants/app_constants.dart';

class MoneyFormatter {
  // var format = NumberFormat.simpleCurrency(locale: Platform.localeName);

  String? returnFormattedMoney(double value, [bool isShowAll = false]) {
    MoneyFormatterOutput? fmf = new FlutterMoneyFormatter(
            amount: value,
            settings: MoneyFormatterSettings(
                symbol: StringConstants.CURRENCY,
                thousandSeparator: ',',
                decimalSeparator: '.',
                symbolAndNumberSeparator: ' ',
                fractionDigits: 2,
                compactFormatType: CompactFormatType.short))
        .output;

//     print(fmf!.nonSymbol); // 12,345,678.90
//     print(fmf.symbolOnLeft); // $ 12,345,678.90
//     print(fmf.symbolOnRight); // 12,345,678.90 $
//     print(fmf.fractionDigitsOnly); // 90
//     print(fmf.withoutFractionDigits); // 12,345,678
//
// // compact form
//     print(fmf.compactNonSymbol); // 12.3M
//     print(fmf.compactSymbolOnLeft); // $ 12.3M
//     print(fmf.compactSymbolOnRight); // 12.3M $

    String val = value.toString();
    if (isShowAll) return fmf!.symbolOnLeft;
    return val.length < 3 ? fmf!.symbolOnLeft : fmf!.compactSymbolOnLeft;
  }
}
