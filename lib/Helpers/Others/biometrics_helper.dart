import 'dart:developer';

import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/local_auth.dart';
import 'package:tuzz/AppConstants/SharedPrefConstants/shared_pref_constants.dart';
import 'package:tuzz/AppConstants/StringConstants/app_constants.dart';
import 'package:tuzz/Helpers/Others/shared_pref_helpers.dart';

class BiometricsScreen {
  static final LocalAuthentication _localAuthentication = LocalAuthentication();

  static const iosStrings = const IOSAuthMessages(
      cancelButton: StringConstants.BIOMETRICS_CANCEL_BUTTON,
      goToSettingsButton: StringConstants.BIOMETRICS_SETTINGS_BUTTON,
      goToSettingsDescription: StringConstants.BIOMETRICS_SETTINGS_DESC_BUTTON,
      lockOut: StringConstants.BIOMETRICS_LOCKOUT_DESC_BUTTON);

  static Future<bool> startBiometricLogin() async {
    if (!await readFromPref(SharedPreferenceConstants.LOGIN_DETAILS)) {
      return false;
    } else {
      return true;
    }
  }

  static Future<bool> startBiometricTransactions() async {
    if (!await readFromPref(SharedPreferenceConstants.BIOMETRIC_TRANSACTION)) {
      return false;
    } else {
      return true;
    }
  }

  static Future<bool> getBiometricsSupport() async {
    bool hasFingerPrintSupport = false;
    try {
      hasFingerPrintSupport = await _localAuthentication.canCheckBiometrics;

      return hasFingerPrintSupport;
    } catch (e) {
      print(e);
      return hasFingerPrintSupport;
    }
  }

  static Future<void> getAvailableSupport() async {
    List<BiometricType> availableBuimetricType = [];
    try {
      availableBuimetricType =
          await _localAuthentication.getAvailableBiometrics();
    } catch (e) {
      print(e);
    }
  }

  static Future<bool> authenticateMe() async {
    bool authenticated = false;
    await getAvailableSupport();

    try {
      authenticated = await _localAuthentication.authenticate(
        // sensitiveTransaction: true,
        biometricOnly: true,
        localizedReason: StringConstants.BIOMETRICS_AUTH_REASON_DESC,
        useErrorDialogs: true,
        stickyAuth: true,
      );

      return authenticated;
    } catch (e) {
      log(e.toString());
      return authenticated;
    }
  }
}
