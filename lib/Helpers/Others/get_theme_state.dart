
import 'package:get/get.dart';
import 'package:tuzz/Helpers/Others/print_log_formats.dart';

bool isThemeDark() {
  var themeState = Get.isDarkMode;
  getPrintLogInDifferentWays(themeState.toString());
  return themeState;
}