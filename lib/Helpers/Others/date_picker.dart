import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tuzz/AppConstants/StringConstants/app_constants.dart';

customDatePicker(
    BuildContext? context,
    TextEditingController controller,
    {bool isEndDate = false,
      TextEditingController? startDate,
      required DateFormat dateFormat,
      bool isLastDate = false,
      required int noOfYears}) async {
  final format = DateFormat(StringConstants.DATE_FORMATTER);

  DateTime dateTime = DateTime.now();

  DateTime? dateTimePicker = await showDatePicker(
    context: context!,
    initialDate: isLastDate
        ? DateFormat(StringConstants.DATE_FORMATTER).parse(
        format.format(DateTime(DateTime.now().year - (noOfYears * 2))))
        : DateFormat(StringConstants.DATE_FORMATTER)
        .parse(startDate?.text ?? format.format(dateTime)),
    firstDate: isLastDate
        ? DateFormat(StringConstants.DATE_FORMATTER)
        .parse(format.format(DateTime(DateTime.now().year - 100)))
        : isEndDate
        ? DateFormat(StringConstants.DATE_FORMATTER)
        .parse(startDate?.text ?? format.format(dateTime) )
        : dateTime.subtract(Duration(days: 365000)),
    lastDate: isLastDate
        ? DateFormat(StringConstants.DATE_FORMATTER)
        .parse(format.format(DateTime(DateTime.now().year - noOfYears)))
        : dateTime.add(Duration(days: 365000)),
    currentDate:
    isLastDate ? DateTime(DateTime.now().year - noOfYears) : DateTime.now(),
  );

  controller.text = dateFormat.format(dateTimePicker!);

  return controller.text;
}

Future<List<TextEditingController>> customDateRangePicker(
    BuildContext context, List<TextEditingController> controller, bool isStart,
    {bool isEndDate = false,
      TextEditingController? startDate,
      DateFormat? dateFormat}) async {
  final format = DateFormat(StringConstants.DATE_FORMATTER);

  DateTime dateTime = DateTime.now();

  DateTimeRange? dateTimePicker = await showDateRangePicker(
    context: context,
    firstDate: isEndDate
        ? DateFormat(StringConstants.DATE_FORMATTER).parse(startDate!.text)
        : dateTime.subtract(Duration(minutes: 365000)),
    lastDate: DateTime.now(),
    currentDate: DateTime.now(),
  );

  controller[0].text = dateFormat != null
      ? dateFormat.format(dateTimePicker!.start)
      : format.format(dateTimePicker!.start);

  controller[1].text = dateFormat != null
      ? dateFormat.format(dateTimePicker.end)
      : format.format(dateTimePicker.end);

  return controller;
}

bool isAdult2(String birthDateString) {
  String datePattern = "dd-MM-yyyy";

  // Current time - at this moment
  DateTime today = DateTime.now();

  // Parsed date to check
  DateTime birthDate = DateFormat(datePattern).parse(birthDateString);

  // Date to check but moved 18 years ahead
  DateTime adultDate = DateTime(
    birthDate.year + 18,
    birthDate.month,
    birthDate.day,
  );

  return adultDate.isBefore(today);
}
