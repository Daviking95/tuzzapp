

import 'dart:developer';

getPrintLogInDifferentWays(String logString) {

  log("PRINTING IN LOG : $logString");
  print("PRINTING IN PRINTS : $logString");
  _printWrapped("PRINTING IN PRINT_WRAPPED : $logString");
}


void _printWrapped(String text) {
  final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
  pattern.allMatches(text).forEach((match) => print(match.group(0)));
}