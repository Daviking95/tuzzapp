import 'package:catcher/catcher.dart';
import 'flavors.dart';
import 'main.dart';
import 'package:get/get.dart';
import 'Shared/Controllers/main_controllers.dart';
import 'rb_starter_functions.dart';

void main() async {
  F.appFlavor = Flavor.PROD;
  await StarterFunctions().setupFunctions();
  List catchOptions = await StarterFunctions().errorCatcherSetup();
  final MainController _mainController = Get.put(MainController());
  Catcher(
      rootWidget: TuzzMainApp(navigatorKey : _mainController.navigatorKey ),
      navigatorKey: _mainController.navigatorKey,
      debugConfig: catchOptions[0],
      releaseConfig: catchOptions[1]);

  // runApp(MultiRepositoryProvider(
  //   providers: RepositoryProviders.repositoryProviderList,
  //   child: MultiBlocProvider(
  //     providers: SharedBlocProviders.providersList,
  //     child: TuzzMainApp(),
  //   ),
  // ));
}
