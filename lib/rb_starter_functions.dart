import 'dart:async';
import 'dart:io';

import 'package:catcher/catcher.dart';
import 'package:flutter/foundation.dart';
import 'package:firebase_core/firebase_core.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:tuzz/Shared/Services/PushNotificationServices/rb_fcm_services.dart';
import 'package:uni_links/uni_links.dart' as UniLinks;

import 'Shared/Services/DeeplinkingServices/rb_dynamic_link_services.dart';
import 'flavors.dart';
import 'main.dart';

class StarterFunctions {
  setupFunctions() async {
    WidgetsFlutterBinding.ensureInitialized();
    // await _firebasePushNotificationSetup();
    await _deeplinkingSetup();
    await _envSetup();
    await _hydratedBlocSetup();

    // await Intercom.initialize(
    //     appId: 'pwta5071',
    //     iOSApiKey: 'ios_sdk-3b929daff10f72d82e3cb2aa89b4d65dc468a2a2',
    //     androidApiKey: 'android_sdk-6d6bf312895de81dd592b3995404d12be0019a1b');
    // Intercom.registerUnidentifiedUser();
  }

  _firebasePushNotificationSetup() async {
    await Firebase.initializeApp();

    // FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
    //
    // await flutterLocalNotificationsPlugin
    //     .resolvePlatformSpecificImplementation<
    //         AndroidFlutterLocalNotificationsPlugin>()
    //     ?.createNotificationChannel(channel);
  }

  _deeplinkingSetup() async {
    await _checkDeepLinking();
    // DynamicLinkService().handleDynamicLinks();
  }

  _checkDeepLinking() async {
    await UniLinks.getInitialUri();

    StreamController<double> controller = StreamController<double>();
    Stream stream = controller.stream;

    StreamSubscription _sub = stream.listen((value) {
      print('Value from controller: $value');
    });

    try {
      _sub = UniLinks.getUriLinksStream().listen((Uri? uri) {
        // print('uri: $uri');
        WidgetsFlutterBinding.ensureInitialized();
        runApp(TuzzMainApp(
          uri: uri,
        ));
      });
    } on PlatformException {
      print('platform error');
      _sub.cancel();
    } on HttpException {
      print('http exception');
      _sub.cancel();
    } on SocketException {
      print('socket exception');
      _sub.cancel();
    } catch (err) {
      print('error');
      _sub.cancel();
    }
  }

  _envSetup() async {
    await DotEnv().load(fileName: '.env');
  }

  _hydratedBlocSetup() async {
    HydratedBloc.storage = await HydratedStorage.build(
      storageDirectory: await getTemporaryDirectory(),
    );
  }

  errorCatcherSetup() {
    CatcherOptions debugOptions = CatcherOptions(DialogReportMode(), [
      ConsoleHandler(),

      ToastHandler(
          gravity: ToastHandlerGravity.bottom,
          length: ToastHandlerLength.long,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          textSize: 12.0,
          customMessage: "We are sorry but unexpected error occured."),

      // todo : Use this only in development and set up the SMTP correctly. The user setup will receive the email
      EmailAutoHandler("smtp.gmail.com", 587, "somefakeemail@gmail.com",
          "Catcher", "FakePassword", ["myemail@gmail.com"])
    ]);

    CatcherOptions releaseOptions = CatcherOptions(DialogReportMode(), [
      EmailManualHandler(["daviking95@gmail.com"],
          enableDeviceParameters: true,
          enableStackTrace: true,
          enableCustomParameters: true,
          enableApplicationParameters: true,
          sendHtml: true,
          emailTitle: "Crash Report On ${F.title}",
          emailHeader: "Submitting a crash report on ${F.title}",
          printLogs: true),

      ToastHandler(
          gravity: ToastHandlerGravity.bottom,
          length: ToastHandlerLength.long,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          textSize: 12.0,
          customMessage: "We are sorry but unexpected error occured."),

      // todo : Use this to send log error to a server somewhere
      HttpHandler(HttpRequestType.post, Uri.parse("http://logs.server.com"),
          headers: {"header": "value"}, requestTimeout: 4000, printLogs: false)
    ]);

    return [debugOptions, releaseOptions];
  }

}
