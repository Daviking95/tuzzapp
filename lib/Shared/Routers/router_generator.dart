import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:tuzz/AppConstants/RoutesConstants/route_constants.dart';
import 'package:tuzz/Modules/CompetitionModule/Bindings/competition_bindings.dart';
import 'package:tuzz/Modules/CompetitionModule/Screens/join_fpl_screen.dart';
import 'package:tuzz/Modules/DashboardModule/Bindings/dashboard_binding.dart';
import 'package:tuzz/Modules/DashboardModule/Screens/dashboard_screen.dart';
import 'package:tuzz/Modules/NotificationModule/Bindings/notification_binding.dart';
import 'package:tuzz/Modules/NotificationModule/Screens/notification_screen.dart';
import 'package:tuzz/Modules/OnboardingModule/Bindings/onboarding_binding.dart';
import 'package:tuzz/Modules/OnboardingModule/Screens/login_screen.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({WidgetBuilder? builder, RouteSettings? settings})
      : super(builder: builder!, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.name == RoutesConstants.STARTER_URL) return child;
    if (animation.status == AnimationStatus.reverse)
      return super
          .buildTransitions(context, animation, secondaryAnimation, child);
    return FadeTransition(opacity: animation, child: child);
  }
}

class RouteGenerator {
  static List<GetPage<dynamic>> routeList = [
    GetPage(
        name: RoutesConstants.DASHBOARD_URL,
        page: () => DashboardScreen(),
        binding: DashboardBinding()),
    GetPage(
        name: RoutesConstants.JOIN_FPL_DETAILS_URL,
        page: () => JoinFPLDetailsScreen(),
        binding: CompetitionBinding()),
    GetPage(
        name: RoutesConstants.LOGIN_URL,
        page: () => LoginScreen(),
        binding: OnboardingBinding()),
    GetPage(
        name: RoutesConstants.NOTIFICATION_URL,
        page: () => NotificationScreen(),
        binding: NotificationBinding()),
  ];

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: appWhiteColor,
          title: Text('Error'),
        ),
        body: Center(
          child: Text(
            'Error : Page Not Found',
            style: textStylePrimaryColorBold,
          ),
        ),
      );
    });
  }
}
