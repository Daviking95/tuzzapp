//
// import 'package:firebase_core/firebase_core.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';
//
// //Helper Links : https://www.freecodecamp.org/news/local-notifications-in-flutter/
// // https://stackoverflow.com/questions/57891871/flutter-firebase-message-plugin-setup-errors
// // https://pub.dev/packages/flutter_local_notifications#example-app
//
// Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
//
//   await Firebase.initializeApp();
//
//   print('Handling a background message ${message.messageId}');
//   print(message.data);
//
//   flutterLocalNotificationsPlugin.show(
//       message.data.hashCode,
//       message.data['title'],
//       message.data['body'],
//       NotificationDetails(
//         android: AndroidNotificationDetails(
//           channel.id,
//           channel.name,
//           channel.description,
//         ),
//       ));
// }
//
// const AndroidNotificationChannel channel = AndroidNotificationChannel(
//   'high_importance_channel', // id
//   'High Importance Notifications', // title
//   'This channel is used for important notifications.', // description
//   importance: Importance.high,
// );
//
// /// Initialize the [FlutterLocalNotificationsPlugin] package.
// final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
// FlutterLocalNotificationsPlugin();
