import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppConstants/StringConstants/app_constants.dart';
import 'package:tuzz/Helpers/Enums/app_enum.dart';
import 'package:tuzz/Helpers/Others/get_theme_state.dart';
import 'package:tuzz/Shared/Controllers/main_controllers.dart';


const Color appPrimaryColor = const Color(0XFF11A683);
const Color appPrimaryDarkColor = const Color(0XFF100231);
const Color appSecondaryColor = const Color(0XFFFFFFFF);
const Color darkThemeBgColor = Color(0xff100231);

const Color appErrorColor = const Color(0XFFef6a6e);

const Color appWhiteColor = const Color(0XFFffffff);
const Color appWhiteTwoColor = const Color(0XFFfafafa);

const Color appBlackColor = const Color(0XFF212121);
const Color appBlackTwoColor = const Color(0XFF303030);

const Color appBrownGrayOneColor = const Color(0XFF626262);
const Color appBrownGrayTwoColor = const Color(0XFF777777);
const Color appBrownGrayThreeColor = const Color(0XFFeeeeee);

const Color appBackgroundColor = const Color(0XFFF0F4F7);
const Color appActiveColor = const Color(0XFFef6a6e);
const Color transparentColor = const Color.fromRGBO(0, 0, 0, 0.2);



class AppThemes {
  static final appThemeData = {

    AppTheme.LIGHT_THEME: ThemeData(
      scaffoldBackgroundColor: Colors.white,
      // primarySwatch: Colors.green,
      primaryColor: appPrimaryColor,
      fontFamily: StringConstants.INTER_FONT_FAMILY,
      accentColor: appSecondaryColor,
      buttonColor: appPrimaryColor,
      errorColor: appErrorColor,
      highlightColor: appActiveColor,
      buttonTheme: appButtonTheme,
      backgroundColor: Colors.white, // Color(0xffE5E5E5),
      // primaryTextTheme: _buildTextTheme(base.primaryTextTheme),
      // accentTextTheme: _buildTextTheme(base.accentTextTheme),
      iconTheme: new IconThemeData(color: appPrimaryColor),
      textTheme: TextTheme(
        bodyText1: TextStyle(
          color: Colors.black,
        ),
      ),
      // textTheme: _buildTextTheme(base.textTheme),
    ),


    AppTheme.DARK_THEME: ThemeData(
      scaffoldBackgroundColor: darkThemeBgColor,
      // primarySwatch: Colors.teal,
      primaryColor: appPrimaryColor,
      backgroundColor: Colors.black,
      fontFamily: StringConstants.INTER_FONT_FAMILY,
      accentColor: appSecondaryColor,
      buttonColor: appPrimaryColor,
      errorColor: appErrorColor,
      highlightColor: appActiveColor,
      buttonTheme: appButtonTheme,
      // primaryTextTheme: _buildTextTheme(base.primaryTextTheme),
      // accentTextTheme: _buildTextTheme(base.accentTextTheme),
      iconTheme: new IconThemeData(color: appPrimaryColor),
      textTheme: TextTheme(
        bodyText1: TextStyle(
          color: Colors.white,
        ),
      ),
    )
  };
}



final ThemeData lightTheme = _buildLightTheme();
final ThemeData darkTheme = _buildDarkTheme();

ThemeData _buildLightTheme() {
  final ColorScheme colorScheme = const ColorScheme.light().copyWith(
    primary: appPrimaryColor,
    secondary: appSecondaryColor,
  );
  final ThemeData base = ThemeData(
    brightness: Brightness.light,
    accentColorBrightness: Brightness.dark,
    colorScheme: colorScheme,
    primaryColor: appPrimaryColor,
    primaryColorDark: appWhiteColor,
    buttonColor: appPrimaryColor,
    indicatorColor: Color(0xff6A5C8A),
    toggleableActiveColor: appSecondaryColor,
    splashColor: Colors.white24,
    splashFactory: InkRipple.splashFactory,
    accentColor: appSecondaryColor,
    canvasColor: Colors.white,
    scaffoldBackgroundColor: Colors.white,
    backgroundColor: Colors.white, // Color(0xfff8fcfd),
    errorColor: const Color(0xFFB00020),
    focusColor: const Color(0xffC1C1C3),
    buttonTheme: ButtonThemeData(
      colorScheme: colorScheme,
      textTheme: ButtonTextTheme.primary,
    ),
    textTheme: TextTheme(
        bodyText1: TextStyle(color: Color(0xff262C31)),
        bodyText2: TextStyle(
          color: Color(0xff4D4D4D),
          decorationColor: Color(0xff3457AD),
        ),
        headline1: TextStyle(color: Color(0xff263743)),
        overline: TextStyle(
          color: Color(0xffabaaaa),
        ),
        subtitle1: TextStyle(color: Color(0xff333333)),
        subtitle2: TextStyle(color: Color(0xff262C31))),
  );
  return base;
}

ThemeData _buildDarkTheme() {
  final ColorScheme colorScheme = const ColorScheme.dark().copyWith(
    primary: appPrimaryColor,
    secondary: appSecondaryColor,
  );
  final ThemeData base = ThemeData(
    brightness: Brightness.dark,
    accentColorBrightness: Brightness.dark,
    primaryColor: appPrimaryColor,
    cardColor: Color(0xFF121A26),
    primaryColorDark: appPrimaryDarkColor,
    primaryColorLight: appSecondaryColor,
    buttonColor: appPrimaryColor,
    indicatorColor: Color(0xff6A5C8A),
    toggleableActiveColor: appSecondaryColor,
    accentColor: appSecondaryColor,
    canvasColor: const Color(0xFF2A4058),
    scaffoldBackgroundColor: const Color(0xFF121A26),
    backgroundColor: Color(0xfff8fcfd),
    focusColor: const Color(0xff342850),
    errorColor: const Color(0xFFB00020),
    buttonTheme: ButtonThemeData(
      colorScheme: colorScheme,
      textTheme: ButtonTextTheme.primary,
    ),
    textTheme: TextTheme(
        bodyText1: TextStyle(
          color: Colors.white,
        ),
        bodyText2:
        TextStyle(color: Colors.white, decorationColor: Colors.white),
        overline: TextStyle(
          color: Colors.white,
        ),
        headline1: TextStyle(
          color: Colors.white,
        ),
        subtitle1: TextStyle(color: Color(0xff6A5C8A)),
        subtitle2: TextStyle(color: appPrimaryColor)),
  );
  return base;
}


final ButtonThemeData appButtonTheme = ButtonThemeData(
    buttonColor: appPrimaryColor,
//    highlightColor: appSecondaryColor,
    disabledColor: appBrownGrayOneColor,
    textTheme: ButtonTextTheme.primary,
    focusColor: appSecondaryColor);

TextStyle textStylePrimaryColorBold = TextStyle(
    color: Get.theme.primaryColor,
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    fontFamily: StringConstants.INTER_FONT_FAMILY);

TextStyle textStylePrimaryColorNormal = TextStyle(
    color: Get.theme.primaryColor,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
    fontFamily: StringConstants.INTER_FONT_FAMILY);

TextStyle textStylePrimaryDarkColorNormal = TextStyle(
    color: Get.theme.primaryColorDark,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
    fontFamily: StringConstants.INTER_FONT_FAMILY);

TextStyle textStyleSecondaryColorBold = TextStyle(
    color: appSecondaryColor,
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    fontFamily: StringConstants.INTER_FONT_FAMILY);

TextStyle textStyleSecondaryColorNormal = TextStyle(
    color: appSecondaryColor,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
    fontFamily: StringConstants.INTER_FONT_FAMILY);

TextStyle textStyleBlackColorBold = TextStyle(
    color: appBlackColor,
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    fontFamily: StringConstants.INTER_FONT_FAMILY);

TextStyle textStyleBlackColorNormal = TextStyle(
    color: appBlackColor,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
    fontFamily: StringConstants.INTER_FONT_FAMILY);

TextStyle textStyleGrayColorBold = TextStyle(
    color: appBrownGrayOneColor,
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    fontFamily: StringConstants.INTER_FONT_FAMILY);

TextStyle textStyleGrayColorNormal = TextStyle(
    color: appBrownGrayOneColor,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
    fontFamily: StringConstants.INTER_FONT_FAMILY);

TextStyle textStyleWhiteColorNormal = TextStyle(
    color: appWhiteColor,
    fontSize: 14.0,
    fontWeight: FontWeight.w600,
    fontFamily: StringConstants.INTER_FONT_FAMILY);
