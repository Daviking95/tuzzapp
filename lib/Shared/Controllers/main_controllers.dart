import 'dart:async';
import 'dart:developer';

import 'package:catcher/catcher.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppConstants/RoutesConstants/route_constants.dart';
import 'package:tuzz/AppConstants/SharedPrefConstants/shared_pref_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainController extends GetxController {
  final _navigatorKey = Catcher.navigatorKey; // GlobalKey<NavigatorState>();
  bool _isSkipStarterScreen = false;
  Timer? _timer;

  get navigatorKey => _navigatorKey;

  Timer? get timer => _timer;

  bool get isSkipStarterScreen => _isSkipStarterScreen;

  void initializeTimer() {
    if (_timer != null) {
      _timer?.cancel();
    }

    _timer = Timer(const Duration(minutes: 50), logOutUser);
  }

  void logOutUser() async {
    _timer?.cancel();
    _timer = null;

  }

  void handleUserInteraction([_]) {
    FocusManager.instance.primaryFocus?.unfocus();
    initializeTimer();
  }

  deepLinkingWidgetToReturn(Uri? uri, Widget? child) {
    Map<String, String> query = {};

    var email = query['email'];
    var token = query['token'];

    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      if (uri != null) {
        var path = '';
        if (uri.hasQuery) {
          query = uri.queryParameters;
          log('queryParameters $query');
        }
        path = uri.path;
        if (path == '/register') {

        }
        if (path == '/login') {
          log('token $email ${email} $token');

          SharedPreferences _pref = await SharedPreferences.getInstance();
          await _pref.setString(SharedPreferenceConstants.EMAIL_VERIFY, "true");
          Get.toNamed(RoutesConstants.LOGIN_URL,
              arguments: {'email': email, 'token': token});
        }
        if (path == '/resetlink') {
          log('resetlinktoken $email ${email} $token');

          Get.toNamed(RoutesConstants.FORGOT_PASSWORD_URL,
              arguments: {'email': email, 'token': token});
        }
        if (path == '/forgot_password') {
          log('resetlinktoken $email ${email} $token');

          Get.toNamed(RoutesConstants.FORGOT_PASSWORD_URL,
              arguments: {'email': email, 'token': token});
        }
        if (path == '/') {
          Get.toNamed(RoutesConstants.LOGIN_URL);
        }
      } else {
        Get.toNamed(RoutesConstants.LOGIN_URL);
      }
    });
    return child;
  }
}
