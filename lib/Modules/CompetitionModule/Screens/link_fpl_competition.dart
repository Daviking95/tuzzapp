import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Helpers/Enums/categories_enum.dart';
import 'package:tuzz/Helpers/Others/print_log_formats.dart';
import 'package:tuzz/Modules/CompetitionModule/Controllers/competition_controller.dart';
import 'package:tuzz/Modules/CompetitionModule/Screens/paste_fpl_link_screen.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/starter_widget.dart';
import 'package:tuzz/Widgets/text.dart';

class LinkFPLCompetitionScreen extends StatefulWidget {
  final CategoriesEnum categoriesEnum;

  LinkFPLCompetitionScreen(this.categoriesEnum);

  @override
  _LinkFPLCompetitionScreenState createState() =>
      _LinkFPLCompetitionScreenState();
}

class _LinkFPLCompetitionScreenState extends State<LinkFPLCompetitionScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompetitionController>(
      init: CompetitionController(),
      builder: (controller) {
        return StarterWidget(
            hasTopWidget: false,
            hasPagePadding: true,
            isExitApp: false,
            isBgAllowed: false,
            hasBottomNav: false,
            hasRefresh: false,
            // startLoader: controller.startLoader,
            bottomPageContent: _bottomPageContent());
      },
    );
  }

  _bottomPageContent() {
    return GetBuilder<CompetitionController>(
      init: CompetitionController(),
      builder: (controller) {
        return SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                      onTap: () => controller.counter == 1
                          ? Get.back()
                          : controller.counter--,
                      child: Icon(Icons.arrow_back_ios, size: 15).paddingAll(10)),
                  SizedBox(
                    width: 15,
                  ),
                  CustomText(
                    title: "Link your FPL League",
                    textSize: 15,
                    fontWeight: FontWeight.w400,
                    textColor: Color(0xff1B1C20),
                  ),
                ],
              ),
              SizedBox(
                height: 40,
              ),
              CustomText(
                title: "Link your fantasy premier \nleague account to continue",
                textColor: Color(0xff1B1C20),
                textSize: 20,
                isCenter: true,
                fontWeight: FontWeight.w700,
              ),
              SizedBox(
                height: 15,
              ),
              CustomText(
                title:
                    "We need to access your score and league details on \nfantasy premier league to be able to award your prizes",
                textSize: 12,
                isCenter: true,
                fontWeight: FontWeight.w400,
              ),
              SizedBox(
                height: 25,
              ),
              CustomText(
                title: "How to get link your league \nin 3 steps",
                textColor: Color(0xff1B1C20),
                textSize: 16,
                isCenter: true,
                fontWeight: FontWeight.w600,
              ),
              SizedBox(
                height: 40,
              ),
              CustomText(
                title: "STEP ${controller.counter}",
                textSize: 12,
                isCenter: true,
                fontWeight: FontWeight.w500,
              ),
              SizedBox(
                height: 25,
              ),
              Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16)),
                elevation: 0,
                color: Color(0xffFFDDF7),
                child: Container(
                  child: _showCardBasedOnNumber(controller.counter),
                ).paddingAll(15),
              ),
              SizedBox(
                height: 60,
              ),
              AppPrimaryDarkButtonRound(
                  width: Get.width,
                  // isLoader: state is SignUpLoading ? true : false,
                  borderRadius: 10.0,
                  textTitle: controller.counter < 3 ? "Next" : "I understand",
                  bgColor: appPrimaryColor,
                  textStyle: textStyleWhiteColorNormal,
                  functionToRun: () {
                    if (controller.counter == 3)
                      Get.to(() => PasteFPLLinkScreen());
                    else
                      controller.counter++;
                  }),
            ],
          ).paddingSymmetric(vertical: 20),
        );
      },
    );
  }

  _showCardBasedOnNumber(int counter) {
    switch (counter) {
      case 1:
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title:
                  "Go to the link below and sign into on your browser and click on points",
              textSize: 14,
              fontWeight: FontWeight.w400,
            ),
            SizedBox(
              height: 16,
            ),
            CustomText(
              title: "Fantasy.premierleague.com",
              textSize: 14,
              textDecoration: TextDecoration.underline,
              fontWeight: FontWeight.w400,
            ),
            SizedBox(
              height: 16,
            ),
            ImageWidget.withSizeOnly(
                "assets/images/pngs/link_fpl_1.png", 145, 270, true)
          ],
        );
      case 2:
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title: "Scroll down and Click on view game week history",
              textSize: 14,
              fontWeight: FontWeight.w400,
            ),
            SizedBox(
              height: 16,
            ),
            ImageWidget.withSizeOnly(
                "assets/images/pngs/link_fpl_2.png", 280, 270, true)
          ],
        );
      case 3:
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title: "Copy the link at the top of the page",
              textSize: 14,
              fontWeight: FontWeight.w400,
            ),
            SizedBox(
              height: 30,
            ),
            ImageWidget.withSizeOnly(
                "assets/images/pngs/link_fpl_3.png", 320, 270, true)
          ],
        );
    }
  }
}
