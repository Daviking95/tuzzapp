
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/Modules/CompetitionModule/Controllers/competition_controller.dart';
import 'package:tuzz/Modules/CompetitionModule/Screens/show_fpl_link_result_screen.dart';
import 'package:tuzz/Modules/DashboardModule/Screens/dashboard_screen.dart';
import 'package:tuzz/Modules/DashboardModule/Screens/no_data_widget.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/inputs.dart';
import 'package:tuzz/Widgets/starter_widget.dart';
import 'package:tuzz/Widgets/text.dart';

import 'fpl_payment_confirmation_screen.dart';
import 'join_fpl_success_screen.dart';
import 'my_competitions_screen.dart';
import 'paste_fpl_link_screen.dart';

class FPLJoinSuccessScreen extends StatefulWidget {
  const FPLJoinSuccessScreen({Key? key}) : super(key: key);

  @override
  _FPLJoinSuccessScreenState createState() => _FPLJoinSuccessScreenState();
}

class _FPLJoinSuccessScreenState extends State<FPLJoinSuccessScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompetitionController>(
      init: CompetitionController(),
      builder: (controller) {
        return StarterWidget(
            hasTopWidget: false,
            hasPagePadding: true,
            isExitApp: false,
            isBgAllowed: false,
            hasBottomNav: false,
            hasRefresh: false,
            // startLoader: controller.startLoader,
            bottomPageContent: _bottomPageContent());
      },
    );
  }

  _bottomPageContent() {
    return GetBuilder<CompetitionController>(
      init: CompetitionController(),
      builder: (controller) {
        return SingleChildScrollView(
          child: Container(
            height: Get.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
               SizedBox(height: 50,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Lottie.asset("assets/images/lottie_images/success.json", width: 140),
                    SizedBox(height: 10,),

                    CustomText(
                      title: "Good luck, Ore!",
                      isCenter: true,
                      textSize: 18,
                      fontWeight: FontWeight.w700,
                      textColor: Color(0xff1B1C20),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    CustomText(
                      title:
                      "You have successfully joined Do am if e easy competition with ₦10,000, \nWishing you best of luck",
                      textSize: 14,
                      isCenter: true,
                      fontWeight: FontWeight.w400,
                      textColor: Color(0xff5F6C6A),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    CustomText(
                      title:
                      "See more details of this My competition \nin the competition page",
                      textSize: 14,
                      isCenter: true,
                      fontWeight: FontWeight.w400,
                      textColor: Color(0xff5F6C6A),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                  ],
                ),
                Column(
                  children: [
                    AppPrimaryDarkButtonRound(
                        width: Get.width,
                        // isLoader: state is SignUpLoading ? true : false,
                        borderRadius: 10.0,
                        textTitle: "Go to my competition",
                        bgColor: appPrimaryColor,
                        textStyle: textStyleWhiteColorNormal,
                        functionToRun: () {
                          Get.to(() => MyCompetitionsScreen());
                        }),
                    SizedBox(
                      height: 10,
                    ),
                    AppPrimaryDarkButtonRound(
                        width: Get.width,
                        // isLoader: state is SignUpLoading ? true : false,
                        borderRadius: 10.0,
                        textTitle: "Go to home ",
                        bgColor: Color(0xff96E0D2),
                        textStyle: textStyleBlackColorNormal,
                        functionToRun: () {
                          Get.to(() => DashboardScreen());
                        }),
                  ],
                ),

                SizedBox(height: 2,),
              ],
            ).paddingSymmetric(vertical: 20),
          ),
        );
      },
    );
  }
}
