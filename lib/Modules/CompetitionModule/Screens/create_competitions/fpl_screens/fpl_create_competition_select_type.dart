import 'dart:developer';

import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/AppConstants/StringConstants/app_constants.dart';
import 'package:tuzz/Modules/CompetitionModule/Controllers/competition_controller.dart';
import 'package:tuzz/Modules/DashboardModule/Screens/dashboard_screen.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/bottom_sheet_widget.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/inputs.dart';
import 'package:tuzz/Widgets/text.dart';

class FPLCreateCompetitionSelectType extends StatefulWidget {
  const FPLCreateCompetitionSelectType({Key? key}) : super(key: key);

  @override
  _FPLCreateCompetitionSelectTypeState createState() =>
      _FPLCreateCompetitionSelectTypeState();
}

class _FPLCreateCompetitionSelectTypeState
    extends State<FPLCreateCompetitionSelectType> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompetitionController>(
      init: CompetitionController(),
      builder: (controller) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title: "Choose competition type",
              textColor: Color(0xff1B1C20),
              textSize: 20,
              fontWeight: FontWeight.w700,
            ),
            SizedBox(
              height: 30,
            ),
            _buildCompetitionSelectCard(
                "Fantasy Gameweek",
                "Create a tuzz for the coming gameweek, \nit ends when the game week ends",
                Color(0xffFFDDF7),
                () => modalBottomSheet(context, _gameWeekNameWidget(controller),
                    true, Get.height)),
            _buildCompetitionSelectCard(
                "PL Fantasy League",
                "Create a league that lasts throughout the \nseason, league ends at the end of the season",
                Color(0xffDFFFF4),
                () => modalBottomSheet(context, _gameWeekNameWidget(controller),
                    true, Get.height)),
          ],
        );
      },
    );
  }

  Widget _buildCompetitionSelectCard(
      String title, String subTitle, Color color, Function func) {
    return GestureDetector(
      onTap: () => func(),
      child: Container(
        width: Get.width,
        height: 150,
        child: Card(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          elevation: 0,
          color: color,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(
                title: title,
                textColor: Color(0xff1B1C20),
                textSize: 16,
                fontWeight: FontWeight.w600,
              ),
              SizedBox(
                height: 7,
              ),
              CustomText(
                title: subTitle,
                textColor: Color(0xff5F6C6A),
                textSize: 12,
                fontWeight: FontWeight.w500,
              ),
            ],
          ).paddingSymmetric(horizontal: 20, vertical: 20),
        ).marginSymmetric(vertical: 10),
      ),
    );
  }

  Widget _gameWeekNameWidget(CompetitionController controller) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () => Get.back(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Icon(Icons.keyboard_arrow_left),
                  SizedBox(
                    width: 10,
                  ),
                  CustomText(
                    title: "Competition",
                    textColor: Color(0xff1B1C20),
                    textSize: 14,
                    fontWeight: FontWeight.w600,
                  ).paddingAll(5),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            AppTextInputs(
              controller: controller.gameWeekTitleController,
              textInputType: TextInputType.text,
              onChange: (value) => value,
              validation: DIConstants.validators.validateString,
              textInputTitle: "Game week",
              color: Color(0xff1B1C20).withOpacity(.2),
            ),
            AppTextInputs(
              controller: controller.gameWeekDescriptionController,
              textInputType: TextInputType.text,
              onChange: (value) => value,
              validation: DIConstants.validators.validateString,
              maxLine: 5,
              textInputTitle: "Description",
              color: Color(0xff1B1C20).withOpacity(.2),
            ),
          ],
        ),
        AppPrimaryDarkButtonRound(
            width: Get.width,
            // isLoader: state is SignUpLoading ? true : false,
            borderRadius: 10.0,
            textTitle: "Continue",
            bgColor: appPrimaryColor,
            textStyle: textStyleWhiteColorNormal,
            functionToRun: () {
              modalBottomSheet(context, _gameWeekParticipantWidget(controller),
                  true, Get.height);
            }),
      ],
    ).marginSymmetric(vertical: 20);
  }

  Widget _gameWeekParticipantWidget(CompetitionController controller) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () => Get.back(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Icon(Icons.keyboard_arrow_left),
                  SizedBox(
                    width: 10,
                  ),
                  CustomText(
                    title: "Participant details",
                    textColor: Color(0xff1B1C20),
                    textSize: 14,
                    fontWeight: FontWeight.w600,
                  ).paddingAll(5),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            CustomText(
              title: "Set participant",
              textColor: Color(0xff1B1C20),
              textSize: 16,
              fontWeight: FontWeight.w600,
            ),
            SizedBox(
              height: 10,
            ),
            CustomText(
              title: "How many people can join this \ncompetition",
              textColor: Color(0xff5F6C6A),
              textSize: 12,
              fontWeight: FontWeight.w400,
            ),
            SizedBox(
              height: 30,
            ),
            AppTextInputs(
              controller: controller.gameWeekParticipantController,
              textInputType: TextInputType.number,
              onChange: (value) => value,
              validation: DIConstants.validators.validateNumber,
              textInputTitle: "No. of participant",
              color: Color(0xff1B1C20).withOpacity(.2),
            ),
            AppTextInputs(
              controller: controller.gameWeekAmountController,
              textInputType: TextInputType.number,
              onChange: (value) => value,
              formatter:
                  CurrencyTextInputFormatter(symbol: StringConstants.CURRENCY),
              validation: DIConstants.validators.validateString,
              textInputTitle: "Entry amount (₦)",
              color: Color(0xff1B1C20).withOpacity(.2),
            ),
          ],
        ),
        AppPrimaryDarkButtonRound(
            width: Get.width,
            // isLoader: state is SignUpLoading ? true : false,
            borderRadius: 10.0,
            textTitle: "Continue",
            bgColor: appPrimaryColor,
            textStyle: textStyleWhiteColorNormal,
            functionToRun: () {
              modalBottomSheet(context, _processingFeeWidget(controller), true,
                  Get.height / 1.5, Color(0xff7D2DFF), false);
            }),
      ],
    ).marginSymmetric(vertical: 20);
  }

  Widget _processingFeeWidget(CompetitionController controller) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          height: 250,
          width: Get.width,
          decoration: BoxDecoration(
              // color: Color(0xff7D2DFF)
              // image: DecorationImage(
              //   image: AssetImage("assets/images/pngs/processing_fee_img.png")
              // ),
              ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 50,
              ),
              ImageWidget.withSizeOnly(
                  "assets/images/pngs/img_bulb.png", 53, 53, true),
              Align(
                alignment: Alignment.centerRight,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    CustomText(
                      title: "10%",
                      textColor: Colors.white,
                      textSize: 36,
                      fontWeight: FontWeight.w700,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    CustomText(
                      title: "Processing fee",
                      textColor: Colors.white,
                      textSize: 36,
                      fontWeight: FontWeight.w700,
                    )
                  ],
                ),
              )
            ],
          ),
        ).paddingSymmetric(horizontal: 20),
        Container(
          color: Colors.white,
          height: 300,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CustomText(
                title:
                    "Tuzz processing fee is 10% of amount \naccumulated from entry fees",
                textColor: Color(0xff1B1C20),
                isCenter: true,
                textSize: 14,
                fontWeight: FontWeight.w400,
              ),
              AppPrimaryDarkButtonRound(
                  width: Get.width,
                  // isLoader: state is SignUpLoading ? true : false,
                  borderRadius: 10.0,
                  textTitle: "Alright!",
                  bgColor: appPrimaryColor,
                  textStyle: textStyleWhiteColorNormal,
                  functionToRun: () {
                    modalBottomSheet(
                        context,
                        _gameWeekPrizeDetailsWidget(controller),
                        true,
                        Get.height);
                  }),
            ],
          ).paddingSymmetric(horizontal: 15, vertical: 30),
        ),
      ],
    );
  }

  Widget _gameWeekPrizeDetailsWidget(CompetitionController controller) {
    int selectedRadioTile = 0;

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () => Get.back(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Icon(Icons.keyboard_arrow_left),
                  SizedBox(
                    width: 10,
                  ),
                  CustomText(
                    title: "Prize",
                    textColor: Color(0xff1B1C20),
                    textSize: 14,
                    fontWeight: FontWeight.w600,
                  ).paddingAll(5),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            IntrinsicHeight(
              child: Card(
                elevation: 1,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16)),
                color: Color(0xffF7F6FD),
                child: Stack(
                  children: [
                    Positioned(
                        top: 40,
                        right: Get.width / 2.50,
                        child: ImageWidget.withSizeOnly(
                            "assets/images/svgs/equality.svg", 32, 32)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomText(
                              title: "ENTRY FEE".toUpperCase(),
                              textSize: 12,
                              fontWeight: FontWeight.w500,
                              textColor: Color(0xff5F6C6A),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            CustomText(
                              title: DIConstants.moneyFormatter
                                  .returnFormattedMoney(5000, true),
                              textSize: 14,
                              fontWeight: FontWeight.w500,
                              textColor: Color(0xff1B1C20),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            CustomText(
                              title: "No of players".toUpperCase(),
                              textSize: 12,
                              fontWeight: FontWeight.w500,
                              textColor: Color(0xff5F6C6A),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            CustomText(
                              title: "305",
                              textSize: 14,
                              fontWeight: FontWeight.w500,
                              textColor: Color(0xff1B1C20),
                            ),
                          ],
                        ).paddingSymmetric(vertical: 15),
                        SizedBox(width: 2,),
                        VerticalDivider(
                          width: 1,
                          color: Color(0xff1B1C20).withOpacity(.2),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            CustomText(
                              title: "PRIZE ESTIMATION".toUpperCase(),
                              textSize: 12,
                              fontWeight: FontWeight.w500,
                              textColor: Color(0xff5F6C6A),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            CustomText(
                              title: DIConstants.moneyFormatter
                                  .returnFormattedMoney(1456555, true),
                              textSize: 18,
                              fontWeight: FontWeight.w700,
                              textColor: Color(0xff5F6C6A),
                            ),
                          ],
                        ).paddingSymmetric(vertical: 15),
                      ],
                    ).paddingSymmetric(
                      horizontal: 17,
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            CustomText(
              title: "How do you want the prize shared?",
              textSize: 16,
              fontWeight: FontWeight.w600,
              textColor: Color(0xff1B1C20),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                border: Border.all(color: Color(0xff1B1C20).withOpacity(.1)),
              ),
              child: RadioListTile(
                value: 1,
                groupValue: selectedRadioTile,
                title: CustomText(
                  title: "Winner Takes all",
                  textSize: 16,
                  fontWeight: FontWeight.w600,
                  textColor: Color(0xff1B1C20),
                ),
                subtitle: CustomText(
                  title: "One winner takes all the prize",
                  textSize: 12,
                  fontWeight: FontWeight.w400,
                  textColor: Color(0xff5F6C6A),
                ),
                onChanged: (int? val) {
                  print("Radio Tile pressed $val");
                  setState(() {
                    selectedRadioTile = val!;
                  });
                },
                selected: true,
              ),
            ).paddingSymmetric(vertical: 5),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                border: Border.all(color: Color(0xff1B1C20).withOpacity(.1)),
              ),
              child: RadioListTile(
                value: 2,
                groupValue: selectedRadioTile,
                title: CustomText(
                  title: "Top Players",
                  textSize: 16,
                  fontWeight: FontWeight.w600,
                  textColor: Color(0xff1B1C20),
                ),
                subtitle: CustomText(
                  title: "Prize shared among top players",
                  textSize: 12,
                  fontWeight: FontWeight.w400,
                  textColor: Color(0xff5F6C6A),
                ),
                onChanged: (int? val) {
                  print("Radio Tile pressed $val");
                  setState(() {
                    selectedRadioTile = val!;
                  });
                },
                selected: false,
              ),
            ).paddingSymmetric(vertical: 5),
            SizedBox(
              height: 40,
            ),
            Text.rich(
                TextSpan(text: 'For professionals, ', children: <InlineSpan>[
              TextSpan(
                text: 'See advanced settings',
                style: textStylePrimaryColorNormal,
              )
            ]))
          ],
        ),
        AppPrimaryDarkButtonRound(
            width: Get.width,
            // isLoader: state is SignUpLoading ? true : false,
            borderRadius: 10.0,
            textTitle: "Continue",
            bgColor: appPrimaryColor,
            textStyle: textStyleWhiteColorNormal,
            functionToRun: () {
              modalBottomSheet(context, _createCompetitionWidget(controller),
                  true, Get.height);
            }),
      ],
    ).marginSymmetric(vertical: 20);
  }

  Widget _createCompetitionWidget(CompetitionController controller) {
    int selectedRadioTile = 0;

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () => Get.back(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Icon(Icons.keyboard_arrow_left),
                  SizedBox(
                    width: 10,
                  ),
                  CustomText(
                    title: "Create Competition",
                    textColor: Color(0xff1B1C20),
                    textSize: 14,
                    fontWeight: FontWeight.w600,
                  ).paddingAll(5),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            CustomText(
              title: "Competition privacy",
              textSize: 16,
              fontWeight: FontWeight.w600,
              textColor: Color(0xff1B1C20),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Color(0xff1B1C20).withOpacity(.1)),
              ),
              child: RadioListTile(
                value: 1,
                groupValue: selectedRadioTile,
                title: CustomText(
                  title: "Public",
                  textSize: 16,
                  fontWeight: FontWeight.w600,
                  textColor: Color(0xff1B1C20),
                ),
                subtitle: CustomText(
                  title: "Anybody can join",
                  textSize: 12,
                  fontWeight: FontWeight.w400,
                  textColor: Color(0xff5F6C6A),
                ),
                onChanged: (int? val) {
                  print("Radio Tile pressed $val");
                  setState(() {
                    selectedRadioTile = val!;
                  });
                },
                selected: true,
              ),
            ).paddingSymmetric(vertical: 5),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Color(0xff1B1C20).withOpacity(.1)),
              ),
              child: RadioListTile(
                value: 2,
                groupValue: selectedRadioTile,
                title: CustomText(
                  title: "Private",
                  textSize: 16,
                  fontWeight: FontWeight.w600,
                  textColor: Color(0xff1B1C20),
                ),
                subtitle: CustomText(
                  title: "You would approve people to join",
                  textSize: 12,
                  fontWeight: FontWeight.w400,
                  textColor: Color(0xff5F6C6A),
                ),
                onChanged: (int? val) {
                  print("Radio Tile pressed $val");
                  setState(() {
                    selectedRadioTile = val!;
                  });
                },
                selected: false,
              ),
            ).paddingSymmetric(vertical: 5),
          ],
        ),
        Column(
          children: [
            Card(
              color: Color(0xff7D2DFF),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    title: "Pay",
                    textSize: 12,
                    fontWeight: FontWeight.w500,
                    textColor: Colors.white,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CustomText(
                    title: DIConstants.moneyFormatter
                        .returnFormattedMoney(5000, true),
                    textSize: 36,
                    fontWeight: FontWeight.w700,
                    textColor: Colors.white,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CustomText(
                    title:
                        "You are to pay a default prize fee to create this competition",
                    textSize: 14,
                    fontWeight: FontWeight.w400,
                    textColor: Colors.white,
                  ),
                ],
              ).paddingSymmetric(horizontal: 17, vertical: 17),
            ),
            SizedBox(
              height: 40,
            ),
            AppPrimaryDarkButtonRound(
                width: Get.width,
                // isLoader: state is SignUpLoading ? true : false,
                borderRadius: 10.0,
                textTitle: "Pay to create competition",
                bgColor: appPrimaryColor,
                textStyle: textStyleWhiteColorNormal,
                functionToRun: () {
                  modalBottomSheet(context, _transactionPinWidget(controller),
                      true, Get.height / 2, Color(0xff7D2DFF));
                }),
          ],
        ),
      ],
    ).marginSymmetric(vertical: 20);
  }

  Widget _transactionPinWidget(CompetitionController controller) {
    TextEditingController transactionPinController =
        new TextEditingController();

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          height: 20,
        ),
        CustomText(
          title: "Input transaction pin",
          textSize: 20,
          fontWeight: FontWeight.w700,
          textColor: Colors.white,
        ),
        SizedBox(
          height: 50,
        ),
        Container(
          width: 300,
          child: PinTextInput(
              controller: transactionPinController,
              validation: DIConstants.validators.validateString,
              fieldCount: 4,
              isTransactionPin: true,
              onChange: (value) {

                if (value.length == 4) {
                  modalBottomSheet(
                      context,
                      _successScreenForGameWeekCreationWidget(controller),
                      true,
                      Get.height);
                }
              }),
        )
      ],
    );
  }

  Widget _successScreenForGameWeekCreationWidget(
      CompetitionController controller) {
    return Container(
      height: Get.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Lottie.asset("assets/images/lottie_images/success.json", width: 140),
          SizedBox(
            height: 30,
          ),
          CustomText(
            title: "Success!",
            textSize: 24,
            isCenter: true,
            fontWeight: FontWeight.w700,
            textColor: Color(0xff1B1C20),
          ),
          SizedBox(
            height: 10,
          ),
          CustomText(
            title:
                "You have successfully created a Game Week \ncompetition with ₦10,000, \nWishing you best of luck",
            textSize: 14,
            isCenter: true,
            fontWeight: FontWeight.w400,
            textColor: Color(0xff5F6C6A),
          ),
          SizedBox(
            height: 15,
          ),
          CustomText(
            title:
                "See more details of this My competition \nin the competition page",
            textSize: 14,
            isCenter: true,
            fontWeight: FontWeight.w400,
            textColor: Color(0xff5F6C6A),
          ),
          SizedBox(
            height: 50,
          ),
          SizedBox(
            height: 50,
          ),
          AppPrimaryDarkButtonRound(
              width: Get.width,
              // isLoader: state is SignUpLoading ? true : false,
              borderRadius: 10.0,
              textTitle: "Go to Dashboard",
              bgColor: appPrimaryColor,
              textStyle: textStyleWhiteColorNormal,
              functionToRun: () {
                Get.to(() => DashboardScreen());
              })
        ],
      ),
    );
  }
}
