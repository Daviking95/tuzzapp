import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Helpers/Enums/categories_enum.dart';
import 'package:tuzz/Modules/CompetitionModule/Controllers/competition_controller.dart';
import 'package:tuzz/Modules/DashboardModule/Screens/dashboard_screen.dart';
import 'package:tuzz/Widgets/starter_widget.dart';
import 'package:tuzz/Widgets/text.dart';

import 'fpl_screens/fpl_create_competition_select_type.dart';

class CreateCompetitionScreen extends StatefulWidget {
  final CategoriesEnum categoriesEnum;

  const CreateCompetitionScreen(this.categoriesEnum);

  @override
  _CreateCompetitionScreenState createState() => _CreateCompetitionScreenState();
}

class _CreateCompetitionScreenState extends State<CreateCompetitionScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompetitionController>(
      init: CompetitionController(),
      builder: (controller) {
        return StarterWidget(
            hasTopWidget: false,
            hasPagePadding: true,
            isExitApp: false,
            isBgAllowed: false,
            isBackWidget: true,
            backUrlWidgetScreen: DashboardScreen(),
            hasRefresh: true,
            bottomPageContent: _bottomPageContent(controller));
      },
    );
  }

  _bottomPageContent(CompetitionController controller) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () => Get.back(),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(Icons.keyboard_arrow_left),
              SizedBox(width: 10,),
              CustomText(
                title: "Create Competition",
                textColor: Color(0xff1B1C20),
                textSize: 14,
                fontWeight: FontWeight.w600,
              ).paddingAll(5),
            ],
          ),
        ),
        SizedBox(
          height: 30,
        ),
        _getWidgetToShow(widget.categoriesEnum)
      ],
    ).paddingSymmetric(vertical: 15);
  }

  _getWidgetToShow(CategoriesEnum categoriesEnum) {
    switch(categoriesEnum){

      case CategoriesEnum.FPL:
        // TODO: Handle this case.
      return FPLCreateCompetitionSelectType();
        break;
      case CategoriesEnum.XBOX:
        // TODO: Handle this case.
      return Container();
        break;
      case CategoriesEnum.MOBILE_GAMES:
        // TODO: Handle this case.
        return Container();

        break;
      case CategoriesEnum.BOARD_GAMES:
        // TODO: Handle this case.
        return Container();

        break;
      case CategoriesEnum.SPORTS:
        // TODO: Handle this case.
        return Container();

        break;
      case CategoriesEnum.OTHERS:
        // TODO: Handle this case.
        return Container();

        break;
    }
  }
}
