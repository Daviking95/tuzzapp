import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/Modules/CompetitionModule/Controllers/competition_controller.dart';
import 'package:tuzz/Widgets/bottom_sheet_widget.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/inputs.dart';
import 'package:tuzz/Widgets/text.dart';

class FPLParticipantsScreen extends StatefulWidget {
  const FPLParticipantsScreen({Key? key}) : super(key: key);

  @override
  _FPLParticipantsScreenState createState() => _FPLParticipantsScreenState();
}

class _FPLParticipantsScreenState extends State<FPLParticipantsScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompetitionController>(
      builder: (controller) {
        return SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                      onTap: () => Get.back(),
                      child: Icon(Icons.arrow_back_ios, size: 15,).paddingAll(10)),
                  SizedBox(
                    width: 15,
                  ),
                  CustomText(
                    title: "Participants",
                    textSize: 14,
                    fontWeight: FontWeight.w500,
                    textColor: Color(0xff1B1C20),
                  ),
                ],
              ),
              SizedBox(
                height: 40,
              ),
              CustomText(
                title: "7 of 8 participants",
                textSize: 20,
                fontWeight: FontWeight.w700,
                textColor: Color(0xff1B1C20),
              ),
              SizedBox(
                height: 20,
              ),
              AppTextInputs(
                controller: controller.noOfPlayersController,
                textInputType: TextInputType.text,
                onChange: (value) => value,
                validation: DIConstants.validators.validateString,
                textInputTitle: "Search",
                color: Theme
                    .of(context)
                    .textTheme
                    .bodyText1!
                    .color,
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: Get.height,
                child: ListView.builder(
                  itemCount: 10,
                    itemBuilder: (context, index) => _returnPlayersDetailsItem(index)),
              )
            ],
          ),
        );
      },
    );
  }

  _returnPlayersDetailsItem(int index) {
    return Column(
      children: [
        GestureDetector(
          onTap: () => _showBottomSheetForSinglePlayerDetails(),
          child: ListTile(
            leading: CustomText(
              title: index.toString(),
              textSize: 16,
              fontWeight: FontWeight.w600,
              textColor: Color(0xff5F6C6A),
            ),
            trailing: index == 0 ? CustomText(
              title: "HOST",
              textSize: 12,
              fontWeight: FontWeight.w400,
              textColor: Color(0xff7D2DFF),
            ) : SizedBox(),
            title: Row(
              children: [
                ImageWidget.withSizeOnly("assets/images/pngs/partis.png", 48, 48, true),
                SizedBox(width: 16,),
                CustomText(
                  title: "Joseph Hungbo",
                  textSize: 14,
                  fontWeight: FontWeight.w400,
                  textColor: Color(0xff51B1C20),
                ),
              ],
            ),
          ),
        ),
        Divider(height: 1, color: Color(0xff1B1C20).withOpacity(.2),).paddingSymmetric(vertical: 8)
      ],
    );
  }

  _showBottomSheetForSinglePlayerDetails() {
    return modalBottomSheet(
        context,
        SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomText(
                    title: "Details",
                    textSize: 20,
                    fontWeight: FontWeight.w700,
                    textColor: Color(0xff1B1C20),
                  ),
                  GestureDetector(
                      onTap: () => Get.back(),
                      child: Icon(Icons.close))
                ],
              ),
              SizedBox(height: 40,),
              Row(
                children: [
                  ImageWidget.withSizeOnly("assets/images/pngs/partis.png", 48, 48, true),
                  SizedBox(width: 16,),
                  CustomText(
                    title: "Joseph Hungbo",
                    textSize: 14,
                    fontWeight: FontWeight.w400,
                    textColor: Color(0xff51B1C20),
                  ),
                ],
              ),
              SizedBox(height: 40,),
              CustomText(
                title: "Contact details",
                textSize: 16,
                fontWeight: FontWeight.w600,
                textColor: Color(0xff51B1C20),
              ).paddingSymmetric(horizontal: 15),
              SizedBox(height: 20,),
              _contactItem("WHATSAPP", "0901 782 1834", "assets/images/svgs/whatsapp_icon_2.svg"),
              SizedBox(height: 24,),
              _contactItem("PHONE NUMBER", "0901 782 1834", "assets/images/svgs/phone_icon_2.svg"),
              SizedBox(height: 24,),

            ],
          ).paddingAll(15),
        ),
        true,
        Get.height / 1.5);
  }

  ListTile _contactItem(String title, String value, String icon) {
    return ListTile(
              leading:
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    title: title,
                    textSize: 12,
                    fontWeight: FontWeight.w500,
                    textColor: Color(0xff5F6C6A),
                  ),
                  SizedBox(height: 10,),
                  CustomText(
                    title: value,
                    textSize: 14,
                    fontWeight: FontWeight.w400,
                    textColor: Color(0xff1B1C20),
                  ),
                ],
              ),
              trailing: ImageWidget.withSizeOnly(icon, 40, 40).paddingAll(5),
            );
  }
}
