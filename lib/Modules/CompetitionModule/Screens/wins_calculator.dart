import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/Modules/CompetitionModule/Controllers/competition_controller.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/inputs.dart';
import 'package:tuzz/Widgets/text.dart';

class WinsCalculator extends StatefulWidget {
  const WinsCalculator({Key? key}) : super(key: key);

  @override
  _WinsCalculatorState createState() => _WinsCalculatorState();
}

class _WinsCalculatorState extends State<WinsCalculator> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompetitionController>(
      builder: (controller) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CustomText(
                  title: "Prize info",
                  textSize: 20,
                  fontWeight: FontWeight.w700,
                  textColor: Color(0xff1B1C20),
                ),
                GestureDetector(
                    onTap: () => Get.back(), child: Icon(Icons.close))
              ],
            ),
            SizedBox(
              height: 16,
            ),
            CustomText(
              title:
                  "The prize is percentage based and its determined by the number of people in this competition",
              textSize: 14,
              fontWeight: FontWeight.w400,
              textColor: Color(0xff1B1C20),
            ).paddingOnly(right: 80),
            SizedBox(
              height: 16,
            ),
            CustomText(
              title:
                  "Invite more people to play to increase your win prize. Calculate your expected wins below",
              textSize: 14,
              fontWeight: FontWeight.w400,
              textColor: Color(0xff1B1C20),
            ).paddingOnly(right: 50),
            SizedBox(
              height: 40,
            ),
            CustomText(
              title: "Calculate expected wins",
              textSize: 16,
              fontWeight: FontWeight.w600,
              textColor: Color(0xff1B1C20),
            ),
            SizedBox(
              height: 8,
            ),
            CustomText(
              title: "The host set maximum players to 305",
              textSize: 12,
              fontWeight: FontWeight.w400,
              textColor: Color(0xff5F6C6A),
            ),
            SizedBox(
              height: 16,
            ),
            Divider(
              height: 1,
              color: Colors.black.withOpacity(.4),
            ),
            IntrinsicHeight(
              child: Stack(
                children: [
                  Positioned(
                      top: 13,
                      right: Get.width / 2.55,
                      child: ImageWidget.withSizeOnly(
                          "assets/images/svgs/equality.svg", 32, 32)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText(
                            title: "No of players".toUpperCase(),
                            textSize: 12,
                            fontWeight: FontWeight.w500,
                            textColor: Color(0xff5F6C6A),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          // AppTextInputs(
                          //   controller: controller.noOfPlayersController,
                          //   textInputType: TextInputType.number,
                          //   onChange: (value) => value,
                          //   validation: DIConstants.validators.validateString,
                          //   textInputTitle: "Type here",
                          //   color: Theme.of(context).textTheme.bodyText1!.color,
                          // ),
                        ],
                      ).paddingSymmetric(vertical: 9),
                      VerticalDivider(
                        width: 1,
                        color: Color(0xff1B1C20).withOpacity(.2),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText(
                            title: "WINNER TAKES".toUpperCase(),
                            textSize: 12,
                            fontWeight: FontWeight.w500,
                            textColor: Color(0xff5F6C6A),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          CustomText(
                            title: DIConstants.moneyFormatter
                                .returnFormattedMoney(1456555),
                            textSize: 18,
                            fontWeight: FontWeight.w700,
                            textColor: Color(0xff5F6C6A),
                          ),
                        ],
                      ).paddingSymmetric(vertical: 9),
                    ],
                  ),
                ],
              ),
            ),
            Divider(
              height: 1,
              color: Colors.black.withOpacity(.4),
            ),
            SizedBox(
              height: 25,
            ),
            AppPrimaryDarkButtonRound(
                width: Get.width,
                borderRadius: 10.0,
                textTitle: "Invite friends to join",
                bgColor: appPrimaryColor,
                textStyle: textStyleWhiteColorNormal,
                functionToRun: () {}),
          ],
        ).paddingAll(10);
      },
    );
  }
}
