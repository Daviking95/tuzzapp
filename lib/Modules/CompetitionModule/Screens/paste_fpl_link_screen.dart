
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/Modules/CompetitionModule/Controllers/competition_controller.dart';
import 'package:tuzz/Modules/CompetitionModule/Screens/show_fpl_link_result_screen.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/inputs.dart';
import 'package:tuzz/Widgets/starter_widget.dart';
import 'package:tuzz/Widgets/text.dart';

class PasteFPLLinkScreen extends StatefulWidget {
  const PasteFPLLinkScreen({Key? key}) : super(key: key);

  @override
  _PasteFPLLinkScreenState createState() => _PasteFPLLinkScreenState();
}

class _PasteFPLLinkScreenState extends State<PasteFPLLinkScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompetitionController>(
      init: CompetitionController(),
      builder: (controller) {
        return StarterWidget(
            hasTopWidget: false,
            hasPagePadding: true,
            isExitApp: false,
            isBgAllowed: false,
            hasBottomNav: false,
            hasRefresh: false,
            // startLoader: controller.startLoader,
            bottomPageContent: _bottomPageContent());
      },
    );
  }

  _bottomPageContent() {
    return GetBuilder<CompetitionController>(
      init: CompetitionController(),
      builder: (controller) {
        return SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                      onTap: () => controller.counter == 1
                          ? Get.back()
                          : controller.counter--,
                      child: Icon(Icons.arrow_back_ios, size: 15).paddingAll(10)),
                  SizedBox(
                    width: 15,
                  ),
                  CustomText(
                    title: "Paste Link",
                    textSize: 15,
                    fontWeight: FontWeight.w400,
                    textColor: Color(0xff1B1C20),
                  ),
                ],
              ),
              SizedBox(
                height: 40,
              ),
              CustomText(
                title: "Paste your FPL profile link",
                textColor: Color(0xff1B1C20),
                textSize: 20,
                fontWeight: FontWeight.w700,
              ),
              SizedBox(
                height: 15,
              ),
              CustomText(
                title:
                "We need your FPL League link to validate the \nleague you are creating a competetion for",
                textSize: 12,
                fontWeight: FontWeight.w400,
              ),
              SizedBox(
                height: 40,
              ),
              // CustomText(
              //   title: "Paste your FPL link",
              //   textColor: Color(0xff1B1C20),
              //   textSize: 14,
              //   fontWeight: FontWeight.w500,
              // ),
              AppTextInputs(
                controller: controller.fplLinkController,
                textInputType: TextInputType.text,
                onChange: (value) => value,
                validation: DIConstants.validators.validateString,
                textInputTitle: "Paste your FPL link",
                color: Color(0xff1B1C20).withOpacity(.2),
              ),
              SizedBox(
                height: 30,
              ),
              CustomText(
                title: "See how to get your league \nlink in 3 easy steps",
                textSize: 20,
                fontWeight: FontWeight.w700,
              ),
              SizedBox(
                height: 15,
              ),
              GestureDetector(
                onTap: () => controller.isSeeInstructionsTrue = !controller.isSeeInstructionsTrue,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CustomText(
                      title: "Show instructions",
                      textSize: 14,
                      fontWeight: FontWeight.w600,
                      textColor: appPrimaryColor,
                    ),
                    Icon(controller.isSeeInstructionsTrue ? Icons.keyboard_arrow_down : Icons.keyboard_arrow_right, color: appPrimaryColor, size: 20,)
                  ],
                ).paddingOnly(right: 10),
              ),
              SizedBox(
                height: 25,
              ),
              controller.isSeeInstructionsTrue ? _showSteps() : Container(),
              SizedBox(
                height: 60,
              ),
              AppPrimaryDarkButtonRound(
                  width: Get.width,
                  // isLoader: state is SignUpLoading ? true : false,
                  borderRadius: 10.0,
                  textTitle: "Continue",
                  bgColor: appPrimaryColor,
                  textStyle: textStyleWhiteColorNormal,
                  functionToRun: () {
                      Get.to(() => ShowFPLLinkResultScreen());

                  }),
            ],
          ).paddingSymmetric(vertical: 20),
        );
      },
    );
  }

  _showSteps() {
    return Container(
      width: Get.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CustomText(
            title: "STEP 1",
            textSize: 12,
            isCenter: true,
            fontWeight: FontWeight.w500,
          ),
          SizedBox(height: 20,),
          Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16)),
            elevation: 0,
            color: Color(0xffFFDDF7),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                CustomText(
                  title:
                  "Go to the link below and sign into on your browser and click on points",
                  textSize: 14,
                  fontWeight: FontWeight.w400,
                ),
                SizedBox(
                  height: 16,
                ),
                CustomText(
                  title: "Fantasy.premierleague.com",
                  textSize: 14,
                  textDecoration: TextDecoration.underline,
                  fontWeight: FontWeight.w400,
                ),
                SizedBox(
                  height: 16,
                ),
                ImageWidget.withSizeOnly(
                    "assets/images/pngs/link_fpl_1.png", 145, 270, true)
              ],
            ).paddingAll(15),
          ),
          SizedBox(height: 30,),
          CustomText(
            title: "STEP 2",
            textSize: 12,
            isCenter: true,
            fontWeight: FontWeight.w500,
          ),
          SizedBox(height: 20,),
          Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16)),
            elevation: 0,
            color: Color(0xffFFDDF7),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                CustomText(
                  title: "Scroll down and Click on view game week history",
                  textSize: 14,
                  fontWeight: FontWeight.w400,
                ),
                SizedBox(
                  height: 16,
                ),
                ImageWidget.withSizeOnly(
                    "assets/images/pngs/link_fpl_2.png", 280, 270, true)
              ],
            ).paddingAll(15),
          ),
          SizedBox(height: 30,),
          CustomText(
            title: "STEP 3",
            textSize: 12,
            isCenter: true,
            fontWeight: FontWeight.w500,
          ),
          SizedBox(height: 20,),
          Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16)),
            elevation: 0,
            color: Color(0xffFFDDF7),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                CustomText(
                  title: "Copy the link at the top of the page",
                  textSize: 14,
                  fontWeight: FontWeight.w400,
                ),
                SizedBox(
                  height: 30,
                ),
                ImageWidget.withSizeOnly(
                    "assets/images/pngs/link_fpl_3.png", 320, 270, true)
              ],
            ).paddingAll(15),
          )
        ],
      ),
    );
  }
}
