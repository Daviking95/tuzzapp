
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Helpers/Enums/categories_enum.dart';
import 'package:tuzz/Modules/DashboardModule/Screens/no_data_widget.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/text.dart';

import 'link_fpl_competition.dart';

class LinkFPLDescScreen extends StatefulWidget {
  const LinkFPLDescScreen({Key? key}) : super(key: key);

  @override
  _LinkFPLDescScreenState createState() => _LinkFPLDescScreenState();
}

class _LinkFPLDescScreenState extends State<LinkFPLDescScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: Get.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
                onTap: () => Get.back(),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Icon(Icons.arrow_back_ios, size: 15)
                        .paddingSymmetric(horizontal: 10, vertical: 15))),
            Column(
              children: [
                NoDataWidget(
                  iconString: "assets/images/pngs/tc_fpl.png",
                  iconColor: Color(0xff38003D),
                  title: "Link your fantasy premier \nleague account to continue",
                  subTitle:
                  "We need to access your score and league \ndetails on fantasy premier league to be able \nto award your prizes",
                  btnText: "Link your FPL account",
                  routeToGo: LinkFPLCompetitionScreen(CategoriesEnum.FPL),
                )
              ],
            ),
            Center(
              child: Text.rich(
                  TextSpan(
                      text: 'If you don’t have a fantasy premier league account, ',
                      children: <InlineSpan>[
                        TextSpan(
                          text: 'Click here to register',
                          style: textStylePrimaryColorNormal,
                        ),
                      ]
                  ),
                textAlign: TextAlign.center,
                style: textStyleBlackColorNormal.copyWith(fontWeight: FontWeight.w400, fontSize: 15, height: 1.5),
              ).paddingSymmetric(horizontal: 40),
            ),
            SizedBox(height: 10,)
          ],
        ),
      ),
    );
  }
}
