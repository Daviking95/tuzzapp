import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/Modules/CompetitionModule/Controllers/competition_controller.dart';
import 'package:tuzz/Modules/CompetitionModule/Screens/show_fpl_link_result_screen.dart';
import 'package:tuzz/Modules/DashboardModule/Screens/no_data_widget.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/inputs.dart';
import 'package:tuzz/Widgets/starter_widget.dart';
import 'package:tuzz/Widgets/text.dart';

import 'fpl_payment_confirmation_screen.dart';
import 'join_fpl_success_screen.dart';
import 'paste_fpl_link_screen.dart';

class FPLPaymentConfirmationScreen extends StatefulWidget {
  const FPLPaymentConfirmationScreen({Key? key}) : super(key: key);

  @override
  _FPLPaymentConfirmationScreenState createState() =>
      _FPLPaymentConfirmationScreenState();
}

class _FPLPaymentConfirmationScreenState
    extends State<FPLPaymentConfirmationScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompetitionController>(
      init: CompetitionController(),
      builder: (controller) {
        return StarterWidget(
            hasTopWidget: false,
            hasPagePadding: true,
            isExitApp: false,
            isBgAllowed: false,
            hasBottomNav: false,
            hasRefresh: false,
            // startLoader: controller.startLoader,
            bottomPageContent: _bottomPageContent());
      },
    );
  }

  _bottomPageContent() {
    return GetBuilder<CompetitionController>(
      init: CompetitionController(),
      builder: (controller) {
        return SingleChildScrollView(
          child: Container(
            height: Get.height,
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            GestureDetector(
                                onTap: () => Get.back(),
                                child:
                                    Icon(Icons.arrow_back_ios, size: 15).paddingAll(10)),
                            SizedBox(
                              width: 15,
                            ),
                            CustomText(
                              title: "Details",
                              textSize: 14,
                              fontWeight: FontWeight.w500,
                              textColor: Color(0xff1B1C20),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            CustomText(
                              title: "WALLET",
                              textSize: 12,
                              fontWeight: FontWeight.w500,
                              textColor: Color(0xff1B1C20).withOpacity(.5),
                            ),
                            SizedBox(
                              width: 16,
                            ),
                            CustomText(
                              title: DIConstants.moneyFormatter
                                  .returnFormattedMoney(0.0),
                              textSize: 16,
                              fontWeight: FontWeight.w700,
                            ).paddingAll(5),
                          ],
                        )
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        CustomText(
                          title: DIConstants.moneyFormatter
                              .returnFormattedMoney(10000, true),
                          isCenter: true,
                          textSize: 50,
                          fontWeight: FontWeight.w700,
                          textColor: Color(0xff1B1C20),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        CustomText(
                          title:
                              "You are to pay an entry fee of ₦10,000 \nto join this competition",
                          textSize: 14,
                          isCenter: true,
                          fontWeight: FontWeight.w400,
                          textColor: Color(0xff5F6C6A),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        Card(
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(16)),
                          color: Color(0xffDFFFF4),
                          child: CustomText(
                            title:
                                "Endeavour to communicate with the host on competition rules and terms before joining. ",
                            textSize: 14,
                            isCenter: true,
                            fontWeight: FontWeight.w500,
                            textColor: Color(0xff1B1C20),
                          ).paddingSymmetric(vertical: 30, horizontal: 15),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        AppPrimaryDarkButtonRound(
                            width: Get.width,
                            // isLoader: state is SignUpLoading ? true : false,
                            borderRadius: 10.0,
                            textTitle: "Pay Now To Join",
                            bgColor: appPrimaryColor,
                            textStyle: textStyleWhiteColorNormal,
                            functionToRun: () {
                              Get.to(() => FPLJoinSuccessScreen());
                            }),
                      ],
                    ),

                    // SizedBox(height: 2,),
                  ],
                ).paddingOnly(bottom: 50),
                Positioned(
                  bottom: 210,
                  right: Get.width / 2.5,
                  child: Card(
                      elevation: 1,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100)),
                      color: Color(0xffFFF7C0),
                      child: ImageWidget.withSizeOnly(
                              "assets/images/svgs/bulb_light.svg", 30, 30)
                          .paddingAll(10)),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
