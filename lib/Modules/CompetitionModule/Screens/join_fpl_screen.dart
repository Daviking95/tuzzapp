import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/Helpers/Enums/categories_enum.dart';
import 'package:tuzz/Modules/CompetitionModule/Controllers/competition_controller.dart';
import 'package:tuzz/Modules/CompetitionModule/Screens/wins_calculator.dart';
import 'package:tuzz/Modules/DashboardModule/Screens/no_data_widget.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Snippets/more_info_snippets.dart';
import 'package:tuzz/Snippets/participants_snippet.dart';
import 'package:tuzz/Widgets/bottom_sheet_widget.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/inputs.dart';
import 'package:tuzz/Widgets/starter_widget.dart';
import 'package:tuzz/Widgets/text.dart';

import 'fpl_participants_screen.dart';
import 'link_fpl_competition.dart';
import 'link_fpl_desc_screen.dart';

class JoinFPLDetailsScreen extends StatefulWidget {
  const JoinFPLDetailsScreen({Key? key}) : super(key: key);

  @override
  _JoinFPLDetailsScreenState createState() => _JoinFPLDetailsScreenState();
}

class _JoinFPLDetailsScreenState extends State<JoinFPLDetailsScreen> {
  bool isWinnerAll = false;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompetitionController>(
      init: CompetitionController(),
      builder: (controller) {
        return StarterWidget(
            hasTopWidget: false,
            hasPagePadding: true,
            isExitApp: false,
            isBgAllowed: false,
            hasBottomNav: false,
            hasRefresh: false,
            // startLoader: controller.startLoader,
            bottomPageContent: _bottomPageContent(controller));
      },
    );
  }

  _bottomPageContent(controller) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                      onTap: () => Get.back(),
                      child:
                          Icon(Icons.arrow_back_ios, size: 15).paddingAll(10)),
                  SizedBox(
                    width: 15,
                  ),
                  CustomText(
                    title: "Details",
                    textSize: 14,
                    fontWeight: FontWeight.w500,
                    textColor: Color(0xff1B1C20),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  CustomText(
                    title: "WALLET",
                    textSize: 12,
                    fontWeight: FontWeight.w500,
                    textColor: Color(0xff5F6C6A),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  CustomText(
                    title:
                        DIConstants.moneyFormatter.returnFormattedMoney(45000),
                    textSize: 16,
                    fontWeight: FontWeight.w700,
                    textColor: Color(0xff1B1C20),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: 50,
          ),
          Container(
            height: 100,
            child: ListView(
                scrollDirection: Axis.horizontal,
                children: isWinnerAll
                    ? [
                        _trophyCard("assets/images/svgs/first_place.svg",
                            "WINNER TAKES ALL", 45000),
                      ]
                    : [
                        _trophyCard("assets/images/svgs/first_place.svg",
                            "1st TAKES", 45000),
                        _trophyCard("assets/images/svgs/second_place.svg",
                            "2nd TAKES", 30000),
                        _trophyCard("assets/images/svgs/first_place.svg",
                            "3rd TAKES", 20000),
                        GestureDetector(
                          onTap: () => _showBottomSheetForOtherPrizes(),
                          child: Container(
                            height: 20,
                            child: Card(
                              color: Color(0xffDFFFF4),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              child: CustomText(
                                title: "4 more prizes, View all",
                                textSize: 12,
                                fontWeight: FontWeight.w400,
                                textColor: appPrimaryColor,
                              ).paddingSymmetric(vertical: 15, horizontal: 10),
                            ),
                          ),
                        ),
                      ]),
          ),
          SizedBox(
            height: 30,
          ),
          GestureDetector(
            onTap: () => _showBottomSheetForMoreDetails(),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: CustomText(
                    title: "This prize will increase when more players join. ",
                    textSize: 12,
                    fontWeight: FontWeight.w500,
                    textColor: Color(0xff1B1C20),
                  ),
                ),
                CustomText(
                  title: "See details",
                  textSize: 12,
                  fontWeight: FontWeight.w500,
                  textColor: appPrimaryColor,
                ),
              ],
            ).paddingSymmetric(vertical: 5),
          ),
          Divider(
            height: 1,
            color: Colors.black.withOpacity(.4),
          ).paddingSymmetric(vertical: 20),
          CustomText(
            title: "Do am if e easy",
            textSize: 16,
            fontWeight: FontWeight.w600,
            textColor: Color(0xff1B1C20),
          ),
          SizedBox(
            height: 10,
          ),
          CustomText(
            title:
                "Leagues of the best!!! if you think you have what it takes come on board",
            textSize: 12,
            fontWeight: FontWeight.w400,
            textColor: Color(0xff5F6C6A),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                flex: 3,
                child: Row(
                  children: [
                    ImageWidget.withSizeOnly(
                        "assets/images/svgs/competition_clock.svg", 24, 24),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: CustomText(
                        title: "Entry closes in 32 days, OCT 3",
                        textSize: 12,
                        fontWeight: FontWeight.w400,
                        textColor: Color(0xff5F6C6A),
                      ),
                    )
                  ],
                ),
              ),
              Spacer(),
              Row(
                children: [
                  ImageWidget.withSizeOnly(
                      "assets/images/svgs/competition_users.svg", 24, 24),
                  SizedBox(
                    width: 10,
                  ),
                  GestureDetector(
                    onTap: () => _showBottomSheetForPaticipants(),
                    child: CustomText(
                      title: "300/302",
                      textSize: 12,
                      fontWeight: FontWeight.w400,
                      textDecoration: TextDecoration.underline,
                      textColor: appPrimaryColor,
                    ).paddingAll(3),
                  )
                ],
              ),
            ],
          ),
          SizedBox(
            height: 25,
          ),
          _joinCompetitionCard("Join Competition", 10000),
          AppPrimaryDarkButtonRound(
              width: Get.width,
              // isLoader: state is SignUpLoading ? true : false,
              borderRadius: 10.0,
              textTitle: "Invite friends to join",
              bgColor: Color(0xff96E0D2),
              textStyle: textStyleBlackColorNormal,
              functionToRun: () {}),
          SizedBox(
            height: 25,
          ),
          _hostInformationCard(),
          SizedBox(
            height: 40,
          ),
          controller.isSeeMoreTrue ? _moreDetailsWidget() : Container(),
          GestureDetector(
            onTap: () => controller.isSeeMoreTrue = !controller.isSeeMoreTrue,
            child: Align(
              alignment: Alignment.center,
              child: CustomText(
                title: controller.isSeeMoreTrue
                    ? "Show less details"
                    : "See More details",
                textSize: 14,
                isCenter: true,
                fontWeight: FontWeight.w600,
                textColor: appPrimaryColor,
              ).paddingSymmetric(vertical: 5),
            ),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ).paddingSymmetric(vertical: 20),
    );
  }

  Widget _trophyCard(String icon, String title, double amount) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ImageWidget.withSizeOnly(icon, 56, 56),
        SizedBox(
          width: 15,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title: title,
              textSize: 12,
              fontWeight: FontWeight.w500,
              textColor: Color(0xff5F6C6A),
            ),
            SizedBox(
              height: 10,
            ),
            CustomText(
              title:
                  DIConstants.moneyFormatter.returnFormattedMoney(amount, true),
              textSize: 24,
              fontWeight: FontWeight.w700,
              textColor: Color(0xff1B1C20),
            ),
          ],
        )
      ],
    ).paddingSymmetric(horizontal: 20);
  }

  Widget _joinCompetitionCard(String title, double amount) {
    return GestureDetector(
      onTap: () => _showBottomSheetForLinkAccount(),
      child: Container(
        decoration: BoxDecoration(
            color: appPrimaryColor,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Color(0xff000000).withOpacity(.9),
                blurRadius: 7.0,
                // soften the shadow
                spreadRadius: -8,
                //extend the shadow
                offset: Offset(
                  0,
                  5.0, // Move to bottom 10 Vertically
                ),
              )
            ]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
              title: title,
              textSize: 14,
              fontWeight: FontWeight.w600,
              textColor: Colors.white,
            ),
            CustomText(
              title:
                  DIConstants.moneyFormatter.returnFormattedMoney(amount, true),
              textSize: 14,
              fontWeight: FontWeight.w600,
              textColor: Colors.white,
            ),
          ],
        ).paddingSymmetric(horizontal: 15, vertical: 20),
      ),
    );
  }

  _hostInformationCard() {
    return Container(
      decoration: BoxDecoration(
          color: Color(0xffEEF2F0), borderRadius: BorderRadius.circular(10)),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              CustomText(
                title: "HOSTED BY",
                textSize: 12,
                fontWeight: FontWeight.w500,
                textColor: Color(0xff5F6C6A),
              ),
              SizedBox(
                height: 5,
              ),
              CustomText(
                title: "Jubril Abass",
                textSize: 14,
                fontWeight: FontWeight.w400,
                textColor: Color(0xff1B1C20),
              ),
            ],
          ),
          Spacer(),
          ImageWidget.withSizeOnly(
              "assets/images/svgs/whatsapp_icon.svg", 40, 40),
          SizedBox(
            width: 24,
          ),
          ImageWidget.withSizeOnly("assets/images/svgs/phone_icon.svg", 40, 40),
        ],
      ).paddingSymmetric(horizontal: 15, vertical: 20),
    );
  }

  _showBottomSheetForMoreDetails() {
    return modalBottomSheet(context, WinsCalculator(), true, Get.height / 1.5);
  }

  _moreDetailsWidget() {
    return Column(
      children: [
        moreInfoItemSnippet("Entry fee",
            DIConstants.moneyFormatter.returnFormattedMoney(10000, true)),
        moreInfoItemSnippet("Date created", "21 Oct 2021"),
        moreInfoItemSnippet("Time", "12:000pm"),
        moreInfoItemSnippet("Category", "FPL League"),
        moreInfoItemSnippet("COUNTING FROM", "Gameweek 4"),
        moreInfoItemSnippet("Winner announcement", "May 3 2021, 2 days"),
        moreInfoItemSnippet("Winners gets paid by ", "May 3 202, 2 days"),
      ],
    ).marginSymmetric(vertical: 25);
  }

  _showBottomSheetForOtherPrizes() {
    return modalBottomSheet(
        context, CompetitionParticipantsSnippet(), true, Get.height / 1.5);
  }

  _showBottomSheetForPaticipants() {
    return modalBottomSheet(context, FPLParticipantsScreen(), true, Get.height);
  }

  _showBottomSheetForLinkAccount() {
    return modalBottomSheet(context, LinkFPLDescScreen(), true, Get.height);
  }
}
