import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/Modules/CompetitionModule/Controllers/competition_controller.dart';
import 'package:tuzz/Modules/CompetitionModule/Screens/show_fpl_link_result_screen.dart';
import 'package:tuzz/Modules/DashboardModule/Screens/no_data_widget.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/inputs.dart';
import 'package:tuzz/Widgets/starter_widget.dart';
import 'package:tuzz/Widgets/text.dart';

import 'fpl_payment_confirmation_screen.dart';
import 'paste_fpl_link_screen.dart';

class ShowFPLLinkResultScreen extends StatefulWidget {
  const ShowFPLLinkResultScreen({Key? key}) : super(key: key);

  @override
  _ShowFPLLinkResultScreenState createState() =>
      _ShowFPLLinkResultScreenState();
}

class _ShowFPLLinkResultScreenState extends State<ShowFPLLinkResultScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CompetitionController>(
      init: CompetitionController(),
      builder: (controller) {
        return StarterWidget(
            hasTopWidget: false,
            hasPagePadding: true,
            isExitApp: false,
            isBgAllowed: false,
            hasBottomNav: false,
            hasRefresh: false,
            // startLoader: controller.startLoader,
            bottomPageContent: _bottomPageContent());
      },
    );
  }

  _bottomPageContent() {
    return GetBuilder<CompetitionController>(
      init: CompetitionController(),
      builder: (controller) {
        return SingleChildScrollView(
          child: Container(
            height: Get.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    GestureDetector(
                        onTap: () => Get.back(),
                        child: Icon(Icons.arrow_back_ios, size: 15).paddingAll(10)),
                    Row(
                      children: [
                        CustomText(
                          title: "WALLET",
                          textSize: 12,
                          fontWeight: FontWeight.w500,
                          textColor: Color(0xff1B1C20).withOpacity(.5),
                        ),
                        SizedBox(
                          width: 16,
                        ),
                        CustomText(
                          title: DIConstants.moneyFormatter
                              .returnFormattedMoney(0.0),
                          textSize: 16,
                          fontWeight: FontWeight.w700,
                        ).paddingAll(5),
                      ],
                    )
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ImageWidget.withSizeAndColor(
                        "assets/images/pngs/tc_fpl.png",
                        50,
                        50,
                        Color(0xff38003D),
                        true),
                    SizedBox(
                      height: 30,
                    ),
                    CustomText(
                      title:
                          "Your account is now linked to your \nFantasy premier league",
                      textSize: 14,
                      fontWeight: FontWeight.w400,
                      textColor: Color(0xff1B1C20),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Divider(
                      height: 1,
                      color: Colors.black.withOpacity(.4),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomText(
                              title: "Manager name".toUpperCase(),
                              textSize: 12,
                              fontWeight: FontWeight.w500,
                              textColor: Color(0xff5F6C6A),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            CustomText(
                              title: "Jubril Abass",
                              textSize: 16,
                              fontWeight: FontWeight.w600,
                              textColor: Color(0xff1B1C20),
                            ),
                          ],
                        ).paddingSymmetric(vertical: 9),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            CustomText(
                              title: "Team name".toUpperCase(),
                              textSize: 12,
                              fontWeight: FontWeight.w500,
                              textColor: Color(0xff5F6C6A),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            CustomText(
                              title: "Big boys FC",
                              textSize: 16,
                              fontWeight: FontWeight.w600,
                              textColor: Color(0xff1B1C20),
                            ),
                          ],
                        ).paddingSymmetric(vertical: 9),
                      ],
                    ),
                    Divider(
                      height: 1,
                      color: Colors.black.withOpacity(.4),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    GestureDetector(
                      onTap: () => Get.off(() => PasteFPLLinkScreen()),
                      child: CustomText(
                        title: "Use another account",
                        textSize: 14,
                        fontWeight: FontWeight.w700,
                        textColor: appPrimaryColor,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 2,),
                AppPrimaryDarkButtonRound(
                    width: Get.width,
                    // isLoader: state is SignUpLoading ? true : false,
                    borderRadius: 10.0,
                    textTitle: "Continue",
                    bgColor: appPrimaryColor,
                    textStyle: textStyleWhiteColorNormal,
                    functionToRun: () {
                      Get.to(() => FPLPaymentConfirmationScreen());
                    }),
                SizedBox(height: 2,),
              ],
            ).paddingSymmetric(vertical: 20),
          ),
        );
      },
    );
  }
}
