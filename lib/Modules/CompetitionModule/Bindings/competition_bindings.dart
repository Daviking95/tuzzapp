
import 'package:get/get.dart';
import 'package:tuzz/Modules/CompetitionModule/Controllers/competition_controller.dart';

class CompetitionBinding extends Bindings{

  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.put(CompetitionController());
  }
}