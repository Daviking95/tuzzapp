
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class CompetitionController extends GetxController{

  bool _isSeeMoreTrue = false.obs();
  bool _isSeeInstructionsTrue = false.obs();

  int _counter = 1.obs();

  TextEditingController _noOfPlayersController = new TextEditingController();
  TextEditingController _fplLinkController = new TextEditingController();
  TextEditingController _gameWeekTitleController = new TextEditingController();
  TextEditingController _gameWeekDescriptionController = new TextEditingController();
  TextEditingController _gameWeekParticipantController = new TextEditingController();
  TextEditingController _gameWeekAmountController = new TextEditingController();
  TextEditingController _transactionPinController = new TextEditingController();

  TextEditingController get transactionPinController =>
      _transactionPinController;

  TextEditingController get gameWeekAmountController => _gameWeekAmountController;

  TextEditingController get gameWeekParticipantController =>
      _gameWeekParticipantController;

  TextEditingController get noOfPlayersController => _noOfPlayersController;

  TextEditingController get fplLinkController => _fplLinkController;

  TextEditingController get gameWeekTitleController => _gameWeekTitleController;

  TextEditingController get gameWeekDescriptionController =>
      _gameWeekDescriptionController;

  bool get isSeeInstructionsTrue => _isSeeInstructionsTrue;

  set isSeeInstructionsTrue(bool value) {
    _isSeeInstructionsTrue = value;

    update();
  }

  bool get isSeeMoreTrue => _isSeeMoreTrue;

  set isSeeMoreTrue(bool value) {
    _isSeeMoreTrue = value;

    update();
  }

  int get counter => _counter;

  set counter(int value) {
    _counter = value;

    update();
  }

}