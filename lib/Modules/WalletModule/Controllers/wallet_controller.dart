
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Helpers/Others/print_log_formats.dart';
import 'package:tuzz/Widgets/numerical_keyboard_widget.dart';

class WalletController extends GetxController{

  TextEditingController _amountController = new TextEditingController();

  TextEditingController get amountController => _amountController;


  set amountController(TextEditingController value) {
    _amountController = value;

    update();
  }

  @override
  void onInit() {
    // TODO: implement initState
    super.onInit();
    // _amountController.text = "0";
  }

  onKeyPressed(int key, TextEditingController controller) {
    if (key == NumericalKeyboard.backspaceKey) {
      if (controller.text.length > 0) {
        controller.text =
            controller.text.substring(0, controller.text.length - 1);

        update();
      }
    } else {
      controller.text += key.toString();

      getPrintLogInDifferentWays("${controller.text}");

      update();
    }
  }
}