import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/AppConstants/StringConstants/app_constants.dart';
import 'package:tuzz/Modules/WalletModule/Controllers/wallet_controller.dart';
import 'package:tuzz/Modules/WalletModule/Screens/wallet_withdraw_success_screen.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Snippets/withdraw_transaction_snippet.dart';
import 'package:tuzz/Widgets/bottom_sheet_widget.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/inputs.dart';
import 'package:tuzz/Widgets/numerical_keyboard_widget.dart';
import 'package:tuzz/Widgets/text.dart';

class WalletWithdrawalScreen extends StatefulWidget {
  const WalletWithdrawalScreen({Key? key}) : super(key: key);

  @override
  _WalletWithdrawalScreenState createState() => _WalletWithdrawalScreenState();
}

class _WalletWithdrawalScreenState extends State<WalletWithdrawalScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<WalletController>(
      init: WalletController(),
      builder: (controller) {
        return SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      GestureDetector(
                          onTap: () => Get.back(),
                          child: Icon(Icons.arrow_back_ios, size: 15).paddingAll(10)),
                      SizedBox(
                        width: 15,
                      ),
                      CustomText(
                        title: "Withdraw",
                        textSize: 14,
                        fontWeight: FontWeight.w500,
                        textColor: Color(0xff1B1C20),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      CustomText(
                        title: "WALLET",
                        textSize: 12,
                        fontWeight: FontWeight.w500,
                        textColor: Color(0xff5F6C6A),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      CustomText(
                        title: DIConstants.moneyFormatter
                            .returnFormattedMoney(45000),
                        textSize: 16,
                        fontWeight: FontWeight.w700,
                        textColor: Color(0xff1B1C20),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 50,
              ),
              AppTextInputs(
                controller: controller.amountController,
                textInputType: TextInputType.number,
                onChange: (value) => value,
                autoFocus: true,
                isStartTextCenter: true,
                fontSize: 36,
                hasDecoration: false,
                formatter: CurrencyTextInputFormatter(
                    symbol: StringConstants.CURRENCY),
                validation: DIConstants.validators.validateString,
                textInputTitle: "",
                color: Theme.of(context).textTheme.bodyText1!.color,
              ),
              SizedBox(
                height: 50,
              ),
              CustomText(
                title: "WITHDRAW TO:",
                textSize: 12,
                fontWeight: FontWeight.w500,
                textColor: Color(0xff5F6C6A),
              ),
              withdrawTransactionSnippet(
                  "assets/images/svgs/app_logos/logo_only.svg",
                  "O122233368",
                  "Adetunji Oreoluwa",
                  _openBottomSheetForAccountSelection,
                  true),
              SizedBox(
                height: 10,
              ),
              NumericalKeyboard(
                onKeyPressed: (key) =>
                    controller.onKeyPressed(key, controller.amountController),
              ),
              SizedBox(
                height: 20,
              ),
              AppPrimaryDarkButtonRound(
                  width: Get.width,
                  // isLoader: state is SignUpLoading ? true : false,
                  borderRadius: 10.0,
                  textTitle: "Withdraw Money",
                  bgColor: appPrimaryColor,
                  textStyle: textStyleWhiteColorNormal,
                  functionToRun: () {
                    Get.to(() => WithdrawWalletSuccessScreen());
                  })
            ],
          ).paddingSymmetric(vertical: 30),
        );
      },
    );
  }

  _openBottomSheetForAccountSelection() {
    return modalBottomSheet(
        context,
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title: "Withdraw to:",
              textSize: 20,
              fontWeight: FontWeight.w700,
              textColor: Color(0xff1B1C20),
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              height: 200,
              child: ListView(
                children: [
                  selectAccountWidget(
                      "assets/images/svgs/app_logos/logo_only.svg",
                      "O122233368",
                      "Adetunji Oreoluwa",
                      "Sterling Bank",
                      true),
                  selectAccountWidget(
                      "assets/images/svgs/app_logos/logo_only.svg",
                      "O122233368",
                      "Adetunji Oreoluwa",
                      "Sterling Bank"),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            AppPrimaryDarkButtonRound(
                width: Get.width,
                // isLoader: state is SignUpLoading ? true : false,
                borderRadius: 10.0,
                textTitle: "+ Add another account",
                bgColor: Color(0xff96E0D2),
                textStyle: textStyleBlackColorNormal,
                functionToRun: () {}),
          ],
        ).paddingSymmetric(vertical: 10),
        true,
        Get.height / 1.5);
  }
}
