import 'package:flutter/material.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/Helpers/Enums/transaction_enum.dart';
import 'package:tuzz/Modules/WalletModule/Controllers/wallet_controller.dart';
import 'package:tuzz/Snippets/withdraw_transaction_snippet.dart';
import 'package:tuzz/Widgets/text.dart';
import 'package:get/get.dart';

class WalletTransactionDetailsScreen extends StatefulWidget {
  final TransactionType transactionType;

  WalletTransactionDetailsScreen(this.transactionType);

  @override
  _WalletTransactionDetailsScreenState createState() =>
      _WalletTransactionDetailsScreenState();
}

class _WalletTransactionDetailsScreenState
    extends State<WalletTransactionDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<WalletController>(
        init: WalletController(),
        builder: (controller) {
          return SingleChildScrollView(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                      onTap: () => Get.back(),
                      child: Icon(Icons.arrow_back_ios, size: 15).paddingAll(10)),
                  SizedBox(
                    width: 15,
                  ),
                  CustomText(
                    title: "Transaction Summary",
                    textSize: 14,
                    fontWeight: FontWeight.w500,
                    textColor: Color(0xff1B1C20),
                  ),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              CustomText(
                title: "Funded wallet",
                textSize: 14,
                fontWeight: FontWeight.w400,
                textColor: Color(0xff1B1C20),
              ),
              Divider(
                height: 1,
                color: Colors.black.withOpacity(.4),
              ).paddingSymmetric(vertical: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomText(
                        title: "AMOUNT",
                        textSize: 12,
                        fontWeight: FontWeight.w500,
                        textColor: Color(0xff5F6C6A),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      CustomText(
                        title: DIConstants.moneyFormatter
                            .returnFormattedMoney(5000, true),
                        textSize: 14,
                        fontWeight: FontWeight.w400,
                        textColor: Color(0xff1B1C20),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      CustomText(
                        title: "DATE",
                        textSize: 12,
                        fontWeight: FontWeight.w500,
                        textColor: Color(0xff5F6C6A),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      CustomText(
                        title: "Today, 12:30pm",
                        textSize: 14,
                        fontWeight: FontWeight.w400,
                        textColor: Color(0xff1B1C20),
                      ),
                    ],
                  ),
                ],
              ),
              Divider(
                height: 1,
                color: Colors.black.withOpacity(.4),
              ).paddingSymmetric(vertical: 20),
              _tranTypeSubInfo(widget.transactionType)
            ],
          ).paddingSymmetric(vertical: 30)
              // .paddingSymmetric(horizontal: 15),
              );
        });
  }

  _tranTypeSubInfo(TransactionType transactionType) {
    switch (transactionType) {
      case TransactionType.CREDIT:
        // TODO: Handle this case.
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title: "Funded FROM",
              textSize: 12,
              fontWeight: FontWeight.w500,
              textColor: Color(0xff5F6C6A),
            ),
            SizedBox(
              height: 15,
            ),
            selectAccountWidget(
                "assets/images/svgs/app_logos/logo_only.svg",
                "O122233368",
                "Adetunji Oreoluwa",
                "Sterling Bank",
                false,
                false,
                true),
          ],
        );
        break;
      case TransactionType.FEE:
        // TODO: Handle this case.
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  title: "COMPETITION",
                  textSize: 12,
                  fontWeight: FontWeight.w500,
                  textColor: Color(0xff5F6C6A),
                ),
                SizedBox(
                  height: 5,
                ),
                CustomText(
                  title: "Do am if e easy",
                  textSize: 14,
                  fontWeight: FontWeight.w400,
                  textColor: Color(0xff1B1C20),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                CustomText(
                  title: "CATEGORY",
                  textSize: 12,
                  fontWeight: FontWeight.w500,
                  textColor: Color(0xff5F6C6A),
                ),
                SizedBox(
                  height: 5,
                ),
                CustomText(
                  title: "FPL",
                  textSize: 14,
                  fontWeight: FontWeight.w400,
                  textColor: Color(0xff1B1C20),
                ),
              ],
            ),
          ],
        );
        break;
      case TransactionType.PRIZE:
        // TODO: Handle this case.
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  title: "COMPETITION",
                  textSize: 12,
                  fontWeight: FontWeight.w500,
                  textColor: Color(0xff5F6C6A),
                ),
                SizedBox(
                  height: 5,
                ),
                CustomText(
                  title: "Do am if e easy",
                  textSize: 14,
                  fontWeight: FontWeight.w400,
                  textColor: Color(0xff1B1C20),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                CustomText(
                  title: "CATEGORY",
                  textSize: 12,
                  fontWeight: FontWeight.w500,
                  textColor: Color(0xff5F6C6A),
                ),
                SizedBox(
                  height: 5,
                ),
                CustomText(
                  title: "FPL",
                  textSize: 14,
                  fontWeight: FontWeight.w400,
                  textColor: Color(0xff1B1C20),
                ),
              ],
            ),
          ],
        );

        break;
      case TransactionType.WITHDRAWAL:
        // TODO: Handle this case.
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              title: "TO",
              textSize: 12,
              fontWeight: FontWeight.w500,
              textColor: Color(0xff5F6C6A),
            ),
            SizedBox(
              height: 15,
            ),
            selectAccountWidget(
                "assets/images/svgs/app_logos/logo_only.svg",
                "O122233368",
                "Adetunji Oreoluwa",
                "Sterling Bank",
                false,
                false,
                true),
          ],
        );
        break;
    }
  }
}
