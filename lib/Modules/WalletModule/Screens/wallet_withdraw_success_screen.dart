
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:tuzz/Modules/DashboardModule/Screens/dashboard_screen.dart';
import 'package:tuzz/Modules/WalletModule/Controllers/wallet_controller.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Snippets/withdraw_transaction_snippet.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/starter_widget.dart';
import 'package:tuzz/Widgets/text.dart';

class WithdrawWalletSuccessScreen extends StatefulWidget {
  const WithdrawWalletSuccessScreen({Key? key}) : super(key: key);

  @override
  _WithdrawWalletSuccessScreenState createState() => _WithdrawWalletSuccessScreenState();
}

class _WithdrawWalletSuccessScreenState extends State<WithdrawWalletSuccessScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<WalletController>(
      init: WalletController(),
      builder: (controller) {
        return StarterWidget(
            hasTopWidget: false,
            hasPagePadding: true,
            isExitApp: false,
            isBgAllowed: false,
            hasBottomNav: false,
            hasRefresh: false,
            bottomPageContent: _bottomPageContent(controller));
      },
    );
  }

  _bottomPageContent(WalletController controller) {
    return Container(
      height: Get.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Lottie.asset("assets/images/lottie_images/success.json", width: 140),
          SizedBox(height: 30,),
          CustomText(
            title: "Success!",
            textSize: 24,
            isCenter: true,
            fontWeight: FontWeight.w700,
            textColor: Color(0xff1B1C20),
          ),
          SizedBox(height: 10,),
          CustomText(
            title: "You have successfully withdrawn ₦5,000. \nBank charge: ₦25",
            textSize: 14,
            isCenter: true,
            fontWeight: FontWeight.w400,
            textColor: Color(0xff5F6C6A),
          ),
          SizedBox(
            height: 50,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: CustomText(
              title: "RECEPEINT",
              textSize: 12,
              fontWeight: FontWeight.w500,
              textColor: Color(0xff5F6C6A),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          selectAccountWidget(
              "assets/images/svgs/app_logos/logo_only.svg",
              "O122233368",
              "Adetunji Oreoluwa",
              "Sterling Bank",
              false, true, true),
          SizedBox(
            height: 50,
          ),
          AppPrimaryDarkButtonRound(
              width: Get.width,
              // isLoader: state is SignUpLoading ? true : false,
              borderRadius: 10.0,
              textTitle: "Go to Dashboard",
              bgColor: appPrimaryColor,
              textStyle: textStyleWhiteColorNormal,
              functionToRun: () {
                Get.to(() => DashboardScreen());
              })
        ],
      ),
    );
  }
}
