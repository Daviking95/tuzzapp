import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/Modules/WalletModule/Screens/wallet_withdrawal_screen.dart';
import 'package:tuzz/Modules/WalletModule/Screens/wallet_transaction_screen.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Snippets/withdraw_transaction_snippet.dart';
import 'package:tuzz/Widgets/bottom_sheet_widget.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/text.dart';
import 'package:tuzz/Widgets/toasts_and_snacks.dart';

class WalletInfoScreen extends StatefulWidget {
  const WalletInfoScreen({Key? key}) : super(key: key);

  @override
  _WalletInfoScreenState createState() => _WalletInfoScreenState();
}

class _WalletInfoScreenState extends State<WalletInfoScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              GestureDetector(
                  onTap: () => Get.back(),
                  child: Icon(Icons.arrow_back_ios, size: 15).paddingAll(10)),
              SizedBox(width: 15,),
              CustomText(
                title: "Wallet",
                textSize: 14,
                fontWeight: FontWeight.w400,
                textColor: Color(0xff1B1C20),
              ),
            ],
          ),
          SizedBox(height: 50,),
          CustomText(
            title: "WALLET BALANCE",
            textSize: 12,
            fontWeight: FontWeight.w500,
            textColor: Color(0xff5F6C6A),
          ),
          SizedBox(height: 16,),
          CustomText(
            title: DIConstants.moneyFormatter.returnFormattedMoney(
                450000, true),
            textSize: 36,
            fontWeight: FontWeight.w700,
            textColor: Color(0xff1B1C20),
          ),
          SizedBox(height: 60,),
          Container(
            decoration: BoxDecoration(
                color: Color(0xffDFFFF4),
                borderRadius: BorderRadius.circular(16)
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  title: "Fund Wallet",
                  textSize: 18,
                  fontWeight: FontWeight.w700,
                  textColor: Color(0xff1B1C20),
                ),
                SizedBox(height: 8,),
                CustomText(
                  title: "We have created a unique sterling bank account \nnumber for you. send money to the account \nbelow to fund your wallet",
                  textSize: 12,
                  fontWeight: FontWeight.w400,
                  textColor: Color(0xff5F6C6A),
                ),
                SizedBox(height: 24,),
                Row(
                  children: [
                    CustomText(
                      title: "5256187652",
                      textSize: 20,
                      fontWeight: FontWeight.w700,
                      textColor: Color(0xff1B1C20),
                    ),
                    SizedBox(width: 10,),
                    GestureDetector(
                        onTap: () {
                          Clipboard.setData(ClipboardData(text: "your text"));
                          showToast(toastMsg: "Copied to Clipboard", toastLength: Toast
                              .LENGTH_LONG,
                              toastGravity: ToastGravity.BOTTOM,
                              bgColor: appPrimaryColor,
                              txtColor: appWhiteColor);
                        },
                        child: Icon(LineIcons.copy, color: Color(0xff8DAAA5),)
                            .paddingAll(5))
                  ],
                ),
                SizedBox(height: 15,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomText(
                      title: "Sterling bank",
                      textSize: 14,
                      fontWeight: FontWeight.w400,
                      textColor: Color(0xff5F6C6A),
                    ),
                    CustomText(
                      title: "Michael scott",
                      textSize: 14,
                      fontWeight: FontWeight.w400,
                      textColor: Color(0xff5F6C6A),
                    ),
                  ],
                ),
                SizedBox(height: 10,),
              ],
            ).paddingAll(24),
          ).marginSymmetric(horizontal: 10),
          SizedBox(height: 30,),
          withdrawTransactionSnippet("assets/images/svgs/withdraw_icon.svg", "Withdraw", "Withdraw money to your bank", _openBottomSheetForWithdraws),
          withdrawTransactionSnippet("assets/images/svgs/transaction_icon.svg", "Transactions", "See your wallet transactions", _openBottomSheetForTransactions)
        ],
      ).paddingSymmetric(vertical: 30),
    );
  }

  _openBottomSheetForWithdraws() {
    return modalBottomSheet(
        context,
        WalletWithdrawalScreen(),
        true,
        Get.height
    );
  }

  _openBottomSheetForTransactions() {
    return modalBottomSheet(
        context,
        WalletTransactionsScreen(),
        true,
        Get.height
    );
  }
}

