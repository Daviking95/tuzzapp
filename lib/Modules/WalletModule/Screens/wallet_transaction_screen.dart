import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/Helpers/Enums/transaction_enum.dart';
import 'package:tuzz/Modules/WalletModule/Controllers/wallet_controller.dart';
import 'package:tuzz/Modules/WalletModule/Screens/wallet_transaction_details_screen.dart';
import 'package:tuzz/Widgets/bottom_sheet_widget.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/text.dart';

class WalletTransactionsScreen extends StatefulWidget {
  const WalletTransactionsScreen({Key? key}) : super(key: key);

  @override
  _WalletTransactionsScreenState createState() =>
      _WalletTransactionsScreenState();
}

class _WalletTransactionsScreenState extends State<WalletTransactionsScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<WalletController>(
        init: WalletController(),
        builder: (controller) {
          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    GestureDetector(
                        onTap: () => Get.back(),
                        child: Icon(Icons.arrow_back_ios, size: 15).paddingAll(10)),
                    SizedBox(
                      width: 15,
                    ),
                    CustomText(
                      title: "Transactions",
                      textSize: 14,
                      fontWeight: FontWeight.w500,
                      textColor: Color(0xff1B1C20),
                    ),

                  ],
                ),
                SizedBox(
                  height: 25,
                ),
                _transWidget("assets/images/svgs/trans_credit.svg", "Funded wallet", "Today, 12:30pm", 5000, TransactionType.CREDIT),
                _transWidget("assets/images/svgs/trans_fee.svg", "Entry fee", "Today, 12:30pm", 5000, TransactionType.FEE),
                _transWidget("assets/images/svgs/trans_winner.svg", "Winner Prize", "Today, 12:30pm", 5000, TransactionType.PRIZE),
                _transWidget("assets/images/svgs/trans_withdrawal.svg", "Withdrawal", "Today, 12:30pm", 5000, TransactionType.WITHDRAWAL)

              ],
            ).paddingSymmetric(vertical: 30),
          );
        });
  }

  Widget _transWidget(String icon, String title, String date, double amount, TransactionType transactionType) {
    return GestureDetector(
      onTap: () => _openBottomSheetForTransaction(transactionType),
      child: Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              color: _getBgColor(transactionType)),
                          child: ImageWidget.withSizeOnly(
                              icon, 24, 24)
                              .paddingAll(8),
                        ),
                        SizedBox(width: 15,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomText(
                              title: title,
                              textSize: 14,
                              fontWeight: FontWeight.w600,
                              textColor: Color(0xff1B1C20),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            CustomText(
                              title: date,
                              textSize: 12,
                              fontWeight: FontWeight.w400,
                              textColor: Color(0xff1B1C20),
                            ),
                          ],
                        ),
                        Spacer(),
                        CustomText(
                          title: DIConstants.moneyFormatter.returnFormattedMoney(amount, true),
                          textSize: 16,
                          fontWeight: FontWeight.w600,
                          textColor: Color(0xff1B1C20),
                        ),
                      ],
                    ),
                    Divider(height: 1, color: Color(0xff1B1C20).withOpacity(.10),).paddingSymmetric(vertical: 15)
                  ],
                ),
    );
  }

  _getBgColor(TransactionType transactionType) {
    switch (transactionType){

      case TransactionType.CREDIT:
        // TODO: Handle this case.
      return Color(0xffDFFFF4);
        break;
      case TransactionType.FEE:
        // TODO: Handle this case.
        return Color(0xffFFDDF7);

        break;
      case TransactionType.PRIZE:
        // TODO: Handle this case.
        return Color(0xffFFF7C0);

        break;
      case TransactionType.WITHDRAWAL:
        // TODO: Handle this case.
        return Color(0xffEEF2F0);

        break;
    }
  }

  _openBottomSheetForTransaction(TransactionType transactionType) {
      return modalBottomSheet(
          context,
          WalletTransactionDetailsScreen(transactionType),
          true,
          Get.height
      );
  }
}
