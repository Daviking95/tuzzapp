

import 'package:get/get.dart';

class NotificationController extends GetxController{

  bool _startLoader = false.obs();

  bool get startLoader => _startLoader;

  set startLoader(bool value) {
    _startLoader = value;

    update();
  }

}