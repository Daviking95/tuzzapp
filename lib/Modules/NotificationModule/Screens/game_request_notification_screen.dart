
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Modules/NotificationModule/Controllers/notification_controller.dart';
import 'package:tuzz/Modules/NotificationModule/Screens/notification_screen.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/bottom_sheet_widget.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/starter_widget.dart';
import 'package:tuzz/Widgets/text.dart';

class GameRequestNotificationScreen extends StatefulWidget {
  const GameRequestNotificationScreen({Key? key}) : super(key: key);

  @override
  _GameRequestNotificationScreenState createState() => _GameRequestNotificationScreenState();
}

class _GameRequestNotificationScreenState extends State<GameRequestNotificationScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<NotificationController>(
      init: NotificationController(),
      builder: (controller) {
        return StarterWidget(
            hasTopWidget: false,
            hasPagePadding: true,
            isExitApp: false,
            isBgAllowed: false,
            isBackWidget: true,
            backUrlWidgetScreen: NotificationScreen(),
            hasRefresh: true,
            startLoader: controller.startLoader,
            bottomPageContent: _bottomPageContent(controller));
      },
    );
  }

  _bottomPageContent(NotificationController controller) {
    return Column(
      children: [
        GestureDetector(
          onTap: () => Get.to(() => NotificationScreen()),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(Icons.keyboard_arrow_left),
              SizedBox(width: 10,),
              CustomText(
                title: "Requests",
                textColor: appPrimaryColor,
                textSize: 14,
                fontWeight: FontWeight.w600,
              ).paddingAll(5),
            ],
          ),
        ),
        SizedBox(
          height: 30,
        ),
        Container(
          height: Get.height,
          child: ListView.builder(
              itemCount: 3,
              itemBuilder: (context, index) {
                return _requestNotificationItem();
              }),
        )
      ],
    ).marginSymmetric(vertical: 15);
  }

  _requestNotificationItem() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: Color(0xff1B1C20).withOpacity(.1)),
      ),
      child: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () => modalBottomSheet(context, _userProfileWidget(), true, Get.height / 1.7),
                child: Row(
                  children: [
                    ImageWidget.withSizeOnly("assets/images/pngs/partis.png", 30, 30, true),
                    SizedBox(width: 10,),
                    CustomText(
                      title: "Tammy Abraham",
                      textColor: Color(0xff1B1C20),
                      textSize: 14,
                      fontWeight: FontWeight.w600,
                    )
                  ],
                ),
              ),
              SizedBox(height: 15,),
              CustomText(
                title: "Jonathan Championship 2021 ",
                textColor: Color(0xff1B1C20),
                textSize: 14,
                fontWeight: FontWeight.w400,
              ),
              SizedBox(height: 5,),
              CustomText(
                title: "3 Oct, 12:30pm",
                textColor: Color(0xff5F6C6A),
                textSize: 12,
                fontWeight: FontWeight.w400,
              ),

            ],
          ).paddingSymmetric(horizontal: 15, vertical: 10),
          SizedBox(height: 15,),
          Divider(height: 1, color: Color(0xff1B1C20).withOpacity(.1)),
          IntrinsicHeight(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  onTap: () => modalBottomSheet(context, _acceptRejectWidget(), true, Get.height / 2),
                  child: CustomText(
                    title: "Decline",
                    textColor: Colors.red,
                    textSize: 14,
                    fontWeight: FontWeight.w400,
                  ).paddingSymmetric(vertical: 15),
                ),
                VerticalDivider(
                  width: 1,
                  color: Color(0xff1B1C20).withOpacity(.2),
                ),
                GestureDetector(
                  onTap: () => modalBottomSheet(context, _acceptRejectWidget(true), true, Get.height / 2),
                  child: CustomText(
                    title: "Accept",
                    textColor: appPrimaryColor,
                    textSize: 14,
                    fontWeight: FontWeight.w400,
                  ).paddingSymmetric(vertical: 15),
                ),
              ],
            ),
          )
        ],
      ),
    ).marginSymmetric(vertical: 10);
  }

  Widget _acceptRejectWidget([bool isAccept = false]) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Align(
          alignment: Alignment.centerRight,
          child: GestureDetector(
            onTap: () => Navigator.pop(context),
              child: Icon(Icons.close).paddingAll(5)),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomText(
              title: "Do you want to ${isAccept ? 'accept' : 'decline'}? ",
              textColor: Color(0xff1B1C20),
              textSize: 20,
              fontWeight: FontWeight.w700,
            ),
            SizedBox(height: 5,),
            CustomText(
              title: "Are you sure you want to ${isAccept ? 'accept' : 'decline'} this request? ",
              textColor: Color(0xff5F6C6A),
              textSize: 12,
              fontWeight: FontWeight.w400,
            ),
          ],
        ),
        Column(
          children: [
            AppPrimaryDarkButtonRound(
                width: Get.width,
                // isLoader: state is SignUpLoading ? true : false,
                borderRadius: 10.0,
                textTitle: "Yes, ${isAccept ? 'accept' : 'decline'}",
                bgColor: appPrimaryColor,
                textStyle: textStyleWhiteColorNormal,
                functionToRun: () {}),
            AppPrimaryDarkButtonRound(
                width: Get.width,
                // isLoader: state is SignUpLoading ? true : false,
                borderRadius: 10.0,
                textTitle: "No",
                bgColor: Color(0xff96E0D2),
                textStyle: textStyleBlackColorNormal,
                functionToRun: () {})
          ],
        )
      ],
    ).paddingSymmetric(vertical: 10);
  }

  Widget _userProfileWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
              title: "Details ",
              textColor: Color(0xff1B1C20),
              textSize: 20,
              fontWeight: FontWeight.w700,
            ),
            Align(
              alignment: Alignment.centerRight,
              child: GestureDetector(
                  onTap: () => Navigator.pop(context),
                  child: Icon(Icons.close).paddingAll(5)),
            ),
          ],
        ).paddingSymmetric(horizontal: 15),
        SizedBox(height: 30,),
        Row(
          children: [
            ImageWidget.withSizeOnly("assets/images/pngs/partis.png", 50, 50, true),
            SizedBox(width: 10,),
            CustomText(
              title: "Tammy Abraham",
              textColor: Color(0xff1B1C20),
              textSize: 14,
              fontWeight: FontWeight.w600,
            )
          ],
        ).paddingSymmetric(horizontal: 15),
        SizedBox(height: 40,),
        CustomText(
          title: "Contact details",
          textSize: 16,
          fontWeight: FontWeight.w600,
          textColor: Color(0xff51B1C20),
        ).paddingSymmetric(horizontal: 15),
        SizedBox(height: 20,),
        _contactItem("WHATSAPP", "0901 782 1834", "assets/images/svgs/whatsapp_icon_2.svg"),
        SizedBox(height: 24,),
        _contactItem("PHONE NUMBER", "0901 782 1834", "assets/images/svgs/phone_icon_2.svg"),
        SizedBox(height: 24,),
        AppPrimaryDarkButtonRound(
            width: Get.width,
            // isLoader: state is SignUpLoading ? true : false,
            borderRadius: 10.0,
            textTitle: "Done",
            bgColor: appPrimaryColor,
            textStyle: textStyleWhiteColorNormal,
            functionToRun: () {}),
      ],
    ).paddingSymmetric(vertical: 10);
  }

  ListTile _contactItem(String title, String value, String icon) {
    return ListTile(
      leading:
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomText(
            title: title,
            textSize: 12,
            fontWeight: FontWeight.w500,
            textColor: Color(0xff5F6C6A),
          ),
          SizedBox(height: 10,),
          CustomText(
            title: value,
            textSize: 14,
            fontWeight: FontWeight.w400,
            textColor: Color(0xff1B1C20),
          ),
        ],
      ),
      trailing: ImageWidget.withSizeOnly(icon, 40, 40).paddingAll(5),
    );
  }

}
