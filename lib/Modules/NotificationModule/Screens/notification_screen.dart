import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Modules/DashboardModule/Screens/dashboard_screen.dart';
import 'package:tuzz/Modules/DashboardModule/Screens/no_data_widget.dart';
import 'package:tuzz/Modules/NotificationModule/Controllers/notification_controller.dart';
import 'package:tuzz/Modules/NotificationModule/Screens/game_request_notification_screen.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/starter_widget.dart';
import 'package:tuzz/Widgets/text.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<NotificationController>(
      init: NotificationController(),
      builder: (controller) {
        return StarterWidget(
            hasTopWidget: false,
            hasPagePadding: true,
            isExitApp: false,
            isBgAllowed: false,
            isBackWidget: true,
            backUrlWidgetScreen: DashboardScreen(),
            hasBottomNav: true,
            bottomNavIndex: 1,
            hasRefresh: true,
            startLoader: controller.startLoader,
            bottomPageContent: _bottomPageContent(controller));
      },
    );
  }

  _bottomPageContent(NotificationController controller) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
              title: "Notifications",
              textColor: Color(0xff1B1C20),
              textSize: 24,
              fontWeight: FontWeight.w700,
            ),
            GestureDetector(
              onTap: () => Get.to(() => GameRequestNotificationScreen()),
              child: CustomText(
                title: "Requests",
                textColor: appPrimaryColor,
                textSize: 14,
                fontWeight: FontWeight.w600,
              ).paddingAll(5),
            ),
          ],
        ),
        SizedBox(
          height: 30,
        ),
        Container(
          height: Get.height,
          child: ListView.builder(
              itemCount: 3,
              itemBuilder: (context, index) {
                return _notificationItem();
              }),
        )
      ],
    ).marginSymmetric(vertical: 15);
  }

  _notificationItem() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomText(
          title:
              "You won ₦32,000 in a FIFA Competition, funds will be sent to your wallet ",
          textSize: 14,
          textColor: Color(0xff1B1C20),
          fontWeight: FontWeight.w400,
        ),
        SizedBox(
          height: 10,
        ),
        CustomText(
          title: "Today, 12:30 PM",
          textSize: 12,
          textColor: Color(0xff5F6C6A),
          fontWeight: FontWeight.w400,
        ),
        Divider(
          height: 1,
          color: Colors.black.withOpacity(.4),
        ).paddingSymmetric(vertical: 15)
      ],
    );
  }
}
