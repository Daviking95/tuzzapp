
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OnboardingController extends GetxController{

  bool _checkHowToBVN = false.obs();

  TextEditingController _phoneNumberController = new TextEditingController();

  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _firstNameController = new TextEditingController();
  TextEditingController _lastNameController = new TextEditingController();
  TextEditingController _bvnController = new TextEditingController();
  TextEditingController _dateOfBirthController = new TextEditingController();

  TextEditingController get phoneNumberController => _phoneNumberController;

  TextEditingController get passwordController => _passwordController;

  TextEditingController get lastNameController => _lastNameController;

  TextEditingController get firstNameController => _firstNameController;

  TextEditingController get dateOfBirthController => _dateOfBirthController;

  TextEditingController get bvnController => _bvnController;

  bool get checkHowToBVN => _checkHowToBVN;

  set checkHowToBVN(bool value) {
    _checkHowToBVN = value;

    update();
  }
}