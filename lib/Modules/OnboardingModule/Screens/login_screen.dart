import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Modules/OnboardingModule/Controllers/onboarding_controller.dart';
import 'package:tuzz/Modules/OnboardingModule/Screens/forgot_password_screen.dart';
import 'package:tuzz/Modules/OnboardingModule/Screens/register_screen.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/bottom_sheet_widget.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/inputs.dart';
import 'package:tuzz/Widgets/text.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<OnboardingController>(
      init: OnboardingController(),
      builder: (controller) {
        return Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomText(
                    title: "Log In",
                    textColor: Color(0xff1B1C20),
                    textSize: 20,
                    fontWeight: FontWeight.w700,
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                      child: Icon(Icons.close))
                ],
              ),
              SizedBox(
                height: 15,
              ),
              CustomText(
                title:
                    "You need to login to your account to continue.",
                textSize: 14,
                fontWeight: FontWeight.w400,
              ),
              SizedBox(
                height: 5,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                  modalBottomSheet(context, RegisterScreen(), true, Get.height);
                },
                child: Text.rich(
                    TextSpan(
                        text: 'Don’t have an account? ',
                        children: <InlineSpan>[
                          TextSpan(
                            text: 'Create One',
                            style: textStylePrimaryColorNormal,
                          )
                        ]
                    )
                ),
              ),
              SizedBox(
                height: 50,
              ),
              PhoneNumberTextInput(
                  controller: controller.phoneNumberController, color: Color(0xff1B1C20).withOpacity(.2),),
              SizedBox(
                height: 5,
              ),
              GestureDetector(
                onTap: () {
                  // Navigator.pop(context);
                  modalBottomSheet(context, ForgotPasswordScreen(), true, Get.height / 1.5);
                },
                child: Align(
                  alignment: Alignment.centerRight,
                  child: CustomText(
                    title:
                    "Forgot Password",
                    textSize: 12,
                    fontWeight: FontWeight.w400,
                    textColor: appPrimaryColor,
                  ),
                ),
              ),
              AppPasswordTextInput(
                color: Color(0xff1B1C20).withOpacity(.2),
                controller: controller.passwordController,
                textInputTitle: "Password",
                isThereNoBottomPadding: true,
              ),
              SizedBox(
                height: 25,
              ),
              AppPrimaryDarkButtonRound(
                  width: Get.width,
                  // isLoader: state is SignUpLoading ? true : false,
                  borderRadius: 10.0,
                  textTitle: "Log In",
                  bgColor: Color(0xff96E0D2),
                  textStyle: textStyleWhiteColorNormal,
                  functionToRun: () {})
            ],
          ).paddingSymmetric(vertical: 10, horizontal: 10),
        );
      },
    );
  }
}
