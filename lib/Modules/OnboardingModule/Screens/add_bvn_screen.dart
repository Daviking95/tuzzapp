import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icon.dart';
import 'package:line_icons/line_icons.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/Modules/OnboardingModule/Controllers/onboarding_controller.dart';
import 'package:tuzz/Modules/OnboardingModule/Screens/login_screen.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/bottom_sheet_widget.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/inputs.dart';
import 'package:tuzz/Widgets/text.dart';

class AddBvnScreen extends StatefulWidget {
  const AddBvnScreen({Key? key}) : super(key: key);

  @override
  _AddBvnScreenState createState() => _AddBvnScreenState();
}

class _AddBvnScreenState extends State<AddBvnScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<OnboardingController>(
      init: OnboardingController(),
      builder: (controller) {
        return Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                        modalBottomSheet(context, LoginScreen(), true, Get.height / 1.5);
                      },
                      child: Icon(Icons.close, size: 15,)),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              ImageWidget.withSizeOnly(
                  "assets/images/svgs/profile.svg", 40, 40),
              SizedBox(
                height: 20,
              ),
              CustomText(
                title: "Add your BVN",
                textColor: Color(0xff1B1C20),
                textSize: 20,
                fontWeight: FontWeight.w700,
              ),
              SizedBox(
                height: 15,
              ),
              CustomText(
                title: "We need your BVN details to create \na wallet for you",
                textSize: 14,
                fontWeight: FontWeight.w400,
              ),
              SizedBox(
                height: 25,
              ),
              GestureDetector(
                onTap: () =>
                    controller.checkHowToBVN = !controller.checkHowToBVN,
                child: Row(
                  children: [
                    CustomText(
                      title: "See how to find your BVN number? ",
                      textSize: 12,
                      fontWeight: FontWeight.w400,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Icon(
                      controller.checkHowToBVN
                          ? LineIcons.arrowCircleDown
                          : LineIcons.arrowCircleRight,
                      size: 20,
                      color: appPrimaryColor,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              controller.checkHowToBVN
                  ? Container(
                width: Get.width,
                    child: Card(
                elevation: 0,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                color: Color(0xffFFDDF7),
                child: CustomText(
                    title: "Dail *565*0# with the sim card you enrolled for \nyour BVN with",
                    textSize: 12,
                    fontWeight: FontWeight.w400,
                ).paddingSymmetric(horizontal: 15, vertical: 20),
              ),
                  ) : Container(),
              SizedBox(
                height: 30,
              ),
              AppTextInputs(
                  textInputType: TextInputType.text,
                  onChange: (value) => value,
                  validation: DIConstants.validators.validateString,
                  textInputTitle: "BVN",
                  maxLength: 11,
                  controller: controller.bvnController,
                  color: Color(0xff1B1C20).withOpacity(.2)),
              DashboardDatePicker(
                  text: "Date of Birth",
                  noOfYears: 18,
                  isLastDate: true,
                  datePickerController: controller.dateOfBirthController),
              SizedBox(
                height: 25,
              ),
              AppPrimaryDarkButtonRound(
                  width: Get.width,
                  // isLoader: state is SignUpLoading ? true : false,
                  borderRadius: 10.0,
                  textTitle: "Submit",
                  bgColor: Color(0xff96E0D2),
                  textStyle: textStyleWhiteColorNormal,
                  functionToRun: () {
                    Navigator.pop(context);
                    modalBottomSheet(context, LoginScreen(), true, Get.height / 1.5);
                  }),
              SizedBox(
                height: 25,
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                  modalBottomSheet(context, LoginScreen(), true, Get.height / 1.5);
                },
                child: Center(
                  child: CustomText(
                    title: "I'll do this later",
                    textSize: 14,
                    isCenter: true,
                    textColor: appPrimaryColor,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ],
          ).paddingSymmetric(vertical: 10, horizontal: 10),
        );
      },
    );
  }
}
