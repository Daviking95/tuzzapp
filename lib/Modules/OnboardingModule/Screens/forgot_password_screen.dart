import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:tuzz/AppConstants/RoutesConstants/route_constants.dart';
import 'package:tuzz/Modules/DashboardModule/Screens/dashboard_screen.dart';
import 'package:tuzz/Modules/OnboardingModule/Controllers/onboarding_controller.dart';
import 'package:tuzz/Modules/OnboardingModule/Screens/forgot_password_screen.dart';
import 'package:tuzz/Modules/OnboardingModule/Screens/register_screen.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/bottom_sheet_widget.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/inputs.dart';
import 'package:tuzz/Widgets/text.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<OnboardingController>(
      init: OnboardingController(),
      builder: (controller) {
        return Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                      onTap: () => Get.back(),
                      child: Icon(Icons.arrow_back)),
                  GestureDetector(
                      onTap: () => Navigator.pop(context),
                      child: Icon(Icons.close))
                ],
              ),
              SizedBox(
                height: 10,
              ),
              CustomText(
                title: "Forgot Password",
                textColor: Color(0xff1B1C20),
                textSize: 20,
                fontWeight: FontWeight.w700,
              ),
              SizedBox(
                height: 15,
              ),
              CustomText(
                title:
                "Input the phone number you registered with, a change \npassword link will be sent to your whatsapp",
                textSize: 12,
                fontWeight: FontWeight.w400,
              ),
              SizedBox(
                height: 50,
              ),
              PhoneNumberTextInput(
                controller: controller.phoneNumberController, color: Color(0xff1B1C20).withOpacity(.2),),
              SizedBox(
                height: 25,
              ),
              AppPrimaryDarkButtonRound(
                  width: Get.width,
                  // isLoader: state is SignUpLoading ? true : false,
                  borderRadius: 10.0,
                  textTitle: "Send reset password link",
                  bgColor: appPrimaryColor,
                  textStyle: textStyleWhiteColorNormal,
                  functionToRun: () {
                    Navigator.pop(context);
                    modalBottomSheet(context, _forgotPasswordSuccessScree(), true, Get.height / 1.5);
                  })
            ],
          ).paddingSymmetric(vertical: 10, horizontal: 10),
        );
      },
    );
  }

  Widget _forgotPasswordSuccessScree() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(height: 50,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Lottie.asset("assets/images/lottie_images/success.json", width: 140),
            SizedBox(height: 10,),

            CustomText(
              title: "Link sent to your whatsapp",
              isCenter: true,
              textSize: 18,
              fontWeight: FontWeight.w700,
              textColor: Color(0xff1B1C20),
            ),
            SizedBox(
              height: 15,
            ),
            CustomText(
              title:
              "Your password rest link sent to \nyour whatsapp number",
              textSize: 14,
              isCenter: true,
              fontWeight: FontWeight.w400,
              textColor: Color(0xff5F6C6A),
            ),
            SizedBox(
              height: 25,
            ),
          ],
        ),
        Column(
          children: [
            AppPrimaryDarkButtonRound(
                width: Get.width,
                // isLoader: state is SignUpLoading ? true : false,
                borderRadius: 10.0,
                textTitle: "Done",
                bgColor: appPrimaryColor,
                textStyle: textStyleWhiteColorNormal,
                routeToGo: RoutesConstants.DASHBOARD_URL,
                functionToRun: () {
                  Get.to(() => DashboardScreen());
                  // Navigator.pop(context);
                }),

          ],
        ),

        SizedBox(height: 2,),
      ],
    );
  }
}
