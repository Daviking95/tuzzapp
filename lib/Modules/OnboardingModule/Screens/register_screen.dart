import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/Modules/OnboardingModule/Controllers/onboarding_controller.dart';
import 'package:tuzz/Modules/OnboardingModule/Screens/add_bvn_screen.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/bottom_sheet_widget.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/inputs.dart';
import 'package:tuzz/Widgets/text.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<OnboardingController>(
      init: OnboardingController(),
      builder: (controller) {
        return Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                      onTap: () => Get.back(), child: Icon(Icons.keyboard_arrow_left_outlined)),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              CustomText(
                title: "Create Account",
                textColor: Color(0xff1B1C20),
                textSize: 20,
                fontWeight: FontWeight.w700,
              ),
              SizedBox(
                height: 15,
              ),
              CustomText(
                title:
                    "Kindly, input your details to create an \naccount on Tuzz",
                textSize: 12,
                isCenter: true,
                fontWeight: FontWeight.w400,
              ),
              SizedBox(
                height: 50,
              ),
              AppTextInputs(
                  textInputType: TextInputType.text,
                  onChange: (value) => value,
                  validation: DIConstants.validators.validateString,
                  textInputTitle: "First Name",
                  controller: controller.firstNameController,
                  color: Color(0xff1B1C20).withOpacity(.2)),
              AppTextInputs(
                  textInputType: TextInputType.text,
                  onChange: (value) => value,
                  validation: DIConstants.validators.validateString,
                  textInputTitle: "Last Name",
                  controller: controller.lastNameController,
                  color: Color(0xff1B1C20).withOpacity(.2)),
              SizedBox(
                height: 5,
              ),
              PhoneNumberTextInput(
                controller: controller.phoneNumberController,
                color: Color(0xff1B1C20).withOpacity(.2),
              ),
              SizedBox(
                height: 5,
              ),
              AppPasswordTextInput(
                color: Color(0xff1B1C20).withOpacity(.2),
                controller: controller.passwordController,
                textInputTitle: "Password",
                isThereNoBottomPadding: true,
              ),
              SizedBox(
                height: 25,
              ),
              AppPrimaryDarkButtonRound(
                  width: Get.width,
                  // isLoader: state is SignUpLoading ? true : false,
                  borderRadius: 10.0,
                  textTitle: "Create account",
                  bgColor: Color(0xff96E0D2),
                  textStyle: textStyleWhiteColorNormal,
                  functionToRun: () {
                    Navigator.pop(context);
                    modalBottomSheet(context, AddBvnScreen(), true, Get.height);
                  }),
              SizedBox(
                height: 25,
              ),
              GestureDetector(
                onTap: () {
                },
                child: Column(
                  children: [
                    CustomText(
                      title:
                      "By clicking on create account, you agree to our",
                      textSize: 12,
                      fontWeight: FontWeight.w400,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    CustomText(
                      title:
                      "Terms, Data policy",
                      textSize: 12,
                      textColor: appPrimaryColor,
                      fontWeight: FontWeight.w400,
                    ),
                  ],
                ),
              ),
            ],
          ).paddingSymmetric(vertical: 10, horizontal: 10),
        );
      },
    );
  }
}
