
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Snippets/games_card_snippets.dart';
import 'package:tuzz/Widgets/image_widget.dart';

class MobileDataWidget extends StatefulWidget {
  const MobileDataWidget({Key? key}) : super(key: key);

  @override
  _MobileDataWidgetState createState() => _MobileDataWidgetState();
}

class _MobileDataWidgetState extends State<MobileDataWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 40,
          width: Get.width,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: 8,
            itemBuilder: (BuildContext context, int index) {
              return index == 0
                  ? ImageWidget.withSizeOnly(
                  "assets/images/svgs/filter.svg", 40, 30).marginOnly(right: 10)
                  : FilterCardSnippet();
            },
            // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            //     crossAxisCount: 2, mainAxisExtent: 120, crossAxisSpacing: 4.0, mainAxisSpacing: 4.0),
          ),
        ),
        SizedBox(height: 30,),
        Container(
          height: Get.height,
          child: GridView.builder(
            scrollDirection: Axis.vertical,
            physics: NeverScrollableScrollPhysics(),
            itemCount: 4,
            itemBuilder: (BuildContext context, int index) {
              return GamesCardSnippet();
            },
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, mainAxisExtent: 200, crossAxisSpacing: 4.0, mainAxisSpacing: 16.0),
          ),
        )
      ],
    );
  }
}
