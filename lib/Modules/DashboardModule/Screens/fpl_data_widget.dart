import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Snippets/games_card_snippets.dart';
import 'package:tuzz/Widgets/bottom_sheet_widget.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/text.dart';

import 'filter_for_fpl_widget.dart';

class FplDataWidget extends StatefulWidget {
  const FplDataWidget({Key? key}) : super(key: key);

  @override
  _FplDataWidgetState createState() => _FplDataWidgetState();
}

class _FplDataWidgetState extends State<FplDataWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
              title: "Top Leagues",
              textSize: 16,
              fontWeight: FontWeight.w600,
              textColor: Color(0xff1B1C20),
            ),
            CustomText(
              title: "View Leagues",
              textSize: 14,
              fontWeight: FontWeight.w600,
              textColor: Color(0xff11A683),
            ),
          ],
        ),
        SizedBox(
          height: 22,
        ),
        Container(
          height: 180,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: 3,
            itemBuilder: (BuildContext context, int index) {
              return GamesCardSnippet();
            },
          ),
        ),
        SizedBox(
          height: 30,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
              title: "Tuzzes for Gameweek 3",
              textSize: 16,
              fontWeight: FontWeight.w600,
              textColor: Color(0xff1B1C20),
            ),
            GestureDetector(
                onTap: () => _bottomSheetForFilter(),
                child: ImageWidget.withSizeOnly(
                    "assets/images/svgs/filter.svg", 40, 30)),
          ],
        ),
        SizedBox(
          height: 22,
        ),
        Container(
          height: Get.height,
          child: GridView.builder(
            scrollDirection: Axis.vertical,
            physics: NeverScrollableScrollPhysics(),
            itemCount: 4,
            itemBuilder: (BuildContext context, int index) {
              return GamesCardSnippet();
            },
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisExtent: 200,
                crossAxisSpacing: 4.0,
                mainAxisSpacing: 16.0),
          ),
        ),
      ],
    );
  }

  _bottomSheetForFilter() {
    return modalBottomSheet(
        context,
        SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomText(
                    title: "Filter",
                    textSize: 20,
                    fontWeight: FontWeight.w700,
                    textColor: Color(0xff1B1C20),
                  ),
                  CustomText(
                    title: "Clear Filter",
                    textSize: 14,
                    fontWeight: FontWeight.w600,
                    textColor: Color(0xff5F6C6A),
                  ),
                ],
              ),
              SizedBox(height: 40,),
              CustomText(
                title: "Entry Amount",
                textSize: 16,
                fontWeight: FontWeight.w600,
                textColor: Color(0xff5F6C6A),
              ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: AppPrimaryDarkButtonRound(
                      width: Get.width,
                      // isLoader: state is SignUpLoading ? true : false,
                      borderRadius: 10.0,
                      textTitle: "High to Low",
                      bgColor: Color(0xffEEF2F0),
                      textStyle: textStyleBlackColorNormal,
                      functionToRun: () {}),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    child: AppPrimaryDarkButtonRound(
                        width: Get.width,
                        // isLoader: state is SignUpLoading ? true : false,
                        borderRadius: 10.0,
                        textTitle: "Low to High",
                        bgColor: Color(0xff96E0D2),
                        textStyle: textStyleBlackColorNormal,
                        functionToRun: () {}),
                  ),
                ],
              ),
              SizedBox(height: 20,),
              CustomText(
                title: "Win Amount",
                textSize: 16,
                fontWeight: FontWeight.w600,
                textColor: Color(0xff5F6C6A),
              ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: AppPrimaryDarkButtonRound(
                      width: Get.width,
                      // isLoader: state is SignUpLoading ? true : false,
                      borderRadius: 10.0,
                      textTitle: "High to Low",
                      bgColor: Color(0xff96E0D2),
                      textStyle: textStyleBlackColorNormal,
                      functionToRun: () {}),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    child: AppPrimaryDarkButtonRound(
                        width: Get.width,
                        // isLoader: state is SignUpLoading ? true : false,
                        borderRadius: 10.0,
                        textTitle: "Low to High",
                        bgColor: Color(0xffEEF2F0),
                        textStyle: textStyleBlackColorNormal,
                        functionToRun: () {}),
                  ),
                ],
              ),
              SizedBox(height: 20,),
              AppPrimaryDarkButtonRound(
                  width: Get.width,
                  // isLoader: state is SignUpLoading ? true : false,
                  borderRadius: 10.0,
                  textTitle: "Apply Filter",
                  bgColor: appPrimaryColor,
                  textStyle: textStyleWhiteColorNormal,
                  functionToRun: _openBottomSheetForFilter)
            ],
          ).paddingSymmetric(vertical: 15),
        ),
        true,
        Get.height / 1.7);
  }

  _openBottomSheetForFilter() {
    Navigator.pop(context);
    return modalBottomSheet(
        context,
        FilteredFPLResultWidget(),
        true,
        Get.height
    );
  }
}
