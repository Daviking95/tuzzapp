import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:tuzz/AppConstants/ApiResponseDataConstants/user_response_data_constants.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/Modules/DashboardModule/Controllers/dashboard_controller.dart';
import 'package:tuzz/Modules/WalletModule/Screens/wallet_info_screen.dart';
import 'package:tuzz/Snippets/games_card_snippets.dart';
import 'package:tuzz/Snippets/top_categories_snippet.dart';
import 'package:tuzz/Widgets/bottom_sheet_widget.dart';
import 'package:tuzz/Widgets/custom_tab.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/starter_widget.dart';
import 'package:tuzz/Widgets/text.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DashboardController>(
      init: DashboardController(),
      builder: (controller) {
        return StarterWidget(
            hasTopWidget: false,
            hasPagePadding: true,
            isExitApp: true,
            isBgAllowed: false,
            hasBottomNav: true,
            hasRefresh: true,
            startLoader: controller.startLoader,
            bottomPageContent: _bottomPageContent(controller));
      },
    );
  }

  _bottomPageContent(DashboardController controller) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ImageWidget.withSizeOnly(
                  "assets/images/svgs/app_logos/logo_with_black_name.svg",
                  40,
                  100),
              Row(
                children: [
                  CustomText(
                    title: "WALLET",
                    textSize: 12,
                    fontWeight: FontWeight.w500,
                    textColor: Color(0xff1B1C20).withOpacity(.5),
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  GestureDetector(
                    onTap: () => _bottomSheetForWallet(),
                    child: CustomText(
                      title: DIConstants.moneyFormatter.returnFormattedMoney(0.0),
                      textSize: 16,
                      fontWeight: FontWeight.w700,
                    ).paddingAll(5),
                  ),
                ],
              )
            ],
          ),
          SizedBox(
            height: 25,
          ),
          Container(
              height: Get.height,
              child: TopCategoryCustomTab(controller.topCategories, controller.topCategoriesWidgets, controller.tabController)),

        ],
      ),
    ).marginSymmetric(vertical: 15);
  }

  _bottomSheetForWallet() {
    return modalBottomSheet(
        context,
      WalletInfoScreen(),
      true,
      Get.height
    );
  }
}
