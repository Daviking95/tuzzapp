import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Shared/Styles/app_styles.dart';
import 'package:tuzz/Widgets/buttons.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/text.dart';

class NoDataWidget extends StatelessWidget {
  final String iconString;
  final String title;
  final String subTitle;
  final String btnText;
  final Widget routeToGo;
  final Color iconColor;

  NoDataWidget(
      {required this.iconString,
      required this.title,
      required this.subTitle,
      required this.btnText,
        this.iconColor = const Color(0xff8DAAA5),
      required this.routeToGo});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height / 2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ImageWidget.withSizeAndColor(
              iconString, 50, 50, iconColor, true),
          SizedBox(
            height: 25,
          ),
          CustomText(
            title: title,
            textColor: Color(0xff1B1C20),
            textSize: 20,
            isCenter: true,
            fontWeight: FontWeight.w700,
          ),
          SizedBox(
            height: 16,
          ),
          CustomText(
            title: subTitle,
            textSize: 14,
            isCenter: true,
            fontWeight: FontWeight.w400,
          ),
          SizedBox(
            height: 24,
          ),
          AppPrimaryDarkButtonRound(
            width: Get.width,
            // isLoader: state is SignUpLoading ? true : false,
            borderRadius: 10.0,
            textTitle: btnText,
            bgColor: appPrimaryColor,
            textStyle: textStyleWhiteColorNormal,
            functionToRun: () => Get.to(() => routeToGo),
          )
        ],
      ),
    );
  }
}
