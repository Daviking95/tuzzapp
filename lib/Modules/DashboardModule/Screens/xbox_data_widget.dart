import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Snippets/games_card_snippets.dart';
import 'package:tuzz/Widgets/image_widget.dart';

class XBoxDataWidget extends StatefulWidget {
  const XBoxDataWidget({Key? key}) : super(key: key);

  @override
  _XBoxDataWidgetState createState() => _XBoxDataWidgetState();
}

class _XBoxDataWidgetState extends State<XBoxDataWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 40,
          width: Get.width,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: 8,
            itemBuilder: (BuildContext context, int index) {
              return index == 0
                  ? ImageWidget.withSizeOnly(
                      "assets/images/svgs/filter.svg", 40, 30).marginOnly(right: 10)
                  : FilterCardSnippet();
            },
            // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            //     crossAxisCount: 2, mainAxisExtent: 120, crossAxisSpacing: 4.0, mainAxisSpacing: 4.0),
          ),
        ),
        SizedBox(height: 30,),
        Container(
          height: Get.height,
          child: GridView.builder(
            scrollDirection: Axis.vertical,
            physics: NeverScrollableScrollPhysics(),
            itemCount: 4,
            itemBuilder: (BuildContext context, int index) {
              return XBoxGamesCardSnippet();
            },
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, mainAxisExtent: 200, crossAxisSpacing: 4.0, mainAxisSpacing: 16.0),
          ),
        )
      ],
    );
  }
}
