import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:tuzz/Helpers/Enums/categories_enum.dart';
import 'package:tuzz/Modules/CompetitionModule/Screens/create_competitions/create_competition_screen.dart';
import 'package:tuzz/Snippets/games_card_snippets.dart';
import 'package:tuzz/Widgets/image_widget.dart';
import 'package:tuzz/Widgets/text.dart';

import 'board_games_data_widget.dart';
import 'fpl_data_widget.dart';
import 'mobile_games_data_widget.dart';
import 'no_data_widget.dart';
import 'other_games_data_widget.dart';
import 'sports_game_data_widget.dart';
import 'xbox_data_widget.dart';

class DashboardTopCategoriesWidget extends StatefulWidget {
  final CategoriesEnum categoriesEnum;

  const DashboardTopCategoriesWidget(this.categoriesEnum, {Key? key})
      : super(key: key);

  @override
  _DashboardTopCategoriesWidgetState createState() =>
      _DashboardTopCategoriesWidgetState();
}

class _DashboardTopCategoriesWidgetState
    extends State<DashboardTopCategoriesWidget> {
  bool hasData = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          !hasData
              ? Container()
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Divider(
                      height: 1,
                      color: Color(0xff000000).withOpacity(.1),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CustomText(
                      title: _getTitle(widget.categoriesEnum)['title'],
                      textSize: 24,
                      fontWeight: FontWeight.w700,
                      textColor: Color(0xff1B1C20),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    GestureDetector(
                      onTap: () => Get.to(
                          () => CreateCompetitionScreen(widget.categoriesEnum)),
                      child: Container(
                        decoration: BoxDecoration(
                            // boxShadow: [
                            //   BoxShadow(
                            //     color: Color(0xff1B1C20).withOpacity(.2),
                            //     spreadRadius: 3,
                            //     blurRadius: 15,
                            //     offset: Offset.fromDirection(1, 5), // changes position of shadow
                            //   ),
                            // ]
                            ),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(16),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0xff000000).withOpacity(.9),
                                  blurRadius: 20.0,
                                  // soften the shadow
                                  spreadRadius: -15,
                                  //extend the shadow
                                  offset: Offset(
                                    0,
                                    5.0, // Move to bottom 10 Vertically
                                  ),
                                )
                              ]),
                          child: ClipPath(
                            clipper: ShapeBorderClipper(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(16),
                              ),
                            ),
                            child: Container(
                              height: 190,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color:
                                    _getTitle(widget.categoriesEnum)['cardBg'],
                              ),
                              child: Stack(
                                children: [
                                  Positioned(
                                      right: 10,
                                      child: ImageWidget.withSizeOnly(
                                          "assets/images/svgs/dash_card_bg_1.svg",
                                          70,
                                          70)),
                                  Positioned(
                                      right: 0,
                                      child: ImageWidget.withSizeOnly(
                                          "assets/images/svgs/dash_card_bg_2.svg",
                                          80,
                                          80)),
                                  Positioned(
                                    top: 25,
                                    left: 20,
                                    right: 20,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          decoration: BoxDecoration(
                                            color: _getTitle(
                                                    widget.categoriesEnum)[
                                                'cardIconBgColor'],
                                            borderRadius:
                                                BorderRadius.circular(100),
                                          ),
                                          child: ImageWidget.withSizeAndColor(
                                                  _getTitle(widget
                                                      .categoriesEnum)['icon'],
                                                  24,
                                                  20,
                                                  _getTitle(widget
                                                          .categoriesEnum)[
                                                      'cardIconColor'],
                                                  true)
                                              .paddingAll(12),
                                        ),
                                        SizedBox(
                                          height: 15,
                                        ),
                                        CustomText(
                                          title:
                                              _getTitle(widget.categoriesEnum)[
                                                  'cardTitle'],
                                          textColor:
                                              _getTitle(widget.categoriesEnum)[
                                                      'hasColorInverse']
                                                  ? Colors.white
                                                  : Color(0xff1B1C20),
                                          textSize: 20,
                                          fontWeight: FontWeight.w700,
                                        ),
                                        SizedBox(
                                          height: 8,
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              flex: 3,
                                              child: CustomText(
                                                title: _getTitle(
                                                        widget.categoriesEnum)[
                                                    'cardSubTitle'],
                                                textColor: _getTitle(widget
                                                            .categoriesEnum)[
                                                        'hasColorInverse']
                                                    ? Colors.white
                                                    : Color(0xff1B1C20),
                                                textSize: 12,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                            Spacer(),
                                            Container(
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    BorderRadius.circular(100),
                                              ),
                                              child: Icon(
                                                LineIcons
                                                    .alternateLongArrowRight,
                                                color: Color(0xff96E0D2),
                                                size: 30,
                                              ).paddingAll(5),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                  ],
                ),
          _getTitle(widget.categoriesEnum)['dataWidget'],
        ],
      ),
    );
  }

  _getTitle(CategoriesEnum categoriesEnum) {
    Map pageData;
    switch (categoriesEnum) {
      case CategoriesEnum.FPL:
        // TODO: Handle this case.
        pageData = {
          "title": "Premier League \nFantasy",
          "icon": "assets/images/pngs/tc_fpl.png",
          "cardIconColor": Color(0xff00223A),
          "cardIconBgColor": Colors.white,
          "hasColorInverse": false,
          "cardTitle": "Create FPL Competition",
          "cardSubTitle":
              "Create your own paid FPL competition, play with friends and win prizes",
          "cardBg": Color(0xff96E0D2),
          "dataWidget": hasData
              ? FplDataWidget()
              : NoDataWidget(
                  iconString: "assets/images/pngs/tc_fpl.png",
                  title: "Public FPL Competitions shows here",
                  subTitle:
                      "Create your own paid FPL competition, play with friends and win prizes",
                  btnText: "Create FPL Competition",
                  routeToGo: CreateCompetitionScreen(categoriesEnum),
                )
        };
        break;
      case CategoriesEnum.XBOX:
        // TODO: Handle this case.
        pageData = {
          "title": "PS & XBOX \nConsole Games",
          "icon": "assets/images/pngs/tc_ps.png",
          "cardIconColor": Color(0xffFFF7C0),
          "cardIconBgColor": Color(0xff1B1C20),
          "hasColorInverse": false,
          "cardTitle": "Create Competition",
          "cardSubTitle":
              "Create your PC, PS or XBOX competition, play with friends and win prizes",
          "cardBg": Color(0xffFFF7C0),
          "dataWidget": hasData
              ? XBoxDataWidget()
              : NoDataWidget(
                  iconString: "assets/images/pngs/tc_ps.png",
                  title: "PS & XBOX Competitions shows here",
                  subTitle:
                      "Create your own console competition, play with friends and win prizes",
                  btnText: "Create Console Competition",
                  routeToGo: CreateCompetitionScreen(categoriesEnum),
                )
        };

        break;
      case CategoriesEnum.MOBILE_GAMES:
        // TODO: Handle this case.
        pageData = {
          "title": "Mobile Game \nCompetitions",
          "icon": "assets/images/pngs/tc_games.png",
          "cardIconColor": Color(0xffFFDDF7),
          "cardIconBgColor": Color(0xff1B1C20),
          "hasColorInverse": false,
          "cardTitle": "Create Competition",
          "cardSubTitle":
              "Create your Mobile Game competition, play with friends and win prizes",
          "cardBg": Color(0xffFFDDF7),
          "dataWidget": hasData
              ? MobileDataWidget()
              : NoDataWidget(
                  iconString: "assets/images/pngs/tc_games.png",
                  title: "Mobile Game Competitions shows here",
                  subTitle:
                      "Create your own mobile game competition, play with friends and win prizes",
                  btnText: "Create Mobile Game Competition",
                  routeToGo: CreateCompetitionScreen(categoriesEnum),
                )
        };

        break;
      case CategoriesEnum.BOARD_GAMES:
        // TODO: Handle this case.
        pageData = {
          "title": "Board Game \nCompetitions",
          "icon": "assets/images/pngs/tc_board_games.png",
          "cardIconColor": Color(0xff11A683),
          "cardIconBgColor": Color(0xff1B1C20),
          "hasColorInverse": false,
          "cardTitle": "Create Competition",
          "cardSubTitle":
              "Create your Board Game competition, play with friends and win prizes",
          "cardBg": Color(0xffC0F4FF),
          "dataWidget": hasData
              ? BoardGamesDataWidget()
              : NoDataWidget(
                  iconString: "assets/images/pngs/tc_board_games.png",
                  title: "Board Game Competitions shows here",
                  subTitle:
                      "Create your own board game competition, play with friends and win prizes",
                  btnText: "Create Board Game Competition",
                  routeToGo: CreateCompetitionScreen(categoriesEnum),
                )
        };

        break;
      case CategoriesEnum.SPORTS:
        // TODO: Handle this case.
        pageData = {
          "title": "Sport Competitions",
          "icon": "assets/images/pngs/tc_sports.png",
          "cardIconColor": Color(0xff1B1C20),
          "cardIconBgColor": Colors.white,
          "hasColorInverse": true,
          "cardTitle": "Create Competition",
          "cardSubTitle":
              "Create your sport competition, play with friends and win prizes",
          "cardBg": Color(0xff7D2DFF),
          "dataWidget": hasData
              ? SportsGamesDataWidget()
              : NoDataWidget(
                  iconString: "assets/images/pngs/tc_sports.png",
                  title: "Sport Competitions shows here",
                  subTitle:
                      "Create your own sport competition, play with friends and win prizes",
                  btnText: "Create Sport Competition",
                  routeToGo: CreateCompetitionScreen(categoriesEnum),
                )
        };

        break;
      case CategoriesEnum.OTHERS:
        // TODO: Handle this case.
        pageData = {
          "title": "Other Competitions",
          "icon": "assets/images/pngs/tc_others.png",
          "cardIconColor": Color(0xff1B1C20),
          "cardIconBgColor": Colors.white,
          "hasColorInverse": true,
          "cardTitle": "Create Competition",
          "cardSubTitle":
              "Create your own competition, play with friends and win prizes",
          "cardBg": Color(0xffE4294B),
          "dataWidget": hasData
              ? OtherGamesDataWidget()
              : NoDataWidget(
                  iconString: "assets/images/pngs/tc_others.png",
                  title: "Other Competitions shows here",
                  subTitle:
                      "Create your own competition, play with friends and win prizes",
                  btnText: "Create Console Competition",
                  routeToGo: CreateCompetitionScreen(categoriesEnum),
                )
        };

        break;
    }

    return pageData;
  }
}
