import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/AppConstants/InjectionConstants/injection_constants.dart';
import 'package:tuzz/Modules/DashboardModule/Controllers/dashboard_controller.dart';
import 'package:tuzz/Snippets/games_card_snippets.dart';
import 'package:tuzz/Widgets/text.dart';

class FilteredFPLResultWidget extends StatefulWidget {
  const FilteredFPLResultWidget({Key? key}) : super(key: key);

  @override
  _FilteredFPLResultWidgetState createState() =>
      _FilteredFPLResultWidgetState();
}

class _FilteredFPLResultWidgetState extends State<FilteredFPLResultWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DashboardController>(
      init: DashboardController(),
      builder: (controller) {
        return SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      GestureDetector(
                          onTap: () => Get.back(),
                          child: Icon(Icons.arrow_back_ios, size: 15,).paddingAll(10)),
                      SizedBox(
                        width: 15,
                      ),
                      CustomText(
                        title: "Filter result (42)",
                        textSize: 14,
                        fontWeight: FontWeight.w400,
                        textColor: Color(0xff1B1C20),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      CustomText(
                        title: "WALLET",
                        textSize: 12,
                        fontWeight: FontWeight.w500,
                        textColor: Color(0xff5F6C6A),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      CustomText(
                        title: DIConstants.moneyFormatter
                            .returnFormattedMoney(45000),
                        textSize: 16,
                        fontWeight: FontWeight.w700,
                        textColor: Color(0xff1B1C20),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  CustomText(
                    title: "Tuzzes for Gameweek 3",
                    textSize: 16,
                    fontWeight: FontWeight.w600,
                    textColor: Color(0xff1B1C20),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: Get.height,
                child: GridView.builder(
                  scrollDirection: Axis.vertical,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: 4,
                  itemBuilder: (BuildContext context, int index) {
                    return GamesCardSnippet();
                  },
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      mainAxisExtent: 200,
                      crossAxisSpacing: 4.0,
                      mainAxisSpacing: 16.0),
                ),
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ).paddingSymmetric(vertical: 30),
        );
      },
    );
  }
}
