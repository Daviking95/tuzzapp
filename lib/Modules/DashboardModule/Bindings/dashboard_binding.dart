

import 'package:get/get.dart';
import 'package:tuzz/Modules/DashboardModule/Controllers/dashboard_controller.dart';
import 'package:tuzz/Modules/DashboardModule/Services/dashboard_service.dart';

class DashboardBinding extends Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.put(DashboardController());
    Get.put(DashboardService());
  }

}