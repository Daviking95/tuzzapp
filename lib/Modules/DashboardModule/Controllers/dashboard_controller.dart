

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tuzz/Helpers/Enums/categories_enum.dart';
import 'package:tuzz/Modules/DashboardModule/Screens/dashboard_cat_widget.dart';

class DashboardController extends GetxController with SingleGetTickerProviderMixin{

  final List<dynamic> _topCategories = [
    {"title": "PL Fantasy", "imageString": "assets/images/pngs/tc_fpl.png"},
    {"title": "PS & XBOX", "imageString": "assets/images/pngs/tc_ps.png"},
    {"title": "Mobile games", "imageString": "assets/images/pngs/tc_games.png"},
    {"title": "Board games", "imageString": "assets/images/pngs/tc_board_games.png"},
    {"title": "Sports", "imageString": "assets/images/pngs/tc_sports.png"},
    {"title": "Others", "imageString": "assets/images/pngs/tc_others.png"},
  ];

  final List<Widget> _topCategoriesWidgets = [
    DashboardTopCategoriesWidget(CategoriesEnum.FPL),
    DashboardTopCategoriesWidget(CategoriesEnum.XBOX),
    DashboardTopCategoriesWidget(CategoriesEnum.MOBILE_GAMES),
    DashboardTopCategoriesWidget(CategoriesEnum.BOARD_GAMES),
    DashboardTopCategoriesWidget(CategoriesEnum.SPORTS),
    DashboardTopCategoriesWidget(CategoriesEnum.OTHERS),
  ];


  List<Widget> get topCategoriesWidgets => _topCategoriesWidgets;

  List<dynamic> get topCategories => _topCategories;
  bool _startLoader = false.obs();

  bool get startLoader => _startLoader;

  set startLoader(bool value) {
    _startLoader = value;

    update();
  }

  late TabController tabController;

  @override
  void onInit() {
    super.onInit();
    tabController = TabController(vsync: this, length: _topCategories.length);
  }

  @override
  void onClose() {
    tabController.dispose();
    super.onClose();
  }
}