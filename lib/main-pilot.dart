import 'package:catcher/catcher.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:tuzz/rb_starter_functions.dart';
import 'Shared/Controllers/main_controllers.dart';
import 'flavors.dart';
import 'main.dart';

void main() async{
  F.appFlavor = Flavor.PILOT;
  await StarterFunctions().setupFunctions();
  List catchOptions = await StarterFunctions().errorCatcherSetup();
  final MainController _mainController = Get.put(MainController());
  Catcher(
      rootWidget: TuzzMainApp(navigatorKey : _mainController.navigatorKey),
      navigatorKey: _mainController.navigatorKey,
      debugConfig: catchOptions[0],
      releaseConfig: catchOptions[1]);

  // runApp(MultiRepositoryProvider(
  //   providers: RepositoryProviders.repositoryProviderList,
  //   child: MultiBlocProvider(
  //     providers: SharedBlocProviders.providersList,
  //     child: TuzzMainApp(),
  //   ),
  // ));
}
