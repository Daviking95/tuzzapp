import 'package:catcher/catcher.dart';
import 'flavors.dart';
import 'main.dart';
import 'rb_starter_functions.dart';

void main() async{
  F.appFlavor = Flavor.STAGING;
  await StarterFunctions().setupFunctions();
  List catchOptions = await StarterFunctions().errorCatcherSetup();
  Catcher(
      rootWidget: TuzzMainApp(),
      debugConfig: catchOptions[0],
      releaseConfig: catchOptions[1]);

  // runApp(MultiRepositoryProvider(
  //   providers: RepositoryProviders.repositoryProviderList,
  //   child: MultiBlocProvider(
  //     providers: SharedBlocProviders.providersList,
  //     child: TuzzMainApp(),
  //   ),
  // ));
}
