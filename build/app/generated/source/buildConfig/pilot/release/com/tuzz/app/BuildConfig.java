/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.tuzz.app;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.tuzz.pilot";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "pilot";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0.0";
  // Fields from build type: release
  public static final Boolean DEBUG_MODE = false;
}
