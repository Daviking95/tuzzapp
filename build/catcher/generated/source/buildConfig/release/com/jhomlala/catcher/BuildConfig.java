/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.jhomlala.catcher;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "com.jhomlala.catcher";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}
